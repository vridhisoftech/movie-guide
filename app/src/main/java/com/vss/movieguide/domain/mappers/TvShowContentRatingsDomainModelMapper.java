package com.vss.movieguide.domain.mappers;

import com.vss.movieguide.data.repositories.models.TvShowContentRatingsDataModel;
import com.vss.movieguide.domain.models.TvShowContentRatingsDomainModel;

public class TvShowContentRatingsDomainModelMapper implements DomainModelMapper<TvShowContentRatingsDataModel, TvShowContentRatingsDomainModel> {

    // region Member Variables
    private ContentRatingDomainModelMapper contentRatingDomainModelMapper = new ContentRatingDomainModelMapper();
    // endregion

    @Override
    public TvShowContentRatingsDomainModel mapToDomainModel(TvShowContentRatingsDataModel tvShowContentRatingsDataModel) {
        TvShowContentRatingsDomainModel tvShowContentRatingsDomainModel = new TvShowContentRatingsDomainModel();
        tvShowContentRatingsDomainModel.setContentRatings(contentRatingDomainModelMapper.mapListToDomainModelList(tvShowContentRatingsDataModel.getContentRatings()));
        tvShowContentRatingsDomainModel.setId(tvShowContentRatingsDataModel.getId());
        return tvShowContentRatingsDomainModel;
    }
}

