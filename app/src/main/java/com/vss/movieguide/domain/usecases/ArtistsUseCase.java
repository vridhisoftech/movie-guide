package com.vss.movieguide.domain.usecases;

import com.vss.movieguide.data.repositories.artist.ArtistDataSourceContract;
import com.vss.movieguide.domain.mappers.ArtistsDomainModelMapper;
import com.vss.movieguide.domain.models.ArtistsDomainModel;

import io.reactivex.Single;

public class ArtistsUseCase implements ArtistsDomainContract.UseCase {

    // region Member Variables
    private final ArtistDataSourceContract.Repository personRepository;
    private ArtistsDomainModelMapper artistsDomainModelMapper = new ArtistsDomainModelMapper();
    // endregion

    // region Constructors
    public ArtistsUseCase(ArtistDataSourceContract.Repository personRepository) {
        this.personRepository = personRepository;
    }
    // endregion

    // region PersonsDomainContract.UseCase Methods
    @Override
    public Single<ArtistsDomainModel> getPopularPersons(int currentPage) {
        return personRepository.getPopularPersons(currentPage)
                .map(personsDataModel -> artistsDomainModelMapper.mapToDomainModel(personsDataModel));
    }
    // endregion

}
