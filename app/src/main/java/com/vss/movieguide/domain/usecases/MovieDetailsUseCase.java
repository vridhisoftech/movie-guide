package com.vss.movieguide.domain.usecases;

import com.vss.movieguide.data.repositories.movie.MovieDataSourceContract;
import com.vss.movieguide.domain.composers.MovieDetailsDomainModelComposer;
import com.vss.movieguide.domain.models.MovieDetailsDomainModel;

import io.reactivex.Single;

public class MovieDetailsUseCase implements MovieDetailsDomainContract.UseCase {

    // region Member Variables
    private final MovieDataSourceContract.Repository movieRepository;
    private final MovieDetailsDomainModelComposer movieDetailsDomainModelComposer = new MovieDetailsDomainModelComposer();
    // endregion

    // region Constructors
    public MovieDetailsUseCase(MovieDataSourceContract.Repository movieRepository) {
        this.movieRepository = movieRepository;
    }
    // endregion

    // region MovieDetailsDomainContract.UseCase Methods
    @Override
    public Single<MovieDetailsDomainModel> getMovieDetails(long movieId) {
        return Single.zip(
                movieRepository.getMovie(movieId),
                movieRepository.getMovieCredits(movieId),
                movieRepository.getSimilarMovies(movieId),
                movieRepository.getMovieReleaseDates(movieId),
                movieDetailsDomainModelComposer::compose
                );
    }
    // endregion

}
