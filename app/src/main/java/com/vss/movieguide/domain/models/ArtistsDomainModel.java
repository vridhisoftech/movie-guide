package com.vss.movieguide.domain.models;

import java.util.Date;
import java.util.List;

public class ArtistsDomainModel {

    // region Fields
    private List<ArtistDomainModel> persons;
    private int pageNumber;
    private boolean isLastPage;
    private Date expiredAt;
    // endregion

    // region Constructors

    public ArtistsDomainModel() {
    }

    // endregion

    // region Getters

    public List<ArtistDomainModel> getPersons() {
        return persons;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public boolean isLastPage() {
        return isLastPage;
    }

    public Date getExpiredAt() {
        return expiredAt;
    }

    // endregion

    // region Setters

    public void setPersons(List<ArtistDomainModel> persons) {
        this.persons = persons;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public void setLastPage(boolean lastPage) {
        isLastPage = lastPage;
    }

    public void setExpiredAt(Date expiredAt) {
        this.expiredAt = expiredAt;
    }

    // endregion

    // region Helper Methods
    public boolean hasPersons() { return persons.size() > 0;}
    // endregion

    @Override
    public String toString() {
        return "PersonsDomainModel{" +
                "persons=" + persons +
                ", pageNumber=" + pageNumber +
                ", isLastPage=" + isLastPage +
                ", expiredAt=" + expiredAt +
                '}';
    }
}
