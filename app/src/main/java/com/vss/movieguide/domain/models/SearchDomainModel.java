package com.vss.movieguide.domain.models;

import java.util.List;

public class SearchDomainModel {

    // region Fields
    private String query;
    private List<MovieDomainModel> movies;
    private List<TvShowDomainModel> televisionShows;
    private List<ArtistDomainModel> persons;
    // endregion

    // region Constructors
    public SearchDomainModel() {
    }
    // endregion

    // region Getters

    public String getQuery() {
        return query;
    }

    public List<MovieDomainModel> getMovies() {
        return movies;
    }

    public List<TvShowDomainModel> getTelevisionShows() {
        return televisionShows;
    }

    public List<ArtistDomainModel> getPersons() {
        return persons;
    }

    // endregion

    // region Setters

    public void setQuery(String query) {
        this.query = query;
    }

    public void setMovies(List<MovieDomainModel> movies) {
        this.movies = movies;
    }

    public void setTelevisionShows(List<TvShowDomainModel> televisionShows) {
        this.televisionShows = televisionShows;
    }

    public void setPersons(List<ArtistDomainModel> persons) {
        this.persons = persons;
    }

    // endregion

    // region Helper Methods
    public boolean hasMovies() {
        return movies != null && movies.size() > 0;
    }

    public boolean hasTelevisionShows() {
        return televisionShows != null && televisionShows.size() > 0;
    }

    public boolean hasPersons() {
        return persons != null && persons.size() > 0;
    }

    public boolean hasResults(){
        return (hasMovies())
                || (hasTelevisionShows())
                || (hasPersons());
    }
    // endregion

    @Override
    public String toString() {
        return "SearchDomainModel{" +
                "query='" + query + '\'' +
                ", movies=" + movies +
                ", televisionShows=" + televisionShows +
                ", persons=" + persons +
                '}';
    }
}
