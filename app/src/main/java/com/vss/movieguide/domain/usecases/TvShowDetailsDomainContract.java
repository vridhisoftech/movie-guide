package com.vss.movieguide.domain.usecases;

import com.vss.movieguide.domain.models.TvShowDetailsDomainModel;

import io.reactivex.Single;

public interface TvShowDetailsDomainContract {

    interface UseCase {
        Single<TvShowDetailsDomainModel> getTelevisionShowDetails(int televisionShowId);
    }
}
