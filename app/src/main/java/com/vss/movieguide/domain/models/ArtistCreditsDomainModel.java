package com.vss.movieguide.domain.models;

import java.util.List;


public class ArtistCreditsDomainModel {

    // region Fields
    public int id;
    public List<ArtistCreditDomainModel> cast = null;
    public List<ArtistCreditDomainModel> crew = null;
    // endregion

    // region Getters

    public int getId() {
        return id;
    }

    public List<ArtistCreditDomainModel> getCast() {
        return cast;
    }

    public List<ArtistCreditDomainModel> getCrew() {
        return crew;
    }

    // endregion

    // region Setters

    public void setId(int id) {
        this.id = id;
    }

    public void setCast(List<ArtistCreditDomainModel> cast) {
        this.cast = cast;
    }

    public void setCrew(List<ArtistCreditDomainModel> crew) {
        this.crew = crew;
    }

    // endregion

    @Override
    public String toString() {
        return "PersonCreditsDomainModel{" +
                "id=" + id +
                ", cast=" + cast +
                ", crew=" + crew +
                '}';
    }
}
