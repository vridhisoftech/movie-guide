package com.vss.movieguide.domain.mappers;

import com.vss.movieguide.data.repositories.models.TvShowCreditsDataModel;
import com.vss.movieguide.domain.models.TvShowCreditsDomainModel;

public class TvShowCreditsDomainModelMapper implements DomainModelMapper<TvShowCreditsDataModel, TvShowCreditsDomainModel> {

    // region Member Variables
    private TvShowCreditDomainModelMapper tvShowCreditDomainModelMapper = new TvShowCreditDomainModelMapper();
    // endregion

    @Override
    public TvShowCreditsDomainModel mapToDomainModel(TvShowCreditsDataModel tvShowCreditsDataModel) {
        TvShowCreditsDomainModel tvShowCreditsDomainModel = new TvShowCreditsDomainModel();
        tvShowCreditsDomainModel.setId(tvShowCreditsDataModel.getId());
        tvShowCreditsDomainModel.setCast(tvShowCreditDomainModelMapper.mapListToDomainModelList(tvShowCreditsDataModel.getCast()));
        tvShowCreditsDomainModel.setCrew(tvShowCreditDomainModelMapper.mapListToDomainModelList(tvShowCreditsDataModel.getCrew()));
        return tvShowCreditsDomainModel;
    }
}
