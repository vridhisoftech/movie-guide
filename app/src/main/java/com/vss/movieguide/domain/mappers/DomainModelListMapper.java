package com.vss.movieguide.domain.mappers;

import java.util.List;


public interface DomainModelListMapper<DataModel, DomainModel> {
    List<DomainModel> mapListToDomainModelList(List<DataModel> dataModels);
}