package com.vss.movieguide.domain.composers;

import com.vss.movieguide.data.repositories.models.MovieDataModel;
import com.vss.movieguide.data.repositories.models.MoviesDataModel;
import com.vss.movieguide.data.repositories.models.ArtistDataModel;
import com.vss.movieguide.data.repositories.models.ArtistsDataModel;
import com.vss.movieguide.data.repositories.models.TvShowDataModel;
import com.vss.movieguide.data.repositories.models.TvShowsDataModel;
import com.vss.movieguide.domain.mappers.MovieDomainModelMapper;
import com.vss.movieguide.domain.mappers.ArtistDomainModelMapper;
import com.vss.movieguide.domain.mappers.TvShowDomainModelMapper;
import com.vss.movieguide.domain.models.SearchDomainModel;

import java.util.ArrayList;
import java.util.List;

public class SearchDomainModelComposer {

    private MovieDomainModelMapper movieDomainModelMapper = new MovieDomainModelMapper();
    private ArtistDomainModelMapper artistDomainModelMapper = new ArtistDomainModelMapper();
    private TvShowDomainModelMapper tvShowDomainModelMapper = new TvShowDomainModelMapper();

    public SearchDomainModel compose(MoviesDataModel moviesDataModel, TvShowsDataModel tvShowsDataModel, ArtistsDataModel artistsDataModel, String query){
        SearchDomainModel searchDomainModel = new SearchDomainModel();

        List<MovieDataModel> movies = new ArrayList<>();
        List<TvShowDataModel> televisionShows = new ArrayList<>();
        List<ArtistDataModel> persons = new ArrayList<>();

        if (moviesDataModel != null) {
            movies = moviesDataModel.getMovies();
        }

        if (tvShowsDataModel != null) {
            televisionShows = tvShowsDataModel.getTelevisionShows();
        }

        if (artistsDataModel != null) {
            persons = artistsDataModel.getPersons();
        }

        searchDomainModel.setMovies(movieDomainModelMapper.mapListToDomainModelList(movies));
        searchDomainModel.setPersons(artistDomainModelMapper.mapListToDomainModelList(persons));
        searchDomainModel.setQuery(query);
        searchDomainModel.setTelevisionShows(tvShowDomainModelMapper.mapListToDomainModelList(televisionShows));

        return searchDomainModel;
    }
}
