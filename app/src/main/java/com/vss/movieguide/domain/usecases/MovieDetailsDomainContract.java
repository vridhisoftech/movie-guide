package com.vss.movieguide.domain.usecases;

import com.vss.movieguide.domain.models.MovieDetailsDomainModel;

import io.reactivex.Single;

public interface MovieDetailsDomainContract {

    interface UseCase {
        Single<MovieDetailsDomainModel> getMovieDetails(long movieId);
    }
}
