package com.vss.movieguide.domain.usecases;

import com.vss.movieguide.domain.models.ArtistsDomainModel;

import io.reactivex.Single;

public interface ArtistsDomainContract {

    interface UseCase {
        Single<ArtistsDomainModel> getPopularPersons(int currentPage);
    }
}
