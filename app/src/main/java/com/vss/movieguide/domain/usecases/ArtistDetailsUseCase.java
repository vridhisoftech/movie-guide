package com.vss.movieguide.domain.usecases;

import com.vss.movieguide.data.repositories.artist.ArtistDataSourceContract;
import com.vss.movieguide.domain.composers.ArtistDetailsDomainModelComposer;
import com.vss.movieguide.domain.models.ArtistDetailsDomainModel;

import io.reactivex.Single;

public class ArtistDetailsUseCase implements ArtistDetailsDomainContract.UseCase {

    // region Member Variables
    private final ArtistDataSourceContract.Repository personRepository;
    private final ArtistDetailsDomainModelComposer artistDetailsDomainModelComposer = new ArtistDetailsDomainModelComposer();
    // endregion

    // region Constructors
    public ArtistDetailsUseCase(ArtistDataSourceContract.Repository personRepository) {
        this.personRepository = personRepository;
    }
    // endregion

    // region MovieDetailsDomainContract.UseCase Methods
    @Override
    public Single<ArtistDetailsDomainModel> getPersonDetails(int personId) {
        return Single.zip(
                personRepository.getPerson(personId),
                personRepository.getPersonCredits(personId),
                artistDetailsDomainModelComposer::compose);
    }
    // endregion

}
