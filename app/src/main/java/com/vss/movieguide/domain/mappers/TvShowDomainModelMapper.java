package com.vss.movieguide.domain.mappers;

import com.vss.movieguide.data.repositories.models.TvShowDataModel;
import com.vss.movieguide.domain.models.TvShowDomainModel;

import java.util.ArrayList;
import java.util.List;

public class TvShowDomainModelMapper implements DomainModelMapper<TvShowDataModel, TvShowDomainModel>, DomainModelListMapper<TvShowDataModel, TvShowDomainModel> {

    // region Member Variables
    private GenreDomainModelMapper genreDomainModelMapper = new GenreDomainModelMapper();
    private NetworkDomainModelMapper networkDomainModelMapper = new NetworkDomainModelMapper();
    // endregion

    @Override
    public TvShowDomainModel mapToDomainModel(TvShowDataModel tvShowDataModel) {
        TvShowDomainModel tvShowDomainModel = new TvShowDomainModel();
        tvShowDomainModel.setBackdropPath(tvShowDataModel.getBackdropPath());
        tvShowDomainModel.setEpisodeRunTime(tvShowDataModel.getEpisodeRunTime());
        tvShowDomainModel.setFirstAirDate(tvShowDataModel.getFirstAirDate());
        tvShowDomainModel.setGenres(genreDomainModelMapper.mapListToDomainModelList(tvShowDataModel.getGenres()));
        tvShowDomainModel.setHomepage(tvShowDataModel.getHomepage());
        tvShowDomainModel.setId(tvShowDataModel.getId());
        tvShowDomainModel.setInProduction(tvShowDataModel.isInProduction());
        tvShowDomainModel.setLanguages(tvShowDataModel.getLanguages());
        tvShowDomainModel.setLastAirDate(tvShowDataModel.getLastAirDate());
        tvShowDomainModel.setName(tvShowDataModel.getName());
        tvShowDomainModel.setNetworks(networkDomainModelMapper.mapListToDomainModelList(tvShowDataModel.getNetworks()));
        tvShowDomainModel.setNumberOfEpisodes(tvShowDataModel.getNumberOfEpisodes());
        tvShowDomainModel.setNumberOfSeasons(tvShowDataModel.getNumberOfSeasons());
        tvShowDomainModel.setOriginalLanguage(tvShowDataModel.getOriginalLanguage());
        tvShowDomainModel.setOriginalName(tvShowDataModel.getOriginalName());
        tvShowDomainModel.setOriginCountry(tvShowDataModel.getOriginCountry());
        tvShowDomainModel.setOverview(tvShowDataModel.getOverview());
        tvShowDomainModel.setPopularity(tvShowDataModel.getPopularity());
        tvShowDomainModel.setPosterPath(tvShowDataModel.getPosterPath());
        tvShowDomainModel.setStatus(tvShowDataModel.getStatus());
        tvShowDomainModel.setType(tvShowDataModel.getType());
        tvShowDomainModel.setVoteAverage(tvShowDataModel.getVoteAverage());
        tvShowDomainModel.setVoteCount(tvShowDataModel.getVoteCount());
        return tvShowDomainModel;
    }

    @Override
    public List<TvShowDomainModel> mapListToDomainModelList(List<TvShowDataModel> tvShowDataModels) {
        List<TvShowDomainModel> tvShowDomainModels = new ArrayList<>();
        if(tvShowDataModels != null && tvShowDataModels.size()>0) {
            for (TvShowDataModel tvShowDataModel : tvShowDataModels) {
                tvShowDomainModels.add(mapToDomainModel(tvShowDataModel));
            }
        }
        return tvShowDomainModels;
    }
}