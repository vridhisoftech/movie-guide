package com.vss.movieguide.domain.composers;

import com.vss.movieguide.data.repositories.models.ContentRatingDataModel;
import com.vss.movieguide.data.repositories.models.TvShowContentRatingsDataModel;
import com.vss.movieguide.data.repositories.models.TvShowCreditDataModel;
import com.vss.movieguide.data.repositories.models.TvShowCreditsDataModel;
import com.vss.movieguide.data.repositories.models.TvShowDataModel;
import com.vss.movieguide.data.repositories.models.TvShowsDataModel;
import com.vss.movieguide.domain.mappers.TvShowCreditDomainModelMapper;
import com.vss.movieguide.domain.mappers.TvShowDomainModelMapper;
import com.vss.movieguide.domain.models.TvShowDetailsDomainModel;

import java.util.ArrayList;
import java.util.List;

public class TvShowDetailsDomainModelComposer {

    // region Constants
    private static final String ISO_31661 = "US";
    // endregion

    private TvShowCreditDomainModelMapper tvShowCreditDomainModelMapper = new TvShowCreditDomainModelMapper();
    private TvShowDomainModelMapper tvShowDomainModelMapper = new TvShowDomainModelMapper();

    public TvShowDetailsDomainModel compose(TvShowDataModel tvShowDataModel, TvShowCreditsDataModel tvShowCreditsDataModel, TvShowsDataModel tvShowsDataModel, TvShowContentRatingsDataModel tvShowContentRatingsDataModel){
        TvShowDetailsDomainModel tvShowDetailsDomainModel = new TvShowDetailsDomainModel();

        List<TvShowCreditDataModel> cast = new ArrayList<>();
        List<TvShowCreditDataModel> crew = new ArrayList<>();
        List<TvShowDataModel> similarTelevisionShows = new ArrayList<>();
        String rating = "";

        if(tvShowCreditsDataModel !=null){
            cast = tvShowCreditsDataModel.getCast();
        }

        if(tvShowCreditsDataModel !=null){
            crew = tvShowCreditsDataModel.getCrew();
        }

        if(tvShowsDataModel !=null){
            similarTelevisionShows = tvShowsDataModel.getTelevisionShows();
        }

        if(tvShowContentRatingsDataModel !=null){
            List<ContentRatingDataModel> contentRatingDataModels = tvShowContentRatingsDataModel.getContentRatings();
            if(contentRatingDataModels != null && contentRatingDataModels.size() > 0){
                for(ContentRatingDataModel contentRatingDataModel : contentRatingDataModels){
                    String iso31661 = contentRatingDataModel.getIso31661();
                    if(iso31661.equals(ISO_31661)){
                        rating = contentRatingDataModel.getRating();
                        break;
                    }
                }
            }
        }

        tvShowDetailsDomainModel.setCast(tvShowCreditDomainModelMapper.mapListToDomainModelList(cast));
        tvShowDetailsDomainModel.setCrew(tvShowCreditDomainModelMapper.mapListToDomainModelList(crew));
        tvShowDetailsDomainModel.setRating(rating);
        tvShowDetailsDomainModel.setSimilarTelevisionShows(tvShowDomainModelMapper.mapListToDomainModelList(similarTelevisionShows));
        tvShowDetailsDomainModel.setTelevisionShow(tvShowDomainModelMapper.mapToDomainModel(tvShowDataModel));

        return tvShowDetailsDomainModel;
    }
}
