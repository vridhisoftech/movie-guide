package com.vss.movieguide.domain.mappers;


public interface DomainModelMapper<DataModel, DomainModel> {
    DomainModel mapToDomainModel(DataModel dataModel);
}