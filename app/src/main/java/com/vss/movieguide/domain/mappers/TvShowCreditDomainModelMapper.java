package com.vss.movieguide.domain.mappers;

import com.vss.movieguide.data.repositories.models.TvShowCreditDataModel;
import com.vss.movieguide.domain.models.TvShowCreditDomainModel;

import java.util.ArrayList;
import java.util.List;

public class TvShowCreditDomainModelMapper implements DomainModelMapper<TvShowCreditDataModel, TvShowCreditDomainModel>, DomainModelListMapper<TvShowCreditDataModel, TvShowCreditDomainModel> {

    @Override
    public TvShowCreditDomainModel mapToDomainModel(TvShowCreditDataModel tvShowCreditDataModel) {
        TvShowCreditDomainModel tvShowCreditDomainModel = new TvShowCreditDomainModel();
        tvShowCreditDomainModel.setCharacter(tvShowCreditDataModel.getCharacter());
        tvShowCreditDomainModel.setDepartment(tvShowCreditDataModel.getDepartment());
        tvShowCreditDomainModel.setJob(tvShowCreditDataModel.getJob());
        tvShowCreditDomainModel.setName(tvShowCreditDataModel.getName());
        tvShowCreditDomainModel.setProfilePath(tvShowCreditDataModel.getProfilePath());
        tvShowCreditDomainModel.setCreditId(tvShowCreditDataModel.getCreditId());
        tvShowCreditDomainModel.setId(tvShowCreditDataModel.getId());
        return tvShowCreditDomainModel;
    }

    @Override
    public List<TvShowCreditDomainModel> mapListToDomainModelList(List<TvShowCreditDataModel> tvShowCreditDataModels) {
        List<TvShowCreditDomainModel> tvShowCreditDomainModels = new ArrayList<>();
        if(tvShowCreditDataModels != null && tvShowCreditDataModels.size()>0) {
            for (TvShowCreditDataModel tvShowCreditDataModel : tvShowCreditDataModels) {
                tvShowCreditDomainModels.add(mapToDomainModel(tvShowCreditDataModel));
            }
        }
        return tvShowCreditDomainModels;
    }
}
