package com.vss.movieguide.domain.usecases;

import com.vss.movieguide.domain.models.SearchDomainModel;

import io.reactivex.Single;

public interface SearchDomainContract {

    interface UseCase {
        Single<SearchDomainModel> getSearchResponse(String query);
    }
}
