package com.vss.movieguide.domain.usecases;

import com.vss.movieguide.domain.models.TvShowsDomainModel;

import io.reactivex.Single;

public interface TvShowsDomainContract {

    interface UseCase {
        Single<TvShowsDomainModel> getPopularTelevisionShows(int currentPage);
    }
}
