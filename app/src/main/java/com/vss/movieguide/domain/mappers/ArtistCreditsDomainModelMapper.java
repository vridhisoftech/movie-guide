package com.vss.movieguide.domain.mappers;

import com.vss.movieguide.data.repositories.models.ArtistCreditsDataModel;
import com.vss.movieguide.domain.models.ArtistCreditsDomainModel;

public class ArtistCreditsDomainModelMapper implements DomainModelMapper<ArtistCreditsDataModel, ArtistCreditsDomainModel> {

    // region Member Variables
    private ArtistCreditDomainModelMapper artistCreditDomainModelMapper = new ArtistCreditDomainModelMapper();
    // endregion

    @Override
    public ArtistCreditsDomainModel mapToDomainModel(ArtistCreditsDataModel artistCreditsDataModel) {
        ArtistCreditsDomainModel artistCreditsDomainModel = new ArtistCreditsDomainModel();
        artistCreditsDomainModel.setCast(artistCreditDomainModelMapper.mapListToDomainModelList(artistCreditsDataModel.getCast()));
        artistCreditsDomainModel.setCrew(artistCreditDomainModelMapper.mapListToDomainModelList(artistCreditsDataModel.getCrew()));
        artistCreditsDomainModel.setId(artistCreditsDataModel.getId());
        return artistCreditsDomainModel;
    }
}
