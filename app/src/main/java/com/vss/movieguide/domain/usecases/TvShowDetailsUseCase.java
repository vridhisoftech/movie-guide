package com.vss.movieguide.domain.usecases;

import com.vss.movieguide.data.repositories.tv.TvShowDataSourceContract;
import com.vss.movieguide.domain.composers.TvShowDetailsDomainModelComposer;
import com.vss.movieguide.domain.models.TvShowDetailsDomainModel;

import io.reactivex.Single;

public class TvShowDetailsUseCase implements TvShowDetailsDomainContract.UseCase {

    // region Member Variables
    private final TvShowDataSourceContract.Repository televisionShowRepository;
    private final TvShowDetailsDomainModelComposer tvShowDetailsDomainModelComposer = new TvShowDetailsDomainModelComposer();
    // endregion

    // region Constructors
    public TvShowDetailsUseCase(TvShowDataSourceContract.Repository televisionShowRepository) {
        this.televisionShowRepository = televisionShowRepository;
    }
    // endregion

    // region MovieDetailsDomainContract.UseCase Methods
    @Override
    public Single<TvShowDetailsDomainModel> getTelevisionShowDetails(int televisionShowId) {
        return Single.zip(
                televisionShowRepository.getTelevisionShow(televisionShowId),
                televisionShowRepository.getTelevisionShowCredits(televisionShowId),
                televisionShowRepository.getSimilarTelevisionShows(televisionShowId),
                televisionShowRepository.getTelevisionShowContentRatings(televisionShowId),
                tvShowDetailsDomainModelComposer::compose);
    }
    // endregion

}
