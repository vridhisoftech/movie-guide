package com.vss.movieguide.domain.mappers;

import com.vss.movieguide.data.repositories.models.ArtistDataModel;
import com.vss.movieguide.data.repositories.models.ArtistsDataModel;
import com.vss.movieguide.domain.models.ArtistsDomainModel;

import java.util.Calendar;
import java.util.List;

public class ArtistsDomainModelMapper implements DomainModelMapper<ArtistsDataModel, ArtistsDomainModel> {

    // region Constants
    private static final int PAGE_SIZE = 20;
    private static final int SEVEN_DAYS = 7;
    // endregion

    // region Member Variables
    private ArtistDomainModelMapper artistDomainModelMapper = new ArtistDomainModelMapper();
    // endregion

    @Override
    public ArtistsDomainModel mapToDomainModel(ArtistsDataModel artistsDataModel) {
        ArtistsDomainModel artistsDomainModel = new ArtistsDomainModel();
        List<ArtistDataModel> artistDataModels = artistsDataModel.getPersons();
        artistsDomainModel.setLastPage(artistDataModels.size() < PAGE_SIZE);
        artistsDomainModel.setPageNumber(artistsDataModel.getPageNumber());
        artistsDomainModel.setPersons(artistDomainModelMapper.mapListToDomainModelList(artistsDataModel.getPersons()));
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, SEVEN_DAYS);
        artistsDomainModel.setExpiredAt(calendar.getTime());
        return artistsDomainModel;
    }
}
