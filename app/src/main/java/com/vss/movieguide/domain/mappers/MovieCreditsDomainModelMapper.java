package com.vss.movieguide.domain.mappers;

import com.vss.movieguide.data.repositories.models.MovieCreditsDataModel;
import com.vss.movieguide.domain.models.MovieCreditsDomainModel;

import java.util.Calendar;

public class MovieCreditsDomainModelMapper implements DomainModelMapper<MovieCreditsDataModel, MovieCreditsDomainModel> {

    // region Constants
    private static final int THIRTY_DAYS = 30;
    // endregion

    // region Member Variables
    private MovieCreditDomainModelMapper movieCreditDomainModelMapper = new MovieCreditDomainModelMapper();
    // endregion

    @Override
    public MovieCreditsDomainModel mapToDomainModel(MovieCreditsDataModel movieCreditsDataModel) {
        MovieCreditsDomainModel movieCreditsDomainModel = new MovieCreditsDomainModel();
        movieCreditsDomainModel.setCast(movieCreditDomainModelMapper.mapListToDomainModelList(movieCreditsDataModel.getCast()));
        movieCreditsDomainModel.setCrew(movieCreditDomainModelMapper.mapListToDomainModelList(movieCreditsDataModel.getCrew()));
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, THIRTY_DAYS);
        movieCreditsDomainModel.setExpiredAt(calendar.getTime());
        movieCreditsDomainModel.setId(movieCreditsDataModel.getId());
        return movieCreditsDomainModel;
    }
}
