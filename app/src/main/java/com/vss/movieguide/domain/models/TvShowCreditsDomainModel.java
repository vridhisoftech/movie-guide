package com.vss.movieguide.domain.models;

import java.util.List;

public class TvShowCreditsDomainModel {

    // region Fields
    public int id;
    public List<TvShowCreditDomainModel> cast = null;
    public List<TvShowCreditDomainModel> crew = null;
    // endregion

    // region Getters

    public int getId() {
        return id;
    }

    public List<TvShowCreditDomainModel> getCast() {
        return cast;
    }

    public List<TvShowCreditDomainModel> getCrew() {
        return crew;
    }

    // endregion

    // region Setters

    public void setId(int id) {
        this.id = id;
    }

    public void setCast(List<TvShowCreditDomainModel> cast) {
        this.cast = cast;
    }

    public void setCrew(List<TvShowCreditDomainModel> crew) {
        this.crew = crew;
    }

    // endregion

    @Override
    public String toString() {
        return "TelevisionShowCreditsDomainModel{" +
                "id=" + id +
                ", cast=" + cast +
                ", crew=" + crew +
                '}';
    }
}
