package com.vss.movieguide.domain.mappers;

import com.vss.movieguide.data.repositories.models.ArtistDataModel;
import com.vss.movieguide.domain.models.ArtistDomainModel;

import java.util.ArrayList;
import java.util.List;

public class ArtistDomainModelMapper implements DomainModelMapper<ArtistDataModel, ArtistDomainModel>, DomainModelListMapper<ArtistDataModel, ArtistDomainModel> {

    // region Member Variables
    private ProfileImagesDomainModelMapper profileImagesDomainModelMapper = new ProfileImagesDomainModelMapper();
    // endregion

    @Override
    public ArtistDomainModel mapToDomainModel(ArtistDataModel artistDataModel) {
        ArtistDomainModel artistDomainModel = new ArtistDomainModel();
        artistDomainModel.setBiography(artistDataModel.getBiography());
        artistDomainModel.setBirthday(artistDataModel.getBirthday());
        artistDomainModel.setDeathday(artistDataModel.getDeathday());
        artistDomainModel.setId(artistDataModel.getId());
        artistDomainModel.setImages(profileImagesDomainModelMapper.mapToDomainModel(artistDataModel.getImages()));
        artistDomainModel.setImdbId(artistDataModel.getImdbId());
        artistDomainModel.setName(artistDataModel.getName());
        artistDomainModel.setPlaceOfBirth(artistDataModel.getPlaceOfBirth());
        artistDomainModel.setProfilePath(artistDataModel.getProfilePath());

        return artistDomainModel;
    }

    @Override
    public List<ArtistDomainModel> mapListToDomainModelList(List<ArtistDataModel> artistDataModels) {
        List<ArtistDomainModel> artistDomainModels = new ArrayList<>();
        if(artistDataModels != null && artistDataModels.size()>0) {
            for (ArtistDataModel artistDataModel : artistDataModels) {
                artistDomainModels.add(mapToDomainModel(artistDataModel));
            }
        }
        return artistDomainModels;
    }
}
