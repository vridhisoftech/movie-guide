package com.vss.movieguide.domain.mappers;

import com.vss.movieguide.data.repositories.models.TvShowsDataModel;
import com.vss.movieguide.domain.models.TvShowsDomainModel;

import java.util.Calendar;

public class TvShowsDomainModelMapper implements DomainModelMapper<TvShowsDataModel, TvShowsDomainModel> {

    // region Constants
    private static final int PAGE_SIZE = 20;
    private static final int SEVEN_DAYS = 7;
    // endregion

    // region Member Variables
    private TvShowDomainModelMapper tvShowDomainModelMapper = new TvShowDomainModelMapper();
    // endregion

    @Override
    public TvShowsDomainModel mapToDomainModel(TvShowsDataModel tvShowsDataModel) {
        TvShowsDomainModel tvShowsDomainModel = new TvShowsDomainModel();
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, SEVEN_DAYS);
        tvShowsDomainModel.setExpiredAt(calendar.getTime());
        tvShowsDomainModel.setLastPage(tvShowsDataModel.getTelevisionShows().size() < PAGE_SIZE);
        tvShowsDomainModel.setTelevisionShows(tvShowDomainModelMapper.mapListToDomainModelList(tvShowsDataModel.getTelevisionShows()));
        tvShowsDomainModel.setPageNumber(tvShowsDataModel.getPageNumber());
        return tvShowsDomainModel;
    }
}
