package com.vss.movieguide.domain.usecases;

import com.vss.movieguide.data.repositories.tv.TvShowDataSourceContract;
import com.vss.movieguide.domain.mappers.TvShowsDomainModelMapper;
import com.vss.movieguide.domain.models.TvShowsDomainModel;

import io.reactivex.Single;

public class TvShowsUseCase implements TvShowsDomainContract.UseCase {

    // region Member Variables
    private final TvShowDataSourceContract.Repository televisionShowRepository;
    private TvShowsDomainModelMapper tvShowsDomainModelMapper = new TvShowsDomainModelMapper();
    // endregion

    // region Constructors
    public TvShowsUseCase(TvShowDataSourceContract.Repository televisionShowRepository) {
        this.televisionShowRepository = televisionShowRepository;
    }
    // endregion

    // region TelevisionShowsDomainContract.UseCase Methods
    @Override
    public Single<TvShowsDomainModel> getPopularTelevisionShows(int currentPage) {
        return televisionShowRepository.getPopularTelevisionShows(currentPage)
                .map(televisionShowsDataModel -> tvShowsDomainModelMapper.mapToDomainModel(televisionShowsDataModel));
                // write unit tests for the mapper
    }

    // endregion
}
