package com.vss.movieguide.domain.usecases;

import com.vss.movieguide.domain.models.MoviesDomainModel;

import io.reactivex.Single;

public interface MoviesDomainContract {

    interface UseCase {
        Single<MoviesDomainModel> getPopularMovies(int currentPage);
    }
}
