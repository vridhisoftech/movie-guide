package com.vss.movieguide.domain.models;

import java.util.List;

public class TvShowDetailsDomainModel {

    // region Fields
    private TvShowDomainModel televisionShow;
    private List<TvShowCreditDomainModel> cast;
    private List<TvShowCreditDomainModel> crew;
    private List<TvShowDomainModel> similarTelevisionShows;
    private String rating;
    // endregion

    // region Constructors

    public TvShowDetailsDomainModel() {
    }

    // endregion

    // region Getters


    public TvShowDomainModel getTelevisionShow() {
        return televisionShow;
    }

    public List<TvShowCreditDomainModel> getCast() {
        return cast;
    }

    public List<TvShowCreditDomainModel> getCrew() {
        return crew;
    }

    public List<TvShowDomainModel> getSimilarTelevisionShows() {
        return similarTelevisionShows;
    }

    public String getRating() {
        return rating;
    }

    // endregion

    // region Setters

    public void setTelevisionShow(TvShowDomainModel televisionShow) {
        this.televisionShow = televisionShow;
    }

    public void setCast(List<TvShowCreditDomainModel> cast) {
        this.cast = cast;
    }

    public void setCrew(List<TvShowCreditDomainModel> crew) {
        this.crew = crew;
    }

    public void setSimilarTelevisionShows(List<TvShowDomainModel> similarTelevisionShows) {
        this.similarTelevisionShows = similarTelevisionShows;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    // endregion

    @Override
    public String toString() {
        return "TelevisionShowDetailsDomainModel{" +
                "televisionShow=" + televisionShow +
                ", cast=" + cast +
                ", crew=" + crew +
                ", similarTelevisionShows=" + similarTelevisionShows +
                ", rating='" + rating + '\'' +
                '}';
    }
}
