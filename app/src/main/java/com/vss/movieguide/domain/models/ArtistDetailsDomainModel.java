package com.vss.movieguide.domain.models;

import java.util.List;

public class ArtistDetailsDomainModel {

    // region Fields
    private ArtistDomainModel person;
    private List<ArtistCreditDomainModel> cast;
    private List<ArtistCreditDomainModel> crew;
    // endregion

    // region Constructors
    public ArtistDetailsDomainModel() {
    }
    // endregion

    // region Getters

    public ArtistDomainModel getPerson() {
        return person;
    }

    public List<ArtistCreditDomainModel> getCast() {
        return cast;
    }

    public List<ArtistCreditDomainModel> getCrew() {
        return crew;
    }

    // endregion

    // region Setters

    public void setPerson(ArtistDomainModel person) {
        this.person = person;
    }

    public void setCast(List<ArtistCreditDomainModel> cast) {
        this.cast = cast;
    }

    public void setCrew(List<ArtistCreditDomainModel> crew) {
        this.crew = crew;
    }

    // endregion

    @Override
    public String toString() {
        return "PersonDetailsDomainModel{" +
                "person=" + person +
                ", cast=" + cast +
                ", crew=" + crew +
                '}';
    }
}
