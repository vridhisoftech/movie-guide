package com.vss.movieguide.domain.usecases;

import com.vss.movieguide.domain.models.ArtistDetailsDomainModel;

import io.reactivex.Single;

public interface ArtistDetailsDomainContract {

    interface UseCase {
        Single<ArtistDetailsDomainModel> getPersonDetails(int personId);
    }
}
