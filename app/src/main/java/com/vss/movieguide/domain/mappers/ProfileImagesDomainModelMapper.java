package com.vss.movieguide.domain.mappers;

import com.vss.movieguide.data.repositories.models.ProfileImagesDataModel;
import com.vss.movieguide.domain.models.ProfileImagesDomainModel;

public class ProfileImagesDomainModelMapper implements DomainModelMapper<ProfileImagesDataModel, ProfileImagesDomainModel> {

    // region Member Variables
    private ProfileImageDomainModelMapper profileImageDomainModelMapper = new ProfileImageDomainModelMapper();
    // endregion

    @Override
    public ProfileImagesDomainModel mapToDomainModel(ProfileImagesDataModel profileImagesDataModel) {
        ProfileImagesDomainModel profileImagesDomainModel = new ProfileImagesDomainModel();
        if(profileImagesDataModel != null){
            profileImagesDomainModel.setProfiles(profileImageDomainModelMapper.mapListToDomainModelList(profileImagesDataModel.getProfiles()));
        }
        return profileImagesDomainModel;
    }
}
