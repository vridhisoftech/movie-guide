package com.vss.movieguide.domain.composers;

import com.vss.movieguide.data.repositories.models.ArtistCreditDataModel;
import com.vss.movieguide.data.repositories.models.ArtistCreditsDataModel;
import com.vss.movieguide.data.repositories.models.ArtistDataModel;
import com.vss.movieguide.domain.mappers.ArtistCreditDomainModelMapper;
import com.vss.movieguide.domain.mappers.ArtistDomainModelMapper;
import com.vss.movieguide.domain.models.ArtistDetailsDomainModel;

import java.util.ArrayList;
import java.util.List;

public class ArtistDetailsDomainModelComposer {

    private ArtistCreditDomainModelMapper artistCreditDomainModelMapper = new ArtistCreditDomainModelMapper();
    private ArtistDomainModelMapper artistDomainModelMapper = new ArtistDomainModelMapper();

    public ArtistDetailsDomainModel compose(ArtistDataModel artistDataModel, ArtistCreditsDataModel artistCreditsDataModel){
        ArtistDetailsDomainModel artistDetailsDomainModel = new ArtistDetailsDomainModel();
        List<ArtistCreditDataModel> cast = new ArrayList<>();
        List<ArtistCreditDataModel> crew = new ArrayList<>();

        if(artistCreditsDataModel !=null){
            cast = artistCreditsDataModel.getCast();
        }

        if(artistCreditsDataModel !=null){
            crew = artistCreditsDataModel.getCrew();
        }
        artistDetailsDomainModel.setCast(artistCreditDomainModelMapper.mapListToDomainModelList(cast));
        artistDetailsDomainModel.setCrew(artistCreditDomainModelMapper.mapListToDomainModelList(crew));
        artistDetailsDomainModel.setPerson(artistDomainModelMapper.mapToDomainModel(artistDataModel));
        return artistDetailsDomainModel;
    }
}
