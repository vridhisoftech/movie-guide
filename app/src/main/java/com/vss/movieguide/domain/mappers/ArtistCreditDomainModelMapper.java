package com.vss.movieguide.domain.mappers;

import com.vss.movieguide.data.repositories.models.ArtistCreditDataModel;
import com.vss.movieguide.domain.models.ArtistCreditDomainModel;

import java.util.ArrayList;
import java.util.List;

public class ArtistCreditDomainModelMapper implements DomainModelMapper<ArtistCreditDataModel, ArtistCreditDomainModel>, DomainModelListMapper<ArtistCreditDataModel, ArtistCreditDomainModel> {

    @Override
    public ArtistCreditDomainModel mapToDomainModel(ArtistCreditDataModel artistCreditDataModel) {
        ArtistCreditDomainModel artistCreditDomainModel = new ArtistCreditDomainModel();
        artistCreditDomainModel.setCharacter(artistCreditDataModel.getCharacter());
        artistCreditDomainModel.setDepartment(artistCreditDataModel.getDepartment());
        artistCreditDomainModel.setFirstAirDate(artistCreditDataModel.getFirstAirDate());
        artistCreditDomainModel.setJob(artistCreditDataModel.getJob());
        artistCreditDomainModel.setMediaType(artistCreditDataModel.getMediaType());
        artistCreditDomainModel.setName(artistCreditDataModel.getName());
        artistCreditDomainModel.setPosterPath(artistCreditDataModel.getPosterPath());
        artistCreditDomainModel.setReleaseDate(artistCreditDataModel.getReleaseDate());
        artistCreditDomainModel.setTitle(artistCreditDataModel.getTitle());
        artistCreditDomainModel.setCreditId(artistCreditDataModel.getCreditId());
        artistCreditDomainModel.setId(artistCreditDataModel.getId());
        return artistCreditDomainModel;
    }

    @Override
    public List<ArtistCreditDomainModel> mapListToDomainModelList(List<ArtistCreditDataModel> artistCreditDataModels) {
        List<ArtistCreditDomainModel> artistCreditDomainModels = new ArrayList<>();
        if(artistCreditDataModels != null && artistCreditDataModels.size()>0) {
            for (ArtistCreditDataModel artistCreditDataModel : artistCreditDataModels) {
                artistCreditDomainModels.add(mapToDomainModel(artistCreditDataModel));
            }
        }
        return artistCreditDomainModels;
    }
}
