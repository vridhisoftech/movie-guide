package com.vss.movieguide.domain.models;

import java.util.Date;
import java.util.List;

public class TvShowsDomainModel {

    // region Fields
    private List<TvShowDomainModel> televisionShows;
    private int pageNumber;
    private boolean isLastPage;
    private Date expiredAt;
    // endregion

    // region Constructors

    public TvShowsDomainModel() {
    }

    // endregion

    // region Getters

    public List<TvShowDomainModel> getTelevisionShows() {
        return televisionShows;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public boolean isLastPage() {
        return isLastPage;
    }

    public Date getExpiredAt() {
        return expiredAt;
    }

    // endregion

    // region Setters

    public void setTelevisionShows(List<TvShowDomainModel> televisionShows) {
        this.televisionShows = televisionShows;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public void setLastPage(boolean lastPage) {
        isLastPage = lastPage;
    }

    public void setExpiredAt(Date expiredAt) {
        this.expiredAt = expiredAt;
    }

    // endregion

    // region Helper Methods
    public boolean hasTelevisionShows() { return televisionShows.size() > 0;}
    // endregion

    @Override
    public String toString() {
        return "TelevisionShowsDomainModel{" +
                "televisionShows=" + televisionShows +
                ", pageNumber=" + pageNumber +
                ", isLastPage=" + isLastPage +
                ", expiredAt=" + expiredAt +
                '}';
    }
}
