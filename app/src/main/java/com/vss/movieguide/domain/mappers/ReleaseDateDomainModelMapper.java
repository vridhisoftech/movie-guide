package com.vss.movieguide.domain.mappers;

import com.vss.movieguide.data.repositories.models.ReleaseDateDataModel;
import com.vss.movieguide.domain.models.ReleaseDateDomainModel;

import java.util.ArrayList;
import java.util.List;

public class ReleaseDateDomainModelMapper implements DomainModelMapper<ReleaseDateDataModel, ReleaseDateDomainModel>, DomainModelListMapper<ReleaseDateDataModel, ReleaseDateDomainModel> {

    @Override
    public ReleaseDateDomainModel mapToDomainModel(ReleaseDateDataModel releaseDateDataModel) {
        ReleaseDateDomainModel releaseDateDomainModel = new ReleaseDateDomainModel();
        releaseDateDomainModel.setCertification(releaseDateDataModel.getCertification());
        releaseDateDomainModel.setIso6391(releaseDateDataModel.getIso6391());
        releaseDateDomainModel.setNote(releaseDateDataModel.getNote());
        releaseDateDomainModel.setReleaseDate(releaseDateDataModel.getReleaseDate());
        releaseDateDomainModel.setType(releaseDateDataModel.getType());
        return releaseDateDomainModel;
    }

    @Override
    public List<ReleaseDateDomainModel> mapListToDomainModelList(List<ReleaseDateDataModel> releaseDateDataModels) {
        List<ReleaseDateDomainModel> releaseDateDomainModels = new ArrayList<>();
        if(releaseDateDataModels != null && releaseDateDataModels.size()>0) {
            for (ReleaseDateDataModel releaseDateDataModel : releaseDateDataModels) {
                releaseDateDomainModels.add(mapToDomainModel(releaseDateDataModel));
            }
        }
        return releaseDateDomainModels;
    }
}
