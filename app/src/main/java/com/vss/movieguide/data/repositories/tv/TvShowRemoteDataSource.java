package com.vss.movieguide.data.repositories.tv;

import com.vss.movieguide.data.network.MovieGuideService;
import com.vss.movieguide.data.network.response.TelevisionShowContentRatingsResponse;
import com.vss.movieguide.data.network.response.TelevisionShowCreditsResponse;
import com.vss.movieguide.data.network.response.TelevisionShowResponse;
import com.vss.movieguide.data.network.response.TelevisionShowsResponse;

import javax.inject.Inject;

import io.reactivex.Single;

public class TvShowRemoteDataSource implements TvShowDataSourceContract.RemoteDateSource {

    // region Member Variables
    private MovieGuideService movieGuideService;
    // endregion

    // region Constructors
    @Inject
    public TvShowRemoteDataSource(MovieGuideService movieGuideService) {
        this.movieGuideService = movieGuideService;
    }
    // endregion

    // region TelevisionShowDataSourceContract.RemoteDateSource Methods
    @Override
    public Single<TelevisionShowsResponse> getPopularTelevisionShows(int currentPage) {
        return movieGuideService.getPopularTelevisionShows(currentPage);
    }

    @Override
    public Single<TelevisionShowResponse> getTelevisionShow(long tvId) {
        return movieGuideService.getTelevisionShow(tvId);
    }

    @Override
    public Single<TelevisionShowCreditsResponse> getTelevisionShowCredits(long tvId) {
        return movieGuideService.getTelevisionShowCredits(tvId);
    }

    @Override
    public Single<TelevisionShowsResponse> getSimilarTelevisionShows(long tvId) {
        return movieGuideService.getSimilarTelevisionShows(tvId);
    }

    @Override
    public Single<TelevisionShowContentRatingsResponse> getTelevisionShowContentRatings(long tvId) {
        return movieGuideService.getTelevisionShowContentRatings(tvId);
    }

    // endregion
}
