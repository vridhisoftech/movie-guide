package com.vss.movieguide.data.repositories.tv;

import com.vss.movieguide.data.network.response.TelevisionShowContentRatingsResponse;
import com.vss.movieguide.data.network.response.TelevisionShowCreditsResponse;
import com.vss.movieguide.data.network.response.TelevisionShowResponse;
import com.vss.movieguide.data.network.response.TelevisionShowsResponse;
import com.vss.movieguide.data.repositories.models.TvShowContentRatingsDataModel;
import com.vss.movieguide.data.repositories.models.TvShowCreditsDataModel;
import com.vss.movieguide.data.repositories.models.TvShowDataModel;
import com.vss.movieguide.data.repositories.models.TvShowsDataModel;

import io.reactivex.Maybe;
import io.reactivex.Single;

public interface TvShowDataSourceContract {

    interface Repository {
        Single<TvShowsDataModel> getPopularTelevisionShows(int currentPage);
        Single<TvShowDataModel> getTelevisionShow(long tvId);
        Single<TvShowCreditsDataModel> getTelevisionShowCredits(long tvId);
        Single<TvShowsDataModel> getSimilarTelevisionShows(long tvId);
        Single<TvShowContentRatingsDataModel> getTelevisionShowContentRatings(long tvId);
    }

    interface LocalDateSource {
        Maybe<TvShowsDataModel> getPopularTelevisionShows(int currentPage);
        void savePopularTelevisionShows(TvShowsDataModel tvShowsDataModel);
        Maybe<TvShowDataModel> getTelevisionShow(long tvId);
        void saveTelevisionShow(TvShowDataModel tvShowDataModel);
        Maybe<TvShowCreditsDataModel> getTelevisionShowCredits(long tvId);
        void saveTelevisionShowCredits(TvShowCreditsDataModel tvShowCreditsDataModel);
        Maybe<TvShowsDataModel> getSimilarTelevisionShows(long tvId);
        void saveSimilarTelevisionShows(TvShowsDataModel tvShowsDataModel);
        Maybe<TvShowContentRatingsDataModel> getTelevisionShowContentRatings(long tvId);
        void saveTelevisionShowContentRatings(TvShowContentRatingsDataModel televisionShowContentRatingsResponse);
    }

    interface RemoteDateSource {
        Single<TelevisionShowsResponse> getPopularTelevisionShows(int currentPage);
        Single<TelevisionShowResponse> getTelevisionShow(long tvId);
        Single<TelevisionShowCreditsResponse> getTelevisionShowCredits(long tvId);
        Single<TelevisionShowsResponse> getSimilarTelevisionShows(long tvId);
        Single<TelevisionShowContentRatingsResponse> getTelevisionShowContentRatings(long tvId);
    }
}
