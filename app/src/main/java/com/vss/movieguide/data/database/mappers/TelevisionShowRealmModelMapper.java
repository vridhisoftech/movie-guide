package com.vss.movieguide.data.database.mappers;

import com.vss.movieguide.data.database.models.TelevisionShowRealmModel;
import com.vss.movieguide.data.repositories.models.TvShowDataModel;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;

public class TelevisionShowRealmModelMapper implements RealmModelMapper<TvShowDataModel, TelevisionShowRealmModel>, RealmModelListMapper<TvShowDataModel, TelevisionShowRealmModel> {

    private GenreRealmModelMapper genreRealmMapper = new GenreRealmModelMapper();
    private NetworkRealmModelMapper networkRealmMapper = new NetworkRealmModelMapper();
    private StringRealmModelMapper stringRealmMapper = new StringRealmModelMapper();
    private IntegerRealmModelMapper integerRealmMapper = new IntegerRealmModelMapper();

    @Override
    public TelevisionShowRealmModel mapToRealmModel(TvShowDataModel tvShowDataModel) {
        TelevisionShowRealmModel realmTelevisionShow = Realm.getDefaultInstance().createObject(TelevisionShowRealmModel.class);

        realmTelevisionShow.setBackdropPath(tvShowDataModel.getBackdropPath());
        realmTelevisionShow.setEpisodeRunTime(integerRealmMapper.mapListToRealmModelList(tvShowDataModel.getEpisodeRunTime()));
        realmTelevisionShow.setFirstAirDate(tvShowDataModel.getFirstAirDate());
        realmTelevisionShow.setGenres(genreRealmMapper.mapListToRealmModelList(tvShowDataModel.getGenres()));
        realmTelevisionShow.setHomepage(tvShowDataModel.getHomepage());
        realmTelevisionShow.setId(tvShowDataModel.getId());
        realmTelevisionShow.setInProduction(tvShowDataModel.isInProduction());
        realmTelevisionShow.setLanguages(stringRealmMapper.mapListToRealmModelList(tvShowDataModel.getLanguages()));
        realmTelevisionShow.setLastAirDate(tvShowDataModel.getLastAirDate());
        realmTelevisionShow.setName(tvShowDataModel.getName());
        realmTelevisionShow.setNetworks(networkRealmMapper.mapListToRealmModelList(tvShowDataModel.getNetworks()));
        realmTelevisionShow.setNumberOfEpisodes(tvShowDataModel.getNumberOfEpisodes());
        realmTelevisionShow.setNumberOfSeasons(tvShowDataModel.getNumberOfSeasons());
        realmTelevisionShow.setOriginCountry(stringRealmMapper.mapListToRealmModelList(tvShowDataModel.getOriginCountry()));
        realmTelevisionShow.setOriginalLanguage(tvShowDataModel.getOriginalLanguage());
        realmTelevisionShow.setOriginalName(tvShowDataModel.getOriginalName());
        realmTelevisionShow.setOverview(tvShowDataModel.getOverview());
        realmTelevisionShow.setPopularity(tvShowDataModel.getPopularity());
        realmTelevisionShow.setPosterPath(tvShowDataModel.getPosterPath());
        realmTelevisionShow.setStatus(tvShowDataModel.getStatus());
        realmTelevisionShow.setType(tvShowDataModel.getType());
        realmTelevisionShow.setVoteAverage(tvShowDataModel.getVoteAverage());
        realmTelevisionShow.setVoteCount(tvShowDataModel.getVoteCount());

        return realmTelevisionShow;
    }

    @Override
    public TvShowDataModel mapFromRealmModel(TelevisionShowRealmModel televisionShowRealmModel) {
        TvShowDataModel tvShowDataModel = new TvShowDataModel();
        tvShowDataModel.setBackdropPath(televisionShowRealmModel.getBackdropPath());
        tvShowDataModel.setEpisodeRunTime(integerRealmMapper.mapListFromRealmModelList(televisionShowRealmModel.getEpisodeRunTime()));
        tvShowDataModel.setFirstAirDate(televisionShowRealmModel.getFirstAirDate());
        tvShowDataModel.setGenres(genreRealmMapper.mapListFromRealmModelList(televisionShowRealmModel.getGenres()));
        tvShowDataModel.setHomepage(televisionShowRealmModel.getHomepage());
        tvShowDataModel.setId(televisionShowRealmModel.getId());
        tvShowDataModel.setInProduction(televisionShowRealmModel.isInProduction());
        tvShowDataModel.setLanguages(stringRealmMapper.mapListFromRealmModelList(televisionShowRealmModel.getLanguages()));
        tvShowDataModel.setLastAirDate(televisionShowRealmModel.getLastAirDate());
        tvShowDataModel.setName(televisionShowRealmModel.getName());
        tvShowDataModel.setNetworks(networkRealmMapper.mapListFromRealmModelList(televisionShowRealmModel.getNetworks()));
        tvShowDataModel.setNumberOfEpisodes(televisionShowRealmModel.getNumberOfEpisodes());
        tvShowDataModel.setNumberOfSeasons(televisionShowRealmModel.getNumberOfSeasons());
        tvShowDataModel.setOriginCountry(stringRealmMapper.mapListFromRealmModelList(televisionShowRealmModel.getOriginCountry()));
        tvShowDataModel.setOriginalLanguage(televisionShowRealmModel.getOriginalLanguage());
        tvShowDataModel.setOriginalName(televisionShowRealmModel.getOriginalName());
        tvShowDataModel.setOverview(televisionShowRealmModel.getOverview());
        tvShowDataModel.setPopularity(televisionShowRealmModel.getPopularity());
        tvShowDataModel.setPosterPath(televisionShowRealmModel.getPosterPath());
        tvShowDataModel.setStatus(televisionShowRealmModel.getStatus());
        tvShowDataModel.setType(televisionShowRealmModel.getType());
        tvShowDataModel.setVoteAverage(televisionShowRealmModel.getVoteAverage());
        tvShowDataModel.setVoteCount(televisionShowRealmModel.getVoteCount());

        return tvShowDataModel;
    }

    @Override
    public RealmList<TelevisionShowRealmModel> mapListToRealmModelList(List<TvShowDataModel> tvShowDataModels) {
        RealmList<TelevisionShowRealmModel> televisionShowRealmModels = new RealmList<>();
        for(TvShowDataModel tvShowDataModel : tvShowDataModels){
            televisionShowRealmModels.add(mapToRealmModel(tvShowDataModel));
        }
        return televisionShowRealmModels;
    }

    @Override
    public List<TvShowDataModel> mapListFromRealmModelList(RealmList<TelevisionShowRealmModel> televisionShowRealmModels) {
        List<TvShowDataModel> tvShowDataModels = new ArrayList<>();
        for(TelevisionShowRealmModel televisionShowRealmModel : televisionShowRealmModels){
            tvShowDataModels.add(mapFromRealmModel(televisionShowRealmModel));
        }
        return tvShowDataModels;
    }
}
