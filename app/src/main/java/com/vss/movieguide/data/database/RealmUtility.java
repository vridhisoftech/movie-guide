package com.vss.movieguide.data.database;

import com.vss.movieguide.data.database.mappers.MovieRealmModelMapper;
import com.vss.movieguide.data.database.mappers.PersonRealmModelMapper;
import com.vss.movieguide.data.database.mappers.TelevisionShowRealmModelMapper;
import com.vss.movieguide.data.database.models.ArtistsRealmModel;
import com.vss.movieguide.data.database.models.MoviesRealmModel;
import com.vss.movieguide.data.database.models.TelevisionShowsRealmModel;
import com.vss.movieguide.data.repositories.models.MovieDataModel;
import com.vss.movieguide.data.repositories.models.MoviesDataModel;
import com.vss.movieguide.data.repositories.models.ArtistDataModel;
import com.vss.movieguide.data.repositories.models.ArtistsDataModel;
import com.vss.movieguide.data.repositories.models.TvShowDataModel;
import com.vss.movieguide.data.repositories.models.TvShowsDataModel;

import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;


public class RealmUtility {

//    https://github.com/Innovatube/android-tdd-approach/blob/04c09ca0048c507e9492ff646b23b58e801dc9c0/app/src/main/java/com/example/androidtdd/data/model/Address.java

    private static final MovieRealmModelMapper movieRealmModelMapper = new MovieRealmModelMapper();
    private static final TelevisionShowRealmModelMapper televisionShowRealmModelMapper = new TelevisionShowRealmModelMapper();
    private static final PersonRealmModelMapper personRealmModelMapper = new PersonRealmModelMapper();

    public static MoviesDataModel getMoviesDataModel(int pageNumber){
        MoviesDataModel moviesDataModel = new MoviesDataModel();

        Realm realm = Realm.getDefaultInstance();
        try {
            RealmResults<MoviesRealmModel> realmResults
                    = realm.where(MoviesRealmModel.class).findAll();
            if(realmResults != null && realmResults.isValid() && realmResults.size() > 0){
                if(realmResults.size() == pageNumber-1)
                    return null;

                MoviesRealmModel moviesRealmModel = realmResults.get(pageNumber-1);
                moviesDataModel.setMovies(movieRealmModelMapper.mapListFromRealmModelList(moviesRealmModel.getMovies()));
                moviesDataModel.setPageNumber(moviesRealmModel.getPageNumber());
                moviesDataModel.setLastPage(moviesRealmModel.isLastPage());
                moviesDataModel.setExpiredAt(moviesRealmModel.getExpiredAt());

                return moviesDataModel;
            } else {
                return null;
            }
        } finally {
            realm.close();
        }
    }

    public static void saveMoviesDataModel(MoviesDataModel moviesDataModel){
        Realm realm = Realm.getDefaultInstance();
        try {
            List<MovieDataModel> movies = moviesDataModel.getMovies();
            int pageNumber = moviesDataModel.getPageNumber();
            boolean isLastPage = moviesDataModel.isLastPage();
            Date expiredAt = moviesDataModel.getExpiredAt();

            realm.executeTransaction(realm1 -> {
                MoviesRealmModel moviesRealmModel = new MoviesRealmModel();
                moviesRealmModel.setMovies(movieRealmModelMapper.mapListToRealmModelList(movies));
                moviesRealmModel.setPageNumber(pageNumber);
                moviesRealmModel.setLastPage(isLastPage);
                moviesRealmModel.setExpiredAt(expiredAt);

                realm1.copyToRealmOrUpdate(moviesRealmModel);
            });

        } catch (Exception e){
            e.printStackTrace();
        } finally {
            realm.close();
        }
    }

    public static TvShowsDataModel getTelevisionShowsDataModel(int pageNumber){
        TvShowsDataModel tvShowsDataModel = new TvShowsDataModel();

        Realm realm = Realm.getDefaultInstance();
        try {
            RealmResults<TelevisionShowsRealmModel> realmResults
                    = realm.where(TelevisionShowsRealmModel.class).findAll();
            if(realmResults != null && realmResults.isValid() && realmResults.size() > 0){
                if(realmResults.size() == pageNumber-1)
                    return null;

                TelevisionShowsRealmModel televisionShowsRealmModel = realmResults.get(pageNumber-1);
                tvShowsDataModel.setTelevisionShows(televisionShowRealmModelMapper.mapListFromRealmModelList(televisionShowsRealmModel.getTelevisionShows()));
                tvShowsDataModel.setPageNumber(televisionShowsRealmModel.getPageNumber());
                tvShowsDataModel.setLastPage(televisionShowsRealmModel.isLastPage());
                tvShowsDataModel.setExpiredAt(televisionShowsRealmModel.getExpiredAt());

                return tvShowsDataModel;
            } else {
                return null;
            }
        } finally {
            realm.close();
        }
    }

    public static void saveTelevisionShowsDataModel(TvShowsDataModel tvShowsDataModel){
        Realm realm = Realm.getDefaultInstance();
        try {
            List<TvShowDataModel> tvShowDataModels = tvShowsDataModel.getTelevisionShows();
            int pageNumber = tvShowsDataModel.getPageNumber();
            boolean isLastPage = tvShowsDataModel.isLastPage();
            Date expiredAt = tvShowsDataModel.getExpiredAt();

            realm.executeTransaction(realm1 -> {
                TelevisionShowsRealmModel televisionShowsRealmModel = new TelevisionShowsRealmModel();
                televisionShowsRealmModel.setTelevisionShows(televisionShowRealmModelMapper.mapListToRealmModelList(tvShowDataModels));
                televisionShowsRealmModel.setPageNumber(pageNumber);
                televisionShowsRealmModel.setLastPage(isLastPage);
                televisionShowsRealmModel.setExpiredAt(expiredAt);

                realm1.copyToRealmOrUpdate(televisionShowsRealmModel);
            });

        } catch (Exception e){
            e.printStackTrace();
        } finally {
            realm.close();
        }
    }

    public static ArtistsDataModel getPersonsDataModel(int pageNumber){
        ArtistsDataModel artistsDataModel = new ArtistsDataModel();

        Realm realm = Realm.getDefaultInstance();
        try {
            RealmResults<ArtistsRealmModel> realmResults
                    = realm.where(ArtistsRealmModel.class).findAll();
            if(realmResults != null && realmResults.isValid() && realmResults.size() > 0){
                if(realmResults.size() == pageNumber-1)
                    return null;

                ArtistsRealmModel artistsRealmModel = realmResults.get(pageNumber-1);
                artistsDataModel.setPersons(personRealmModelMapper.mapListFromRealmModelList(artistsRealmModel.getPersons()));
                artistsDataModel.setPageNumber(artistsRealmModel.getPageNumber());
                artistsDataModel.setLastPage(artistsRealmModel.isLastPage());
                artistsDataModel.setExpiredAt(artistsRealmModel.getExpiredAt());

                return artistsDataModel;
            } else {
                return null;
            }
        } finally {
            realm.close();
        }
    }

    public static void savePersonsDataModel(ArtistsDataModel artistsDataModel){
        Realm realm = Realm.getDefaultInstance();
        try {
            List<ArtistDataModel> persons = artistsDataModel.getPersons();
            int pageNumber = artistsDataModel.getPageNumber();
            boolean isLastPage = artistsDataModel.isLastPage();
            Date expiredAt = artistsDataModel.getExpiredAt();

            realm.executeTransaction(realm1 -> {
                ArtistsRealmModel artistsRealmModel = new ArtistsRealmModel();
                artistsRealmModel.setPersons(personRealmModelMapper.mapListToRealmModelList(persons));
                artistsRealmModel.setPageNumber(pageNumber);
                artistsRealmModel.setLastPage(isLastPage);
                artistsRealmModel.setExpiredAt(expiredAt);

                realm1.copyToRealmOrUpdate(artistsRealmModel);
            });

        } catch (Exception e){
            e.printStackTrace();
        } finally {
            realm.close();
        }
    }

}
