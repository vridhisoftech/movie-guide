package com.vss.movieguide.data.repositories.artist;

import com.vss.movieguide.data.database.RealmUtility;
import com.vss.movieguide.data.repositories.models.ArtistCreditsDataModel;
import com.vss.movieguide.data.repositories.models.ArtistDataModel;
import com.vss.movieguide.data.repositories.models.ArtistsDataModel;

import io.reactivex.Maybe;

public class ArtistLocalDataSource implements ArtistDataSourceContract.LocalDateSource {

    // region Constructors
    public ArtistLocalDataSource() {
    }
    // endregion

    // region PersonDataSourceContract.LocalDateSource Methods

    @Override
    public Maybe<ArtistsDataModel> getPopularPersons(int currentPage) {
        ArtistsDataModel artistsDataModel = RealmUtility.getPersonsDataModel(currentPage);
        if(artistsDataModel == null)
            return Maybe.empty();
        else
            return Maybe.just(artistsDataModel);
    }

    @Override
    public void savePopularPersons(ArtistsDataModel artistsDataModel) {
        RealmUtility.savePersonsDataModel(artistsDataModel);
    }

    @Override
    public Maybe<ArtistDataModel> getPerson(long personId) {
        //        Use mapper to convert from realm objects to POJOs
        return Maybe.empty();
    }

    @Override
    public void savePerson(ArtistDataModel person) {
//        Use mapper to convert from POJOs to realm objects
    }

    @Override
    public Maybe<ArtistCreditsDataModel> getPersonCredits(long personId) {
        //        Use mapper to convert from realm objects to POJOs
        return Maybe.empty();
    }

    @Override
    public void savePersonCredits(ArtistCreditsDataModel artistCreditsDataModel) {
//        Use mapper to convert from POJOs to realm objects
    }

    // endregion
}
