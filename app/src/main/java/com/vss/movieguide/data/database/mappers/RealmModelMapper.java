package com.vss.movieguide.data.database.mappers;

public interface RealmModelMapper<Pojo, RealmModel> {
    RealmModel mapToRealmModel(Pojo pojo);
    Pojo mapFromRealmModel(RealmModel realmModel);
}
