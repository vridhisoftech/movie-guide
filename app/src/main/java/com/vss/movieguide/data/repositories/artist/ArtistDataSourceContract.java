package com.vss.movieguide.data.repositories.artist;

import com.vss.movieguide.data.network.response.ArtistCreditsResponse;
import com.vss.movieguide.data.network.response.ArtistResponse;
import com.vss.movieguide.data.network.response.ArtistsResponse;
import com.vss.movieguide.data.repositories.models.ArtistCreditsDataModel;
import com.vss.movieguide.data.repositories.models.ArtistDataModel;
import com.vss.movieguide.data.repositories.models.ArtistsDataModel;

import io.reactivex.Maybe;
import io.reactivex.Single;

public interface ArtistDataSourceContract {

    interface Repository {
        Single<ArtistsDataModel> getPopularPersons(int currentPage);
        Single<ArtistDataModel> getPerson(long personId);
        Single<ArtistCreditsDataModel> getPersonCredits(long personId);
    }

    interface LocalDateSource {
        Maybe<ArtistsDataModel> getPopularPersons(int currentPage);
        void savePopularPersons(ArtistsDataModel artistsDataModel);
        Maybe<ArtistDataModel> getPerson(long personId);
        void savePerson(ArtistDataModel person);
        Maybe<ArtistCreditsDataModel> getPersonCredits(long personId);
        void savePersonCredits(ArtistCreditsDataModel personCreditsResponse);
    }

    interface RemoteDateSource {
        Single<ArtistsResponse> getPopularPersons(int currentPage);
        Single<ArtistResponse> getPerson(long personId);
        Single<ArtistCreditsResponse> getPersonCredits(long personId);
    }
}
