package com.vss.movieguide.data.repositories.mappers;

public interface DataModelMapper<NetworkResponseModel, DataModel> {
    DataModel mapToDataModel(NetworkResponseModel networkResponseModel);
}