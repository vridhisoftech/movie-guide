package com.vss.movieguide.data.repositories.models;

import java.util.List;


public class ArtistCreditsDataModel {

    // region Fields
    public int id;
    public List<ArtistCreditDataModel> cast = null;
    public List<ArtistCreditDataModel> crew = null;
    // endregion

    // region Getters

    public int getId() {
        return id;
    }

    public List<ArtistCreditDataModel> getCast() {
        return cast;
    }

    public List<ArtistCreditDataModel> getCrew() {
        return crew;
    }

    // endregion

    // region Setters

    public void setId(int id) {
        this.id = id;
    }

    public void setCast(List<ArtistCreditDataModel> cast) {
        this.cast = cast;
    }

    public void setCrew(List<ArtistCreditDataModel> crew) {
        this.crew = crew;
    }

    // endregion

    @Override
    public String toString() {
        return "PersonCreditsDataModel{" +
                "id=" + id +
                ", cast=" + cast +
                ", crew=" + crew +
                '}';
    }
}
