package com.vss.movieguide.data.repositories.search;

import com.vss.movieguide.data.repositories.mappers.MoviesDataModelMapper;
import com.vss.movieguide.data.repositories.mappers.ArtistsDataModelMapper;
import com.vss.movieguide.data.repositories.mappers.TvShowsDataModelMapper;
import com.vss.movieguide.data.repositories.models.MoviesDataModel;
import com.vss.movieguide.data.repositories.models.ArtistsDataModel;
import com.vss.movieguide.data.repositories.models.TvShowsDataModel;

import io.reactivex.Maybe;
import io.reactivex.Single;


public class SearchRepository implements SearchDataSourceContract.Repository {

    // region Member Variables
    private SearchDataSourceContract.LocalDateSource searchLocalDataSource;
    private SearchDataSourceContract.RemoteDateSource searchRemoteDataSource;
    private MoviesDataModelMapper moviesDataModelMapper = new MoviesDataModelMapper();
    private ArtistsDataModelMapper artistsDataModelMapper = new ArtistsDataModelMapper();
    private TvShowsDataModelMapper tvShowsDataModelMapper = new TvShowsDataModelMapper();
    // endregion

    // region Constructors
    public SearchRepository(SearchDataSourceContract.LocalDateSource searchLocalDataSource, SearchDataSourceContract.RemoteDateSource searchRemoteDataSource) {
        this.searchLocalDataSource = searchLocalDataSource;
        this.searchRemoteDataSource = searchRemoteDataSource;
    }
    // endregion

    // region SearchDataSourceContract.Repository Methods
    @Override
    public Single<MoviesDataModel> getMovieSearchResults(String query, int page) {
        Maybe<MoviesDataModel> local = searchLocalDataSource.getMovieSearchResults(query, page);
        Single<MoviesDataModel> remote =
                searchRemoteDataSource.getMovieSearchResults(query, page)
                        .map(moviesResponse -> moviesDataModelMapper.mapToDataModel(moviesResponse))
                        .doOnSuccess(moviesDataModel -> searchLocalDataSource.saveMovieSearchResults(moviesDataModel));

        return local.switchIfEmpty(remote);
    }

    @Override
    public Single<TvShowsDataModel> getTelevisionShowSearchResults(String query, int page) {
        Maybe<TvShowsDataModel> local = searchLocalDataSource.getTelevisionShowSearchResults(query, page);
        Single<TvShowsDataModel> remote =
                searchRemoteDataSource.getTelevisionShowSearchResults(query, page)
                        .map(televisionShowsResponse -> tvShowsDataModelMapper.mapToDataModel(televisionShowsResponse))
                        .doOnSuccess(televisionShowsDataModel -> searchLocalDataSource.saveTelevisionShowSearchResults(televisionShowsDataModel));

        return local.switchIfEmpty(remote);
    }

    @Override
    public Single<ArtistsDataModel> getPersonSearchResults(String query, int page) {
        Maybe<ArtistsDataModel> local = searchLocalDataSource.getPersonSearchResults(query, page);
        Single<ArtistsDataModel> remote =
                searchRemoteDataSource.getPersonSearchResults(query, page)
                        .map(personsResponse -> artistsDataModelMapper.mapToDataModel(personsResponse))
                        .doOnSuccess(personsResponse -> searchLocalDataSource.savePersonSearchResults(personsResponse));

        return local.switchIfEmpty(remote);
    }

    // endregion
}
