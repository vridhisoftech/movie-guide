package com.vss.movieguide.data.repositories.mappers;

import com.vss.movieguide.data.network.response.TelevisionShowContentRatingsResponse;
import com.vss.movieguide.data.repositories.models.TvShowContentRatingsDataModel;

public class TvShowContentRatingsDataModelMapper implements DataModelMapper<TelevisionShowContentRatingsResponse, TvShowContentRatingsDataModel> {

    // region Member Variables
    private ContentRatingDataModelMapper contentRatingDataModelMapper = new ContentRatingDataModelMapper();
    // endregion

    @Override
    public TvShowContentRatingsDataModel mapToDataModel(TelevisionShowContentRatingsResponse televisionShowContentRatingsResponse) {
        TvShowContentRatingsDataModel tvShowContentRatingsDataModel = new TvShowContentRatingsDataModel();
        tvShowContentRatingsDataModel.setContentRatings(contentRatingDataModelMapper.mapListToDataModelList(televisionShowContentRatingsResponse.getContentRatings()));
        tvShowContentRatingsDataModel.setId(televisionShowContentRatingsResponse.getId());
        return tvShowContentRatingsDataModel;
    }
}

