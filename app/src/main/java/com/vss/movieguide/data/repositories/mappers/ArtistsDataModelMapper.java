package com.vss.movieguide.data.repositories.mappers;

import com.vss.movieguide.data.network.response.ArtistResponse;
import com.vss.movieguide.data.network.response.ArtistsResponse;
import com.vss.movieguide.data.repositories.models.ArtistsDataModel;

import java.util.Calendar;
import java.util.List;

public class ArtistsDataModelMapper implements DataModelMapper<ArtistsResponse, ArtistsDataModel> {

    // region Constants
    private static final int PAGE_SIZE = 20;
    private static final int SEVEN_DAYS = 7;
    // endregion

    // region Member Variables
    private ArtistDataModelMapper artistDataModelMapper = new ArtistDataModelMapper();
    // endregion

    @Override
    public ArtistsDataModel mapToDataModel(ArtistsResponse artistsResponse) {
        ArtistsDataModel artistsDataModel = new ArtistsDataModel();
        List<ArtistResponse> artistRespons = artistsResponse.getPersons();
        artistsDataModel.setLastPage(artistRespons.size() < PAGE_SIZE);
        artistsDataModel.setPageNumber(artistsResponse.getPage());
        artistsDataModel.setPersons(artistDataModelMapper.mapListToDataModelList(artistsResponse.getPersons()));
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, SEVEN_DAYS);
        artistsDataModel.setExpiredAt(calendar.getTime());
        return artistsDataModel;
    }
}
