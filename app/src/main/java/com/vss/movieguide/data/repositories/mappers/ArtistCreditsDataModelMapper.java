package com.vss.movieguide.data.repositories.mappers;

import com.vss.movieguide.data.network.response.ArtistCreditsResponse;
import com.vss.movieguide.data.repositories.models.ArtistCreditsDataModel;

public class ArtistCreditsDataModelMapper implements DataModelMapper<ArtistCreditsResponse, ArtistCreditsDataModel> {

    // region Member Variables
    private ArtistCreditDataModelMapper artistCreditDataModelMapper = new ArtistCreditDataModelMapper();
    // endregion

    @Override
    public ArtistCreditsDataModel mapToDataModel(ArtistCreditsResponse artistCreditsResponse) {
        ArtistCreditsDataModel artistCreditsDataModel = new ArtistCreditsDataModel();
        artistCreditsDataModel.setCast(artistCreditDataModelMapper.mapListToDataModelList(artistCreditsResponse.getCast()));
        artistCreditsDataModel.setCrew(artistCreditDataModelMapper.mapListToDataModelList(artistCreditsResponse.getCrew()));
        artistCreditsDataModel.setId(artistCreditsResponse.getId());
        return artistCreditsDataModel;
    }
}
