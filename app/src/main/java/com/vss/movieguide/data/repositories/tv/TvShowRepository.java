package com.vss.movieguide.data.repositories.tv;

import com.vss.movieguide.data.repositories.mappers.TvShowContentRatingsDataModelMapper;
import com.vss.movieguide.data.repositories.mappers.TvShowCreditsDataModelMapper;
import com.vss.movieguide.data.repositories.mappers.TvShowDataModelMapper;
import com.vss.movieguide.data.repositories.mappers.TvShowsDataModelMapper;
import com.vss.movieguide.data.repositories.models.TvShowContentRatingsDataModel;
import com.vss.movieguide.data.repositories.models.TvShowCreditsDataModel;
import com.vss.movieguide.data.repositories.models.TvShowDataModel;
import com.vss.movieguide.data.repositories.models.TvShowsDataModel;

import io.reactivex.Maybe;
import io.reactivex.Single;

public class TvShowRepository implements TvShowDataSourceContract.Repository {

    // region Member Variables
    private TvShowDataSourceContract.LocalDateSource televisionShowLocalDataSource;
    private TvShowDataSourceContract.RemoteDateSource televisionShowRemoteDataSource;
    private TvShowsDataModelMapper tvShowsDataModelMapper = new TvShowsDataModelMapper();
    private TvShowDataModelMapper tvShowDataModelMapper = new TvShowDataModelMapper();
    private TvShowCreditsDataModelMapper tvShowCreditsDataModelMapper = new TvShowCreditsDataModelMapper();
    private TvShowContentRatingsDataModelMapper televisionShowContentRatingsDataModelMapper = new TvShowContentRatingsDataModelMapper();
    // endregion

    // region Constructors
    public TvShowRepository(TvShowDataSourceContract.LocalDateSource televisionShowLocalDataSource, TvShowDataSourceContract.RemoteDateSource televisionShowRemoteDataSource) {
        this.televisionShowLocalDataSource = televisionShowLocalDataSource;
        this.televisionShowRemoteDataSource = televisionShowRemoteDataSource;
    }
    // endregion

    // region TelevisionShowDataSourceContract.Repository Methods
    @Override
    public Single<TvShowsDataModel> getPopularTelevisionShows(final int currentPage) {
        Maybe<TvShowsDataModel> local = televisionShowLocalDataSource.getPopularTelevisionShows(currentPage)
                .filter(televisionShowsDataModel -> !televisionShowsDataModel.isExpired());
        Single<TvShowsDataModel> remote =
                televisionShowRemoteDataSource.getPopularTelevisionShows(currentPage)
                        .map(televisionShowsEnvelope -> tvShowsDataModelMapper.mapToDataModel(televisionShowsEnvelope))
                        .doOnSuccess(televisionShowsDataModel -> televisionShowLocalDataSource.savePopularTelevisionShows(televisionShowsDataModel));

        return local.switchIfEmpty(remote);
    }

    @Override
    public Single<TvShowDataModel> getTelevisionShow(long tvId) {
        Maybe<TvShowDataModel> local = televisionShowLocalDataSource.getTelevisionShow(tvId);
        Single<TvShowDataModel> remote =
                televisionShowRemoteDataSource.getTelevisionShow(tvId)
                        .map(televisionShowResponse -> tvShowDataModelMapper.mapToDataModel(televisionShowResponse))
                        .doOnSuccess(televisionShowDataModel -> televisionShowLocalDataSource.saveTelevisionShow(televisionShowDataModel));

        return local.switchIfEmpty(remote);
    }

    @Override
    public Single<TvShowCreditsDataModel> getTelevisionShowCredits(long tvId) {
        Maybe<TvShowCreditsDataModel> local = televisionShowLocalDataSource.getTelevisionShowCredits(tvId);
        Single<TvShowCreditsDataModel> remote =
                televisionShowRemoteDataSource.getTelevisionShowCredits(tvId)
                        .map(televisionShowCreditsResponse -> tvShowCreditsDataModelMapper.mapToDataModel(televisionShowCreditsResponse))
                        .doOnSuccess(televisionShowCreditsDataModel -> televisionShowLocalDataSource.saveTelevisionShowCredits(televisionShowCreditsDataModel));

        return local.switchIfEmpty(remote);
    }

    @Override
    public Single<TvShowsDataModel> getSimilarTelevisionShows(long tvId) {
        Maybe<TvShowsDataModel> local = televisionShowLocalDataSource.getSimilarTelevisionShows(tvId);
        Single<TvShowsDataModel> remote =
                televisionShowRemoteDataSource.getSimilarTelevisionShows(tvId)
                        .map(televisionShowsEnvelope -> tvShowsDataModelMapper.mapToDataModel(televisionShowsEnvelope))
                        .doOnSuccess(televisionShowsDataModel -> televisionShowLocalDataSource.saveSimilarTelevisionShows(televisionShowsDataModel));

        return local.switchIfEmpty(remote);
    }

    @Override
    public Single<TvShowContentRatingsDataModel> getTelevisionShowContentRatings(long tvId) {
        Maybe<TvShowContentRatingsDataModel> local = televisionShowLocalDataSource.getTelevisionShowContentRatings(tvId);
        Single<TvShowContentRatingsDataModel> remote =
                televisionShowRemoteDataSource.getTelevisionShowContentRatings(tvId)
                        .map(televisionShowContentRatingsResponse -> televisionShowContentRatingsDataModelMapper.mapToDataModel(televisionShowContentRatingsResponse))
                        .doOnSuccess(televisionShowContentRatingsDataModel -> televisionShowLocalDataSource.saveTelevisionShowContentRatings(televisionShowContentRatingsDataModel));

        return local.switchIfEmpty(remote);
    }

    // endregion
}
