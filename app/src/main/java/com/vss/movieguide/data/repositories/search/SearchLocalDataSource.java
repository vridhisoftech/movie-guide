package com.vss.movieguide.data.repositories.search;

import com.vss.movieguide.data.repositories.models.MoviesDataModel;
import com.vss.movieguide.data.repositories.models.ArtistsDataModel;
import com.vss.movieguide.data.repositories.models.TvShowsDataModel;

import io.reactivex.Maybe;

public class SearchLocalDataSource implements SearchDataSourceContract.LocalDateSource {

    // region Constructors
    public SearchLocalDataSource() {
    }
    // endregion

    // region SearchDataSourceContract.LocalDateSource Methods

    @Override
    public Maybe<MoviesDataModel> getMovieSearchResults(String query, int page) {
        //        Use mapper to convert from realm objects to POJOs
        return Maybe.empty();
    }

    @Override
    public void saveMovieSearchResults(MoviesDataModel moviesDataModel) {
//        Use mapper to convert from POJOs to realm objects
    }

    @Override
    public Maybe<TvShowsDataModel> getTelevisionShowSearchResults(String query, int page) {
        //        Use mapper to convert from realm objects to POJOs
        return Maybe.empty();
    }

    @Override
    public void saveTelevisionShowSearchResults(TvShowsDataModel tvShowsDataModel) {
//        Use mapper to convert from POJOs to realm objects
    }

    @Override
    public Maybe<ArtistsDataModel> getPersonSearchResults(String query, int page) {
        //        Use mapper to convert from realm objects to POJOs
        return Maybe.empty();
    }

    @Override
    public void savePersonSearchResults(ArtistsDataModel artistsDataModel) {
//        Use mapper to convert from POJOs to realm objects
    }

    // endregion
}
