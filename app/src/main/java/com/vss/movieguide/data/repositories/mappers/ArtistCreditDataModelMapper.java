package com.vss.movieguide.data.repositories.mappers;

import com.vss.movieguide.data.network.response.ArtistCreditResponse;
import com.vss.movieguide.data.repositories.models.ArtistCreditDataModel;

import java.util.ArrayList;
import java.util.List;

public class ArtistCreditDataModelMapper implements DataModelMapper<ArtistCreditResponse, ArtistCreditDataModel>, DataModelListMapper<ArtistCreditResponse, ArtistCreditDataModel> {

    @Override
    public ArtistCreditDataModel mapToDataModel(ArtistCreditResponse artistCreditResponse) {
        ArtistCreditDataModel artistCreditDataModel = new ArtistCreditDataModel();
        artistCreditDataModel.setCharacter(artistCreditResponse.getCharacter());
        artistCreditDataModel.setDepartment(artistCreditResponse.getDepartment());
        artistCreditDataModel.setFirstAirDate(artistCreditResponse.getFirstAirDate());
        artistCreditDataModel.setJob(artistCreditResponse.getJob());
        artistCreditDataModel.setMediaType(artistCreditResponse.getMediaType());
        artistCreditDataModel.setName(artistCreditResponse.getName());
        artistCreditDataModel.setPosterPath(artistCreditResponse.getPosterPath());
        artistCreditDataModel.setReleaseDate(artistCreditResponse.getReleaseDate());
        artistCreditDataModel.setTitle(artistCreditResponse.getTitle());
        artistCreditDataModel.setCreditId(artistCreditResponse.getCreditId());
        artistCreditDataModel.setId(artistCreditResponse.getId());
        return artistCreditDataModel;
    }

    @Override
    public List<ArtistCreditDataModel> mapListToDataModelList(List<ArtistCreditResponse> artistCreditRespons) {
        List<ArtistCreditDataModel> artistCreditDataModels = new ArrayList<>();
        if(artistCreditRespons != null && artistCreditRespons.size()>0) {
            for (ArtistCreditResponse artistCreditResponse : artistCreditRespons) {
                artistCreditDataModels.add(mapToDataModel(artistCreditResponse));
            }
        }
        return artistCreditDataModels;
    }
}
