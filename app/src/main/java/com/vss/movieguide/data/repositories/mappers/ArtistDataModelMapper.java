package com.vss.movieguide.data.repositories.mappers;

import com.vss.movieguide.data.network.response.ArtistResponse;
import com.vss.movieguide.data.repositories.models.ArtistDataModel;

import java.util.ArrayList;
import java.util.List;

public class ArtistDataModelMapper implements DataModelMapper<ArtistResponse, ArtistDataModel>, DataModelListMapper<ArtistResponse, ArtistDataModel> {

    // region Member Variables
    private ProfileImagesDataModelMapper profileImagesDataModelMapper = new ProfileImagesDataModelMapper();
    // endregion

    @Override
    public ArtistDataModel mapToDataModel(ArtistResponse artistResponse) {
        ArtistDataModel artistDataModel = new ArtistDataModel();
        artistDataModel.setBiography(artistResponse.getBiography());
        artistDataModel.setBirthday(artistResponse.getBirthday());
        artistDataModel.setDeathday(artistResponse.getDeathday());
        artistDataModel.setId(artistResponse.getId());
        artistDataModel.setImages(profileImagesDataModelMapper.mapToDataModel(artistResponse.getImages()));
        artistDataModel.setImdbId(artistResponse.getImdbId());
        artistDataModel.setName(artistResponse.getName());
        artistDataModel.setPlaceOfBirth(artistResponse.getPlaceOfBirth());
        artistDataModel.setProfilePath(artistResponse.getProfilePath());

        return artistDataModel;
    }

    @Override
    public List<ArtistDataModel> mapListToDataModelList(List<ArtistResponse> artistRespons) {
        List<ArtistDataModel> artistDataModels = new ArrayList<>();
        if(artistRespons != null && artistRespons.size()>0) {
            for (ArtistResponse artistResponse : artistRespons) {
                artistDataModels.add(mapToDataModel(artistResponse));
            }
        }
        return artistDataModels;
    }
}
