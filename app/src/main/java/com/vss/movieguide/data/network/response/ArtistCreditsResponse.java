package com.vss.movieguide.data.network.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ArtistCreditsResponse {

    // region Fields
    @SerializedName("id")
    public int id;
    @SerializedName("cast")
    public List<ArtistCreditResponse> cast = null;
    @SerializedName("crew")
    public List<ArtistCreditResponse> crew = null;
    // endregion

    // region Getters

    public int getId() {
        return id;
    }

    public List<ArtistCreditResponse> getCast() {
        return cast;
    }

    public List<ArtistCreditResponse> getCrew() {
        return crew;
    }

    // endregion

    // region Setters

    public void setId(int id) {
        this.id = id;
    }

    public void setCast(List<ArtistCreditResponse> cast) {
        this.cast = cast;
    }

    public void setCrew(List<ArtistCreditResponse> crew) {
        this.crew = crew;
    }

    // endregion

    @Override
    public String toString() {
        return "PersonCreditsResponse{" +
                "id=" + id +
                ", cast=" + cast +
                ", crew=" + crew +
                '}';
    }
}
