package com.vss.movieguide.data.repositories.artist;

import com.vss.movieguide.data.network.MovieGuideService;
import com.vss.movieguide.data.network.response.ArtistCreditsResponse;
import com.vss.movieguide.data.network.response.ArtistResponse;
import com.vss.movieguide.data.network.response.ArtistsResponse;

import javax.inject.Inject;

import io.reactivex.Single;


public class ArtistRemoteDataSource implements ArtistDataSourceContract.RemoteDateSource {

    // region Member Variables
    private MovieGuideService movieGuideService;
    // endregion

    // region Constructors
    @Inject
    public ArtistRemoteDataSource(MovieGuideService movieGuideService) {
        this.movieGuideService = movieGuideService;
    }
    // endregion

    // region PersonDataSourceContract.RemoteDateSource Methods

    @Override
    public Single<ArtistsResponse> getPopularPersons(int currentPage) {
        return movieGuideService.getPopularPersons(currentPage);
    }

    @Override
    public Single<ArtistResponse> getPerson(long personId) {
        return movieGuideService.getPerson(personId);
    }

    @Override
    public Single<ArtistCreditsResponse> getPersonCredits(long personId) {
        return movieGuideService.getPersonCredits(personId);
    }

    // endregion
}
