package com.vss.movieguide.data.repositories.search;

import com.vss.movieguide.data.network.response.MoviesResponse;
import com.vss.movieguide.data.network.response.ArtistsResponse;
import com.vss.movieguide.data.network.response.TelevisionShowsResponse;
import com.vss.movieguide.data.repositories.models.MoviesDataModel;
import com.vss.movieguide.data.repositories.models.ArtistsDataModel;
import com.vss.movieguide.data.repositories.models.TvShowsDataModel;

import io.reactivex.Maybe;
import io.reactivex.Single;

public interface SearchDataSourceContract {

    interface Repository {
        Single<MoviesDataModel> getMovieSearchResults(String query, int page);
        Single<TvShowsDataModel> getTelevisionShowSearchResults(String query, int page);
        Single<ArtistsDataModel> getPersonSearchResults(String query, int page);
    }

    interface LocalDateSource {
        Maybe<MoviesDataModel> getMovieSearchResults(String query, int page);
        void saveMovieSearchResults(MoviesDataModel moviesDataModel);
        Maybe<TvShowsDataModel> getTelevisionShowSearchResults(String query, int page);
        void saveTelevisionShowSearchResults(TvShowsDataModel tvShowsDataModel);
        Maybe<ArtistsDataModel> getPersonSearchResults(String query, int page);
        void savePersonSearchResults(ArtistsDataModel artistsDataModel);
    }

    interface RemoteDateSource {
        Single<MoviesResponse> getMovieSearchResults(String query, int page);
        Single<TelevisionShowsResponse> getTelevisionShowSearchResults(String query, int page);
        Single<ArtistsResponse> getPersonSearchResults(String query, int page);
    }
}
