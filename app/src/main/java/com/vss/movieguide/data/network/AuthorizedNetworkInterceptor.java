package com.vss.movieguide.data.network;

import android.content.Context;

import com.vss.movieguide.BuildConfig;
import com.vss.movieguide.R;
import com.vss.movieguide.util.RequestUtility;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class AuthorizedNetworkInterceptor implements Interceptor {

    // region Constants
    private static final String KEY_API_KEY = "api_key";
    private static final String KEY_LANGUAGE = "language";
    // endregion

    // region Member Variables
    private Context context;
    // endregion

    // region Constructors
    public AuthorizedNetworkInterceptor(Context context) {
        this.context = context;
    }
    // endregion

    @Override
    public Response intercept(Chain chain) throws IOException {
        if (chain != null) {
            Request originalRequest = chain.request();

            Map<String, String> queryParamsMap = new HashMap<>();
            queryParamsMap.put(KEY_API_KEY, BuildConfig.Api_Key);
            queryParamsMap.put(KEY_LANGUAGE, "en-US");
            Request modifiedRequest = RequestUtility.addQueryParams(originalRequest, queryParamsMap);

            return chain.proceed(modifiedRequest);
        }

        return null;
    }
}
