package com.vss.movieguide.data.repositories.mappers;

import com.vss.movieguide.data.network.response.NetworkResponse;
import com.vss.movieguide.data.repositories.models.NetworkDataModel;

import java.util.ArrayList;
import java.util.List;

public class NetworkDataModelMapper implements DataModelMapper<NetworkResponse, NetworkDataModel>, DataModelListMapper<NetworkResponse, NetworkDataModel> {
    @Override
    public NetworkDataModel mapToDataModel(NetworkResponse networkResponse) {
        NetworkDataModel networkDataModel = new NetworkDataModel();
        networkDataModel.setId(networkResponse.getId());
        networkDataModel.setName(networkResponse.getName());
        return networkDataModel;
    }

    @Override
    public List<NetworkDataModel> mapListToDataModelList(List<NetworkResponse> networkResponses) {
        List<NetworkDataModel> networkDataModels = new ArrayList<>();
        if(networkResponses != null && networkResponses.size()>0) {
            for (NetworkResponse networkResponse : networkResponses) {
                networkDataModels.add(mapToDataModel(networkResponse));
            }
        }
        return networkDataModels;
    }
}
