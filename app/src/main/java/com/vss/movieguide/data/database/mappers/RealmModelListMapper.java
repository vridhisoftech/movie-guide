package com.vss.movieguide.data.database.mappers;

import java.util.List;

import io.realm.RealmList;

public interface RealmModelListMapper<Pojo, RealmModel> {
    RealmList<RealmModel> mapListToRealmModelList(List<Pojo> pojos);
    List<Pojo> mapListFromRealmModelList(RealmList<RealmModel> realmModels);
}
