package com.vss.movieguide.data.repositories.mappers;

import java.util.List;

public interface DataModelListMapper<NetworkResponseModel, DataModel> {
    List<DataModel> mapListToDataModelList(List<NetworkResponseModel> networkResponseModels);
}