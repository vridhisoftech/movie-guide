package com.vss.movieguide.data.repositories.mappers;

import com.vss.movieguide.data.network.response.TelevisionShowCreditResponse;
import com.vss.movieguide.data.repositories.models.TvShowCreditDataModel;

import java.util.ArrayList;
import java.util.List;

public class TvShowCreditDataModelMapper implements DataModelMapper<TelevisionShowCreditResponse, TvShowCreditDataModel>, DataModelListMapper<TelevisionShowCreditResponse, TvShowCreditDataModel> {

    @Override
    public TvShowCreditDataModel mapToDataModel(TelevisionShowCreditResponse televisionShowCreditResponse) {
        TvShowCreditDataModel tvShowCreditDataModel = new TvShowCreditDataModel();
        tvShowCreditDataModel.setCharacter(televisionShowCreditResponse.getCharacter());
        tvShowCreditDataModel.setDepartment(televisionShowCreditResponse.getDepartment());
        tvShowCreditDataModel.setJob(televisionShowCreditResponse.getJob());
        tvShowCreditDataModel.setName(televisionShowCreditResponse.getName());
        tvShowCreditDataModel.setProfilePath(televisionShowCreditResponse.getProfilePath());
        tvShowCreditDataModel.setCreditId(televisionShowCreditResponse.getCreditId());
        tvShowCreditDataModel.setId(televisionShowCreditResponse.getId());
        return tvShowCreditDataModel;
    }

    @Override
    public List<TvShowCreditDataModel> mapListToDataModelList(List<TelevisionShowCreditResponse> televisionShowCreditResponses) {
        List<TvShowCreditDataModel> tvShowCreditDataModels = new ArrayList<>();
        if(televisionShowCreditResponses != null && televisionShowCreditResponses.size()>0) {
            for (TelevisionShowCreditResponse televisionShowCreditResponse : televisionShowCreditResponses) {
                tvShowCreditDataModels.add(mapToDataModel(televisionShowCreditResponse));
            }
        }
        return tvShowCreditDataModels;
    }
}
