package com.vss.movieguide.data.database.models;

import io.realm.RealmObject;

public class StringRealmModel extends RealmObject {

    // region Fields
    public String value;
    // endregion

    // region Getters
    public String getValue() {
        return value;
    }
    // endregion

    // region Setters
    public void setValue(String value) {
        this.value = value;
    }
    // endregion
}
