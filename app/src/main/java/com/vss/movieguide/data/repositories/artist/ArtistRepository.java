package com.vss.movieguide.data.repositories.artist;

import com.vss.movieguide.data.repositories.mappers.ArtistCreditsDataModelMapper;
import com.vss.movieguide.data.repositories.mappers.ArtistDataModelMapper;
import com.vss.movieguide.data.repositories.mappers.ArtistsDataModelMapper;
import com.vss.movieguide.data.repositories.models.ArtistCreditsDataModel;
import com.vss.movieguide.data.repositories.models.ArtistDataModel;
import com.vss.movieguide.data.repositories.models.ArtistsDataModel;

import io.reactivex.Maybe;
import io.reactivex.Single;


public class ArtistRepository implements ArtistDataSourceContract.Repository {

    // region Member Variables
    private ArtistDataSourceContract.LocalDateSource personLocalDataSource;
    private ArtistDataSourceContract.RemoteDateSource personRemoteDataSource;
    private ArtistsDataModelMapper artistsDataModelMapper = new ArtistsDataModelMapper();
    private ArtistDataModelMapper artistDataModelMapper = new ArtistDataModelMapper();
    private ArtistCreditsDataModelMapper artistCreditsDataModelMapper = new ArtistCreditsDataModelMapper();
    // endregion

    // region Constructors
    public ArtistRepository(ArtistDataSourceContract.LocalDateSource personLocalDataSource, ArtistDataSourceContract.RemoteDateSource personRemoteDataSource) {
        this.personLocalDataSource = personLocalDataSource;
        this.personRemoteDataSource = personRemoteDataSource;
    }
    // endregion

    // region PersonDataSourceContract.Repository Methods
    @Override
    public Single<ArtistsDataModel> getPopularPersons(final int currentPage) {
        Maybe<ArtistsDataModel> local = personLocalDataSource.getPopularPersons(currentPage)
                .filter(personsDataModel -> !personsDataModel.isExpired());
        Single<ArtistsDataModel> remote =
                personRemoteDataSource.getPopularPersons(currentPage)
                        .map(personsResponse -> artistsDataModelMapper.mapToDataModel(personsResponse))
                        .doOnSuccess(personsDataModel -> personLocalDataSource.savePopularPersons(personsDataModel));

        return local.switchIfEmpty(remote);
    }

    @Override
    public Single<ArtistDataModel> getPerson(long personId) {
        Maybe<ArtistDataModel> local = personLocalDataSource.getPerson(personId);
        Single<ArtistDataModel> remote =
                personRemoteDataSource.getPerson(personId)
                        .map(personResponse -> artistDataModelMapper.mapToDataModel(personResponse))
                        .doOnSuccess(personDataModel -> personLocalDataSource.savePerson(personDataModel));

        return local.switchIfEmpty(remote);
    }

    @Override
    public Single<ArtistCreditsDataModel> getPersonCredits(long personId) {
        Maybe<ArtistCreditsDataModel> local = personLocalDataSource.getPersonCredits(personId);
        Single<ArtistCreditsDataModel> remote =
                personRemoteDataSource.getPersonCredits(personId)
                        .map(personCreditsResponse -> artistCreditsDataModelMapper.mapToDataModel(personCreditsResponse))
                        .doOnSuccess(personCreditsDataModel -> personLocalDataSource.savePersonCredits(personCreditsDataModel));

        return local.switchIfEmpty(remote);
    }

    // endregion
}
