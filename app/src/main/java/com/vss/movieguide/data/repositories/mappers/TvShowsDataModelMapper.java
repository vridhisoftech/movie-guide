package com.vss.movieguide.data.repositories.mappers;

import com.vss.movieguide.data.network.response.TelevisionShowsResponse;
import com.vss.movieguide.data.repositories.models.TvShowsDataModel;

import java.util.Calendar;

public class TvShowsDataModelMapper implements DataModelMapper<TelevisionShowsResponse, TvShowsDataModel> {

    // region Constants
    private static final int PAGE_SIZE = 20;
    private static final int SEVEN_DAYS = 7;
    // endregion

    // region Member Variables
    private TvShowDataModelMapper tvShowDataModelMapper = new TvShowDataModelMapper();
    // endregion

    @Override
    public TvShowsDataModel mapToDataModel(TelevisionShowsResponse televisionShowsResponse) {
        TvShowsDataModel tvShowsDataModel = new TvShowsDataModel();
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, SEVEN_DAYS);
        tvShowsDataModel.setExpiredAt(calendar.getTime());
        tvShowsDataModel.setLastPage(televisionShowsResponse.getTelevisionShows().size() < PAGE_SIZE);
        tvShowsDataModel.setTelevisionShows(tvShowDataModelMapper.mapListToDataModelList(televisionShowsResponse.getTelevisionShows()));
        tvShowsDataModel.setPageNumber(televisionShowsResponse.getPage());
        return tvShowsDataModel;
    }
}
