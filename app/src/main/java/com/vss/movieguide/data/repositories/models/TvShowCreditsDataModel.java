package com.vss.movieguide.data.repositories.models;

import java.util.List;

public class TvShowCreditsDataModel {

    // region Fields
    public int id;
    public List<TvShowCreditDataModel> cast = null;
    public List<TvShowCreditDataModel> crew = null;
    // endregion

    // region Getters

    public int getId() {
        return id;
    }

    public List<TvShowCreditDataModel> getCast() {
        return cast;
    }

    public List<TvShowCreditDataModel> getCrew() {
        return crew;
    }

    // endregion

    // region Setters

    public void setId(int id) {
        this.id = id;
    }

    public void setCast(List<TvShowCreditDataModel> cast) {
        this.cast = cast;
    }

    public void setCrew(List<TvShowCreditDataModel> crew) {
        this.crew = crew;
    }

    // endregion

    @Override
    public String toString() {
        return "TelevisionShowCreditsDataModel{" +
                "id=" + id +
                ", cast=" + cast +
                ", crew=" + crew +
                '}';
    }
}
