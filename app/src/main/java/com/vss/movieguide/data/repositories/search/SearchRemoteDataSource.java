package com.vss.movieguide.data.repositories.search;

import com.vss.movieguide.data.network.MovieGuideService;
import com.vss.movieguide.data.network.response.MoviesResponse;
import com.vss.movieguide.data.network.response.ArtistsResponse;
import com.vss.movieguide.data.network.response.TelevisionShowsResponse;

import javax.inject.Inject;

import io.reactivex.Single;

public class SearchRemoteDataSource implements SearchDataSourceContract.RemoteDateSource {

    // region Member Variables
    private MovieGuideService movieGuideService;
    // endregion

    // region Constructors
    @Inject
    public SearchRemoteDataSource(MovieGuideService movieGuideService) {
        this.movieGuideService = movieGuideService;
    }
    // endregion

    // region SearchDataSourceContract.RemoteDateSource Methods
    @Override
    public Single<MoviesResponse> getMovieSearchResults(String query, int page) {
        return movieGuideService.getMovieSearchResults(query, page);
    }

    @Override
    public Single<TelevisionShowsResponse> getTelevisionShowSearchResults(String query, int page) {
        return movieGuideService.getTelevisionShowSearchResults(query, page);
    }

    @Override
    public Single<ArtistsResponse> getPersonSearchResults(String query, int page) {
        return movieGuideService.getPersonSearchResults(query, page);
    }

    // endregion
}
