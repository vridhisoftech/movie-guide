package com.vss.movieguide.data.repositories.mappers;

import com.vss.movieguide.data.network.response.ProfileImagesResponse;
import com.vss.movieguide.data.repositories.models.ProfileImagesDataModel;

public class ProfileImagesDataModelMapper implements DataModelMapper<ProfileImagesResponse, ProfileImagesDataModel> {

    // region Member Variables
    private ProfileImageDataModelMapper profileImageDataModelMapper = new ProfileImageDataModelMapper();
    // endregion

    @Override
    public ProfileImagesDataModel mapToDataModel(ProfileImagesResponse profileImagesResponse) {
        ProfileImagesDataModel profileImagesDataModel = new ProfileImagesDataModel();
        if(profileImagesResponse != null){
            profileImagesDataModel.setProfiles(profileImageDataModelMapper.mapListToDataModelList(profileImagesResponse.getProfiles()));
        }
        return profileImagesDataModel;
    }
}
