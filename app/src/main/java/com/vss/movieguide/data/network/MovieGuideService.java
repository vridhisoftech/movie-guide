package com.vss.movieguide.data.network;

import com.vss.movieguide.data.network.response.ConfigurationResponse;
import com.vss.movieguide.data.network.response.MovieCreditsResponse;
import com.vss.movieguide.data.network.response.MovieReleaseDatesResponse;
import com.vss.movieguide.data.network.response.MovieResponse;
import com.vss.movieguide.data.network.response.MoviesResponse;
import com.vss.movieguide.data.network.response.ArtistCreditsResponse;
import com.vss.movieguide.data.network.response.ArtistResponse;
import com.vss.movieguide.data.network.response.ArtistsResponse;
import com.vss.movieguide.data.network.response.TelevisionShowContentRatingsResponse;
import com.vss.movieguide.data.network.response.TelevisionShowCreditsResponse;
import com.vss.movieguide.data.network.response.TelevisionShowResponse;
import com.vss.movieguide.data.network.response.TelevisionShowsResponse;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MovieGuideService {

    @GET("movie/popular")
    Single<MoviesResponse> getPopularMovies(@Query("page") int page);

    @GET("movie/{movieId}")
    Single<MovieResponse> getMovie(@Path("movieId") long movieId);

    @GET("movie/{movieId}/credits")
    Single<MovieCreditsResponse> getMovieCredits(@Path("movieId") long movieId);

    @GET("movie/{movieId}/similar")
    Single<MoviesResponse> getSimilarMovies(@Path("movieId") long movieId);

    @GET("movie/{movieId}/release_dates")
    Single<MovieReleaseDatesResponse> getMovieReleaseDates(@Path("movieId") long movieId);

    @GET("tv/popular")
    Single<TelevisionShowsResponse> getPopularTelevisionShows(@Query("page") int page);

    @GET("tv/{tvId}")
    Single<TelevisionShowResponse> getTelevisionShow(@Path("tvId") long tvId);

    @GET("tv/{tvId}/credits")
    Single<TelevisionShowCreditsResponse> getTelevisionShowCredits(@Path("tvId") long tvId);

    @GET("tv/{tvId}/similar")
    Single<TelevisionShowsResponse> getSimilarTelevisionShows(@Path("tvId") long tvId);

    @GET("tv/{tvId}/content_ratings")
    Single<TelevisionShowContentRatingsResponse> getTelevisionShowContentRatings(@Path("tvId") long tvId);

    @GET("person/popular")
    Single<ArtistsResponse> getPopularPersons(@Query("page") int page);

    @GET("person/{personId}?append_to_response=images")
    Single<ArtistResponse> getPerson(@Path("personId") long personId);

    @GET("person/{personId}/combined_credits")
    Single<ArtistCreditsResponse> getPersonCredits(@Path("personId") long personId);

    @GET("configuration")
    Single<ConfigurationResponse> getConfiguration();

    @GET("search/movie")
    Single<MoviesResponse> getMovieSearchResults(@Query("query") String query, @Query("page") int page);

    @GET("search/tv")
    Single<TelevisionShowsResponse> getTelevisionShowSearchResults(@Query("query") String query, @Query("page") int page);

    @GET("search/person")
    Single<ArtistsResponse> getPersonSearchResults(@Query("query") String query, @Query("page") int page);

}
