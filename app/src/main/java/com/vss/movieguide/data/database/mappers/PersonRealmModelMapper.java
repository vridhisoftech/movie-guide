package com.vss.movieguide.data.database.mappers;

import com.vss.movieguide.data.database.models.ArtistRealmModel;
import com.vss.movieguide.data.repositories.models.ArtistDataModel;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;

public class PersonRealmModelMapper implements RealmModelMapper<ArtistDataModel, ArtistRealmModel>, RealmModelListMapper<ArtistDataModel, ArtistRealmModel> {

    private ProfileImagesRealmModelMapper profileImagesRealmMapper = new ProfileImagesRealmModelMapper();

    @Override
    public ArtistRealmModel mapToRealmModel(ArtistDataModel artistDataModel) {
        ArtistRealmModel realmPerson = Realm.getDefaultInstance().createObject(ArtistRealmModel.class);

        realmPerson.setBiography(artistDataModel.getBiography());
        realmPerson.setBirthday(artistDataModel.getBirthday());
        realmPerson.setDeathday(artistDataModel.getDeathday());
        realmPerson.setId(artistDataModel.getId());
        realmPerson.setImdbId(artistDataModel.getImdbId());
        realmPerson.setName(artistDataModel.getName());
        realmPerson.setPlaceOfBirth(artistDataModel.getPlaceOfBirth());
        realmPerson.setProfilePath(artistDataModel.getProfilePath());
        realmPerson.setImages(profileImagesRealmMapper.mapToRealmModel(artistDataModel.getImages()));

        return realmPerson;
    }

    @Override
    public ArtistDataModel mapFromRealmModel(ArtistRealmModel artistRealmModel) {
        ArtistDataModel artistDataModel = new ArtistDataModel();

        artistDataModel.setBiography(artistRealmModel.getBiography());
        artistDataModel.setBirthday(artistRealmModel.getBirthday());
        artistDataModel.setDeathday(artistRealmModel.getDeathday());
        artistDataModel.setId(artistRealmModel.getId());
        artistDataModel.setImdbId(artistRealmModel.getImdbId());
        artistDataModel.setName(artistRealmModel.getName());
        artistDataModel.setPlaceOfBirth(artistRealmModel.getPlaceOfBirth());
        artistDataModel.setProfilePath(artistRealmModel.getProfilePath());
        artistDataModel.setImages(profileImagesRealmMapper.mapFromRealmModel(artistRealmModel.getImages()));

        return artistDataModel;
    }

    @Override
    public RealmList<ArtistRealmModel> mapListToRealmModelList(List<ArtistDataModel> artistDataModels) {
        RealmList<ArtistRealmModel> artistRealmModels = new RealmList<>();
        for(ArtistDataModel artistDataModel : artistDataModels){
            artistRealmModels.add(mapToRealmModel(artistDataModel));
        }
        return artistRealmModels;
    }

    @Override
    public List<ArtistDataModel> mapListFromRealmModelList(RealmList<ArtistRealmModel> artistRealmModels) {
        List<ArtistDataModel> artistDataModels = new ArrayList<>();
        for(ArtistRealmModel artistRealmModel : artistRealmModels){
            artistDataModels.add(mapFromRealmModel(artistRealmModel));
        }
        return artistDataModels;
    }
}
