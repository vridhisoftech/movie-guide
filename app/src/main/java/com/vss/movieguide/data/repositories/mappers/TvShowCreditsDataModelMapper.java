package com.vss.movieguide.data.repositories.mappers;

import com.vss.movieguide.data.network.response.TelevisionShowCreditsResponse;
import com.vss.movieguide.data.repositories.models.TvShowCreditsDataModel;

public class TvShowCreditsDataModelMapper implements DataModelMapper<TelevisionShowCreditsResponse, TvShowCreditsDataModel> {

    // region Member Variables
    private TvShowCreditDataModelMapper tvShowCreditDataModelMapper = new TvShowCreditDataModelMapper();
    // endregion

    @Override
    public TvShowCreditsDataModel mapToDataModel(TelevisionShowCreditsResponse televisionShowCreditsResponse) {
        TvShowCreditsDataModel tvShowCreditsDataModel = new TvShowCreditsDataModel();
        tvShowCreditsDataModel.setId(televisionShowCreditsResponse.getId());
        tvShowCreditsDataModel.setCast(tvShowCreditDataModelMapper.mapListToDataModelList(televisionShowCreditsResponse.getCast()));
        tvShowCreditsDataModel.setCrew(tvShowCreditDataModelMapper.mapListToDataModelList(televisionShowCreditsResponse.getCrew()));
        return tvShowCreditsDataModel;
    }
}
