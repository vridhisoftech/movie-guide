package com.vss.movieguide.data.repositories.movie;

import com.vss.movieguide.data.network.MovieGuideService;
import com.vss.movieguide.data.network.response.MovieCreditsResponse;
import com.vss.movieguide.data.network.response.MovieReleaseDatesResponse;
import com.vss.movieguide.data.network.response.MovieResponse;
import com.vss.movieguide.data.network.response.MoviesResponse;

import javax.inject.Inject;

import io.reactivex.Single;

public class MovieRemoteDataSource implements MovieDataSourceContract.RemoteDateSource {

    // region Member Variables
    private MovieGuideService movieGuideService;
    // endregion

    // region Constructors
    @Inject
    public MovieRemoteDataSource(MovieGuideService movieGuideService) {
        this.movieGuideService = movieGuideService;
    }
    // endregion

    // region MovieDataSourceContract.RemoteDateSource Methods
    @Override
    public Single<MoviesResponse> getPopularMovies(final int currentPage) {
        return movieGuideService.getPopularMovies(currentPage);
    }

    @Override
    public Single<MovieResponse> getMovie(long movieId) {
        return movieGuideService.getMovie(movieId);
    }

    @Override
    public Single<MovieCreditsResponse> getMovieCredits(long movieId) {
        return movieGuideService.getMovieCredits(movieId);
    }

    @Override
    public Single<MoviesResponse> getSimilarMovies(long movieId) {
        return movieGuideService.getSimilarMovies(movieId);
    }

    @Override
    public Single<MovieReleaseDatesResponse> getMovieReleaseDates(long movieId) {
        return movieGuideService.getMovieReleaseDates(movieId);
    }

    // endregion
}
