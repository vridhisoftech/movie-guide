package com.vss.movieguide.data.repositories.mappers;

import com.vss.movieguide.data.network.response.TelevisionShowResponse;
import com.vss.movieguide.data.repositories.models.TvShowDataModel;

import java.util.ArrayList;
import java.util.List;

public class TvShowDataModelMapper implements DataModelMapper<TelevisionShowResponse, TvShowDataModel>, DataModelListMapper<TelevisionShowResponse, TvShowDataModel> {

    // region Member Variables
    private GenreDataModelMapper genreDataModelMapper = new GenreDataModelMapper();
    private NetworkDataModelMapper networkDataModelMapper = new NetworkDataModelMapper();
    // endregion

    @Override
    public TvShowDataModel mapToDataModel(TelevisionShowResponse televisionShowResponse) {
        TvShowDataModel tvShowDataModel = new TvShowDataModel();
        tvShowDataModel.setBackdropPath(televisionShowResponse.getBackdropPath());
        tvShowDataModel.setEpisodeRunTime(televisionShowResponse.getEpisodeRunTime());
        tvShowDataModel.setFirstAirDate(televisionShowResponse.getFirstAirDate());
        tvShowDataModel.setGenres(genreDataModelMapper.mapListToDataModelList(televisionShowResponse.getGenres()));
        tvShowDataModel.setHomepage(televisionShowResponse.getHomepage());
        tvShowDataModel.setId(televisionShowResponse.getId());
        tvShowDataModel.setInProduction(televisionShowResponse.isInProduction());
        tvShowDataModel.setLanguages(televisionShowResponse.getLanguages());
        tvShowDataModel.setLastAirDate(televisionShowResponse.getLastAirDate());
        tvShowDataModel.setName(televisionShowResponse.getName());
        tvShowDataModel.setNetworks(networkDataModelMapper.mapListToDataModelList(televisionShowResponse.getNetworks()));
        tvShowDataModel.setNumberOfEpisodes(televisionShowResponse.getNumberOfEpisodes());
        tvShowDataModel.setNumberOfSeasons(televisionShowResponse.getNumberOfSeasons());
        tvShowDataModel.setOriginalLanguage(televisionShowResponse.getOriginalLanguage());
        tvShowDataModel.setOriginalName(televisionShowResponse.getOriginalName());
        tvShowDataModel.setOriginCountry(televisionShowResponse.getOriginCountry());
        tvShowDataModel.setOverview(televisionShowResponse.getOverview());
        tvShowDataModel.setPopularity(televisionShowResponse.getPopularity());
        tvShowDataModel.setPosterPath(televisionShowResponse.getPosterPath());
        tvShowDataModel.setStatus(televisionShowResponse.getStatus());
        tvShowDataModel.setType(televisionShowResponse.getType());
        tvShowDataModel.setVoteAverage(televisionShowResponse.getVoteAverage());
        tvShowDataModel.setVoteCount(televisionShowResponse.getVoteCount());
        return tvShowDataModel;
    }

    @Override
    public List<TvShowDataModel> mapListToDataModelList(List<TelevisionShowResponse> televisionShowResponses) {
        List<TvShowDataModel> tvShowDataModels = new ArrayList<>();
        if(televisionShowResponses != null && televisionShowResponses.size()>0) {
            for (TelevisionShowResponse televisionShowResponse : televisionShowResponses) {
                tvShowDataModels.add(mapToDataModel(televisionShowResponse));
            }
        }
        return tvShowDataModels;
    }
}