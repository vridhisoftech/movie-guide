package com.vss.movieguide.presentation.mappers;

import com.vss.movieguide.domain.models.NetworkDomainModel;
import com.vss.movieguide.presentation.models.NetworkPresentationModel;

import java.util.ArrayList;
import java.util.List;

public class NetworkPresentationModelMapper implements PresentationModelMapper<NetworkDomainModel, NetworkPresentationModel>, PresentationModelListMapper<NetworkDomainModel, NetworkPresentationModel> {

    @Override
    public NetworkPresentationModel mapToPresentationModel(NetworkDomainModel networkDomainModel) {
        NetworkPresentationModel networkPresentationModel = new NetworkPresentationModel();
        networkPresentationModel.setId(networkDomainModel.getId());
        networkPresentationModel.setName(networkDomainModel.getName());
        return networkPresentationModel;
    }

    @Override
    public List<NetworkPresentationModel> mapListToPresentationModelList(List<NetworkDomainModel> networkDomainModels) {
        List<NetworkPresentationModel> networkPresentationModels = new ArrayList<>();
        if(networkDomainModels != null && networkDomainModels.size()>0) {
            for (NetworkDomainModel networkDomainModel : networkDomainModels) {
                networkPresentationModels.add(mapToPresentationModel(networkDomainModel));
            }
        }
        return networkPresentationModels;
    }

}
