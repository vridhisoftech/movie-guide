package com.vss.movieguide.presentation.base;

public interface BasePresenter {

    void onDestroyView();
}
