package com.vss.movieguide.presentation.artists;

import com.vss.movieguide.domain.models.ArtistDomainModel;
import com.vss.movieguide.domain.models.ArtistsDomainModel;
import com.vss.movieguide.domain.usecases.ArtistsDomainContract;
import com.vss.movieguide.presentation.models.ArtistPresentationModel;
import com.vss.movieguide.util.rxjava.SchedulerProvider;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;

public class ArtistsPresenter implements ArtistsPresentationContract.Presenter {

    // region Member Variables
    private final ArtistsPresentationContract.View personsView;
    private final ArtistsDomainContract.UseCase personsUseCase;
    private final SchedulerProvider schedulerProvider;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    // endregion

    // region Constructors
    public ArtistsPresenter(ArtistsPresentationContract.View personsView, ArtistsDomainContract.UseCase personsUseCase, SchedulerProvider schedulerProvider) {
        this.personsView = personsView;
        this.personsUseCase = personsUseCase;
        this.schedulerProvider = schedulerProvider;
    }
    // endregion

    // region PersonsPresentationContract.Presenter Methods
    @Override
    public void onDestroyView() {
        if (compositeDisposable != null)
            compositeDisposable.clear();
    }

    @Override
    public void onLoadPopularPersons(final int currentPage) {
        if(currentPage == 1){
            personsView.hideEmptyView();
            personsView.hideErrorView();
            personsView.showLoadingView();
        } else{
            personsView.showLoadingFooterView();
        }

        Disposable disposable = personsUseCase.getPopularPersons(currentPage)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribeWith(new DisposableSingleObserver<ArtistsDomainModel>() {
                    @Override
                    public void onSuccess(ArtistsDomainModel personsDomainModel) {
                        if(personsDomainModel != null){
                            List<ArtistDomainModel> persons = personsDomainModel.getPersons();
                            int currentPage = personsDomainModel.getPageNumber();
                            boolean isLastPage = personsDomainModel.isLastPage();
                            boolean hasMovies = personsDomainModel.hasPersons();

                            if(currentPage == 1){
                                personsView.hideLoadingView();

                                if(hasMovies){
                                    personsView.addHeaderView();
                                    personsView.showPersons(persons);

                                    if(!isLastPage)
                                        personsView.addFooterView();
                                } else {
                                    personsView.showEmptyView();
                                }
                            } else {
                                personsView.removeFooterView();

                                if(hasMovies){
                                    personsView.showPersons(persons);

                                    if(!isLastPage)
                                        personsView.addFooterView();
                                }
                            }

                            personsView.setPersonsDomainModel(personsDomainModel);
                        }
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        throwable.printStackTrace();

                        if(currentPage == 1){
                            personsView.hideLoadingView();

                            personsView.showErrorView();
                        } else {
                            personsView.showErrorFooterView();
                        }
                    }
                });

        compositeDisposable.add(disposable);
    }

    @Override
    public void onPersonClick(ArtistPresentationModel person) {
        personsView.openPersonDetails(person);
    }

    @Override
    public void onScrollToEndOfList() {
        personsView.loadMorePersons();
    }
    // endregion
}
