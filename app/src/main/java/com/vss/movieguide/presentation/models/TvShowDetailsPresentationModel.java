package com.vss.movieguide.presentation.models;

import java.util.List;

public class TvShowDetailsPresentationModel {

    // region Fields
    private TvShowPresentationModel televisionShow;
    private List<TvShowCreditPresentationModel> cast;
    private List<TvShowCreditPresentationModel> crew;
    private List<TvShowPresentationModel> similarTelevisionShows;
    private String rating;
    // endregion

    // region Constructors

    public TvShowDetailsPresentationModel() {
    }

    // endregion

    // region Getters


    public TvShowPresentationModel getTelevisionShow() {
        return televisionShow;
    }

    public List<TvShowCreditPresentationModel> getCast() {
        return cast;
    }

    public List<TvShowCreditPresentationModel> getCrew() {
        return crew;
    }

    public List<TvShowPresentationModel> getSimilarTelevisionShows() {
        return similarTelevisionShows;
    }

    public String getRating() {
        return rating;
    }

    // endregion

    // region Setters


    public void setTelevisionShow(TvShowPresentationModel televisionShow) {
        this.televisionShow = televisionShow;
    }

    public void setCast(List<TvShowCreditPresentationModel> cast) {
        this.cast = cast;
    }

    public void setCrew(List<TvShowCreditPresentationModel> crew) {
        this.crew = crew;
    }

    public void setSimilarTelevisionShows(List<TvShowPresentationModel> similarTelevisionShows) {
        this.similarTelevisionShows = similarTelevisionShows;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    // endregion

    @Override
    public String toString() {
        return "TelevisionShowDetailsPresentationModel{" +
                "televisionShow=" + televisionShow +
                ", cast=" + cast +
                ", crew=" + crew +
                ", similarTelevisionShows=" + similarTelevisionShows +
                ", rating='" + rating + '\'' +
                '}';
    }
}
