package com.vss.movieguide.presentation.mappers;

import java.util.List;

public interface PresentationModelListMapper<DomainModel, PresentationModel> {
    List<PresentationModel> mapListToPresentationModelList(List<DomainModel> domainModels);
}