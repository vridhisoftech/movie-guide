package com.vss.movieguide.presentation.artistdetails;

import com.vss.movieguide.domain.models.ArtistDetailsDomainModel;
import com.vss.movieguide.domain.usecases.ArtistDetailsDomainContract;
import com.vss.movieguide.presentation.models.MoviePresentationModel;
import com.vss.movieguide.presentation.models.TvShowPresentationModel;
import com.vss.movieguide.util.rxjava.SchedulerProvider;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;

public class ArtistDetailsPresenter implements ArtistDetailsPresentationContract.Presenter {

    // region Member Variables
    private final ArtistDetailsPresentationContract.View personDetailsView;
    private final ArtistDetailsDomainContract.UseCase personDetailsUseCase;
    private final SchedulerProvider schedulerProvider;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    // endregion

    // region Constructors

    public ArtistDetailsPresenter(ArtistDetailsPresentationContract.View personDetailsView, ArtistDetailsDomainContract.UseCase personDetailsUseCase, SchedulerProvider schedulerProvider) {
        this.personDetailsView = personDetailsView;
        this.personDetailsUseCase = personDetailsUseCase;
        this.schedulerProvider = schedulerProvider;
    }

    // endregion

    // region PersonDetailsPresentationContract.Presenter Methods

    @Override
    public void onDestroyView() {
        if (compositeDisposable != null)
            compositeDisposable.clear();
    }

    @Override
    public void onLoadPersonDetails(int personId) {
        Disposable disposable = personDetailsUseCase.getPersonDetails(personId)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribeWith(new DisposableSingleObserver<ArtistDetailsDomainModel>() {
                    @Override
                    public void onSuccess(ArtistDetailsDomainModel personDetailsDomainModel) {
                        if(personDetailsDomainModel != null){
                            personDetailsView.setPersonDetailsDomainModel(personDetailsDomainModel);
                        }
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        throwable.printStackTrace();

                        personDetailsView.showErrorView();
                    }
                });

        compositeDisposable.add(disposable);
    }

    @Override
    public void onMovieClick(MoviePresentationModel movie) {
        personDetailsView.openMovieDetails(movie);
    }

    @Override
    public void onTelevisionShowClick(TvShowPresentationModel televisionShow) {
        personDetailsView.openTelevisionShowDetails(televisionShow);
    }

    @Override
    public void onScrollChange(boolean isScrolledPastThreshold) {
        if(isScrolledPastThreshold)
            personDetailsView.showToolbarTitle();
        else
            personDetailsView.hideToolbarTitle();
    }

    // endregion
}
