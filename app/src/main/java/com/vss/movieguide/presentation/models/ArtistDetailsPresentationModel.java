package com.vss.movieguide.presentation.models;

import java.util.List;

/**
  3/4/17.
 */

public class ArtistDetailsPresentationModel {

    // region Fields
    private ArtistPresentationModel person;
    private List<ArtistCreditPresentationModel> cast;
    private List<ArtistCreditPresentationModel> crew;
    // endregion

    // region Constructors
    public ArtistDetailsPresentationModel() {
    }
    // endregion

    // region Getters

    public ArtistPresentationModel getPerson() {
        return person;
    }

    public List<ArtistCreditPresentationModel> getCast() {
        return cast;
    }

    public List<ArtistCreditPresentationModel> getCrew() {
        return crew;
    }

    // endregion

    // region Setters

    public void setPerson(ArtistPresentationModel person) {
        this.person = person;
    }

    public void setCast(List<ArtistCreditPresentationModel> cast) {
        this.cast = cast;
    }

    public void setCrew(List<ArtistCreditPresentationModel> crew) {
        this.crew = crew;
    }

    // endregion

    @Override
    public String toString() {
        return "PersonDetailsPresentationModel{" +
                "person=" + person +
                ", cast=" + cast +
                ", crew=" + crew +
                '}';
    }
}
