package com.vss.movieguide.presentation.mappers;

import com.vss.movieguide.domain.models.TvShowDomainModel;
import com.vss.movieguide.presentation.models.TvShowPresentationModel;

import java.util.ArrayList;
import java.util.List;

public class TvShowPresentationModelMapper implements PresentationModelMapper<TvShowDomainModel, TvShowPresentationModel>, PresentationModelListMapper<TvShowDomainModel, TvShowPresentationModel> {

    // region Member Variables
    private GenrePresentationModelMapper genrePresentationModelMapper = new GenrePresentationModelMapper();
    private NetworkPresentationModelMapper networkPresentationModelMapper = new NetworkPresentationModelMapper();
    // endregion

    @Override
    public TvShowPresentationModel mapToPresentationModel(TvShowDomainModel tvShowDomainModel) {
        TvShowPresentationModel tvShowPresentationModel = new TvShowPresentationModel();
        tvShowPresentationModel.setBackdropPath(tvShowDomainModel.getBackdropPath());
        tvShowPresentationModel.setEpisodeRunTime(tvShowDomainModel.getEpisodeRunTime());
        tvShowPresentationModel.setFirstAirDate(tvShowDomainModel.getFirstAirDate());
        tvShowPresentationModel.setGenres(genrePresentationModelMapper.mapListToPresentationModelList(tvShowDomainModel.getGenres()));
        tvShowPresentationModel.setHomepage(tvShowDomainModel.getHomepage());
        tvShowPresentationModel.setId(tvShowDomainModel.getId());
        tvShowPresentationModel.setInProduction(tvShowDomainModel.isInProduction());
        tvShowPresentationModel.setLanguages(tvShowDomainModel.getLanguages());
        tvShowPresentationModel.setLastAirDate(tvShowDomainModel.getLastAirDate());
        tvShowPresentationModel.setName(tvShowDomainModel.getName());
        tvShowPresentationModel.setNetworks(networkPresentationModelMapper.mapListToPresentationModelList(tvShowDomainModel.getNetworks()));
        tvShowPresentationModel.setNumberOfEpisodes(tvShowDomainModel.getNumberOfEpisodes());
        tvShowPresentationModel.setNumberOfSeasons(tvShowDomainModel.getNumberOfSeasons());
        tvShowPresentationModel.setOriginalLanguage(tvShowDomainModel.getOriginalLanguage());
        tvShowPresentationModel.setOriginalName(tvShowDomainModel.getOriginalName());
        tvShowPresentationModel.setOriginCountry(tvShowDomainModel.getOriginCountry());
        tvShowPresentationModel.setOverview(tvShowDomainModel.getOverview());
        tvShowPresentationModel.setPopularity(tvShowDomainModel.getPopularity());
        tvShowPresentationModel.setPosterPath(tvShowDomainModel.getPosterPath());
        tvShowPresentationModel.setStatus(tvShowDomainModel.getStatus());
        tvShowPresentationModel.setType(tvShowDomainModel.getType());
        tvShowPresentationModel.setVoteAverage(tvShowDomainModel.getVoteAverage());
        tvShowPresentationModel.setVoteCount(tvShowDomainModel.getVoteCount());
        return tvShowPresentationModel;
    }

    @Override
    public List<TvShowPresentationModel> mapListToPresentationModelList(List<TvShowDomainModel> tvShowDomainModels) {
        List<TvShowPresentationModel> tvShowPresentationModels = new ArrayList<>();
        if(tvShowDomainModels != null && tvShowDomainModels.size()>0) {
            for (TvShowDomainModel tvShowDomainModel : tvShowDomainModels) {
                tvShowPresentationModels.add(mapToPresentationModel(tvShowDomainModel));
            }
        }
        return tvShowPresentationModels;
    }
}