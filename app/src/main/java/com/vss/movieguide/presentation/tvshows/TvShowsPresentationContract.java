package com.vss.movieguide.presentation.tvshows;

import com.vss.movieguide.domain.models.TvShowDomainModel;
import com.vss.movieguide.domain.models.TvShowsDomainModel;
import com.vss.movieguide.presentation.base.BasePresenter;
import com.vss.movieguide.presentation.models.TvShowPresentationModel;

import java.util.List;

public interface TvShowsPresentationContract {

    interface View {
        void showEmptyView();
        void hideEmptyView();
        void showErrorView();
        void hideErrorView();
        void showLoadingView();
        void hideLoadingView();
        void addHeaderView();
        void addFooterView();
        void removeFooterView();
        void showErrorFooterView();
        void showLoadingFooterView();
        void showTelevisionShows(List<TvShowDomainModel> televisionShows);
        void loadMoreTelevisionShows();
        void setTelevisionShowsDomainModel(TvShowsDomainModel tvShowsDomainModel);

        // Navigation methods
        void openTelevisionShowDetails(TvShowPresentationModel televisionShow);
    }

    interface Presenter extends BasePresenter {
        void onLoadPopularTelevisionShows(int currentPage);
        void onTelevisionShowClick(TvShowPresentationModel televisionShow);
        void onScrollToEndOfList();
    }
}
