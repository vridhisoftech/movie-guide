package com.vss.movieguide.presentation.mappers;

import com.vss.movieguide.domain.models.ArtistDetailsDomainModel;
import com.vss.movieguide.presentation.models.ArtistDetailsPresentationModel;

public class ArtistDetailsPresentationModelMapper implements PresentationModelMapper<ArtistDetailsDomainModel, ArtistDetailsPresentationModel> {

    // region Member Variables
    private ArtistCreditPresentationModelMapper artistCreditPresentationModelMapper = new ArtistCreditPresentationModelMapper();
    private ArtistPresentationModelMapper artistPresentationModelMapper = new ArtistPresentationModelMapper();
    // endregion

    @Override
    public ArtistDetailsPresentationModel mapToPresentationModel(ArtistDetailsDomainModel artistDetailsDomainModel) {
        ArtistDetailsPresentationModel artistDetailsPresentationModel = new ArtistDetailsPresentationModel();
        artistDetailsPresentationModel.setCast(artistCreditPresentationModelMapper.mapListToPresentationModelList(artistDetailsDomainModel.getCast()));
        artistDetailsPresentationModel.setCrew(artistCreditPresentationModelMapper.mapListToPresentationModelList(artistDetailsDomainModel.getCrew()));
        artistDetailsPresentationModel.setPerson(artistPresentationModelMapper.mapToPresentationModel(artistDetailsDomainModel.getPerson()));
        return artistDetailsPresentationModel;
    }
}
