package com.vss.movieguide.presentation.mappers;

import com.vss.movieguide.domain.models.MovieCreditsDomainModel;
import com.vss.movieguide.presentation.models.MovieCreditsPresentationModel;

import java.util.Calendar;

public class MovieCreditsPresentationModelMapper implements PresentationModelMapper<MovieCreditsDomainModel, MovieCreditsPresentationModel> {

    // region Constants
    private static final int THIRTY_DAYS = 30;
    // endregion

    // region Member Variables
    private MovieCreditPresentationModelMapper movieCreditPresentationModelMapper = new MovieCreditPresentationModelMapper();
    // endregion

    @Override
    public MovieCreditsPresentationModel mapToPresentationModel(MovieCreditsDomainModel movieCreditsDomainModel) {
        MovieCreditsPresentationModel movieCreditsPresentationModel = new MovieCreditsPresentationModel();
        movieCreditsPresentationModel.setCast(movieCreditPresentationModelMapper.mapListToPresentationModelList(movieCreditsDomainModel.getCast()));
        movieCreditsPresentationModel.setCrew(movieCreditPresentationModelMapper.mapListToPresentationModelList(movieCreditsDomainModel.getCrew()));
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, THIRTY_DAYS);
        movieCreditsPresentationModel.setExpiredAt(calendar.getTime());
        movieCreditsPresentationModel.setId(movieCreditsDomainModel.getId());
        return movieCreditsPresentationModel;
    }
}
