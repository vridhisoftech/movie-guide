package com.vss.movieguide.presentation.moviedetails;

import com.vss.movieguide.domain.models.MovieDetailsDomainModel;
import com.vss.movieguide.presentation.base.BasePresenter;
import com.vss.movieguide.presentation.models.MoviePresentationModel;
import com.vss.movieguide.presentation.models.ArtistPresentationModel;

public interface MovieDetailsPresentationContract {

    interface View {
        void setMovieDetailsDomainModel(MovieDetailsDomainModel movieDetailsDomainModel);
        void showToolbarTitle();
        void hideToolbarTitle();
        void showErrorView();

        // Navigation methods
        void openPersonDetails(ArtistPresentationModel person);
        void openMovieDetails(MoviePresentationModel movie);
    }

    interface Presenter extends BasePresenter {
        void onLoadMovieDetails(int movieId);
        void onPersonClick(ArtistPresentationModel person);
        void onMovieClick(MoviePresentationModel movie);
        void onScrollChange(boolean isScrolledPastThreshold);
    }
}
