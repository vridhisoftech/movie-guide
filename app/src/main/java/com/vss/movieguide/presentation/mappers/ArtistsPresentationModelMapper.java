package com.vss.movieguide.presentation.mappers;

import com.vss.movieguide.domain.models.ArtistDomainModel;
import com.vss.movieguide.domain.models.ArtistsDomainModel;
import com.vss.movieguide.presentation.models.ArtistsPresentationModel;

import java.util.Calendar;
import java.util.List;

public class ArtistsPresentationModelMapper implements PresentationModelMapper<ArtistsDomainModel, ArtistsPresentationModel> {

    // region Constants
    private static final int PAGE_SIZE = 20;
    private static final int SEVEN_DAYS = 7;
    // endregion

    // region Member Variables
    private ArtistPresentationModelMapper artistPresentationModelMapper = new ArtistPresentationModelMapper();
    // endregion

    @Override
    public ArtistsPresentationModel mapToPresentationModel(ArtistsDomainModel artistsDomainModel) {
        ArtistsPresentationModel artistsPresentationModel = new ArtistsPresentationModel();
        List<ArtistDomainModel> artistDomainModels = artistsDomainModel.getPersons();
        artistsPresentationModel.setLastPage(artistDomainModels.size() < PAGE_SIZE);
        artistsPresentationModel.setPageNumber(artistsDomainModel.getPageNumber());
        artistsPresentationModel.setPersons(artistPresentationModelMapper.mapListToPresentationModelList(artistsDomainModel.getPersons()));
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, SEVEN_DAYS);
        artistsPresentationModel.setExpiredAt(calendar.getTime());
        return artistsPresentationModel;
    }
}
