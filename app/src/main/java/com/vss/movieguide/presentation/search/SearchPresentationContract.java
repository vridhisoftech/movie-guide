package com.vss.movieguide.presentation.search;

import com.vss.movieguide.domain.models.MovieDomainModel;
import com.vss.movieguide.domain.models.ArtistDomainModel;
import com.vss.movieguide.domain.models.TvShowDomainModel;
import com.vss.movieguide.presentation.base.BasePresenter;
import com.vss.movieguide.presentation.models.MoviePresentationModel;
import com.vss.movieguide.presentation.models.ArtistPresentationModel;
import com.vss.movieguide.presentation.models.TvShowPresentationModel;

import java.util.List;

import io.reactivex.Observable;

public interface SearchPresentationContract {

    interface View {
        void showEmptyView();
        void hideEmptyView();
        void setEmptyText(String emptyText);
        void showLoadingView();
        void hideLoadingView();
        void showErrorView();
        void addMoviesToAdapter(List<MovieDomainModel> movies);
        void clearMoviesAdapter();
        void hideMoviesView();
        void showMoviesView();
        void addTelevisionShowsToAdapter(List<TvShowDomainModel> televisionShows);
        void clearTelevisionShowsAdapter();
        void hideTelevisionShowsView();
        void showTelevisionShowsView();
        void addPersonsToAdapter(List<ArtistDomainModel> persons);
        void clearPersonsAdapter();
        void hidePersonsView();
        void showPersonsView();

        // Navigation methods
        void openMovieDetails(MoviePresentationModel movie);
        void openTelevisionShowDetails(TvShowPresentationModel televisionShow);
        void openPersonDetails(ArtistPresentationModel person);
    }

    interface Presenter extends BasePresenter {
        void onLoadSearch(Observable<CharSequence> searchQueryChangeObservable);
        void onMovieClick(MoviePresentationModel movie);
        void onTelevisionShowClick(TvShowPresentationModel televisionShow);
        void onPersonClick(ArtistPresentationModel person);
    }
}
