package com.vss.movieguide.presentation.models;

import java.util.List;

public class ArtistCreditsPresentationModel {

    // region Fields
    public int id;
    public List<ArtistCreditPresentationModel> cast = null;
    public List<ArtistCreditPresentationModel> crew = null;
    // endregion

    // region Getters

    public int getId() {
        return id;
    }

    public List<ArtistCreditPresentationModel> getCast() {
        return cast;
    }

    public List<ArtistCreditPresentationModel> getCrew() {
        return crew;
    }

    // endregion

    // region Setters

    public void setId(int id) {
        this.id = id;
    }

    public void setCast(List<ArtistCreditPresentationModel> cast) {
        this.cast = cast;
    }

    public void setCrew(List<ArtistCreditPresentationModel> crew) {
        this.crew = crew;
    }

    // endregion

    @Override
    public String toString() {
        return "PersonCreditsPresentationModel{" +
                "id=" + id +
                ", cast=" + cast +
                ", crew=" + crew +
                '}';
    }
}
