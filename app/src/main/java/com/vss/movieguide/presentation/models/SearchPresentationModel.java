package com.vss.movieguide.presentation.models;

import java.util.List;

public class SearchPresentationModel {

    // region Fields
    private String query;
    private List<MoviePresentationModel> movies;
    private List<TvShowPresentationModel> televisionShows;
    private List<ArtistPresentationModel> persons;
    // endregion

    // region Constructors
    public SearchPresentationModel() {
    }
    // endregion

    // region Getters

    public String getQuery() {
        return query;
    }

    public List<MoviePresentationModel> getMovies() {
        return movies;
    }

    public List<TvShowPresentationModel> getTelevisionShows() {
        return televisionShows;
    }

    public List<ArtistPresentationModel> getPersons() {
        return persons;
    }

    // endregion

    // region Setters

    public void setQuery(String query) {
        this.query = query;
    }

    public void setMovies(List<MoviePresentationModel> movies) {
        this.movies = movies;
    }

    public void setTelevisionShows(List<TvShowPresentationModel> televisionShows) {
        this.televisionShows = televisionShows;
    }

    public void setPersons(List<ArtistPresentationModel> persons) {
        this.persons = persons;
    }

    // endregion

    // region Helper Methods
    public boolean hasMovies() {
        return movies != null && movies.size() > 0;
    }

    public boolean hasTelevisionShows() {
        return televisionShows != null && televisionShows.size() > 0;
    }

    public boolean hasPersons() {
        return persons != null && persons.size() > 0;
    }

    public boolean hasResults(){
        return (hasMovies())
                || (hasTelevisionShows())
                || (hasPersons());
    }
    // endregion

    @Override
    public String toString() {
        return "SearchPresentationModel{" +
                "query='" + query + '\'' +
                ", movies=" + movies +
                ", televisionShows=" + televisionShows +
                ", persons=" + persons +
                '}';
    }
}
