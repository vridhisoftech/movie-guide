package com.vss.movieguide.presentation.artists;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;

import androidx.annotation.Nullable;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.fragment.app.Fragment;

import com.vss.movieguide.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class SettingsFragment extends Fragment {

    @BindView(R.id.share)
    CheckedTextView share;
    @BindView(R.id.rate)
    CheckedTextView rate;
    @BindView(R.id.feedback)
    CheckedTextView feedback;
    @BindView(R.id.tnc)
    CheckedTextView tnc;
    @BindView(R.id.privacy)
    CheckedTextView privacy;
    private Unbinder unbinder;

    public SettingsFragment() {
        // Required empty public constructor
    }


    public static SettingsFragment newInstance() {
        return new SettingsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_settings, container, false);
        unbinder = ButterKnife.bind(this, rootView);

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        share.setOnClickListener(v -> {
            Uri uri = Uri.parse("http://play.google.com/store/apps/details?id=com.vss.movieguide");
            shareApp(uri);
        });
        rate.setOnClickListener(v -> {
            rateApp();
        });
        feedback.setOnClickListener(v -> {
            openCustomTab("https://www.vridhisoftech.com/contact/");
        });
        tnc.setOnClickListener(v -> {
          openCustomTab("https://vridhisoftech.co.in/newsvolt/privacy-policy/");
        });
        privacy.setOnClickListener(v -> {
            openCustomTab("https://vridhisoftech.co.in/newsvolt/privacy-policy/");
        });
    }

    private void rateApp(){
        Uri uri = Uri.parse("market://details?id=com.vss.movieguide");
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=com.vss.movieguide")));
        }
    }

    private void shareApp(Uri uri) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        String shareText = "Download this app:\n"+uri.toString();
        intent.putExtra(Intent.EXTRA_TEXT, shareText);
        intent.setType("text/plain");
        startActivity(intent);
    }

    private void openCustomTab(String url){
        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
        CustomTabsIntent customTabsIntent = builder.build();
        customTabsIntent.launchUrl(getContext(), Uri.parse(url));
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}