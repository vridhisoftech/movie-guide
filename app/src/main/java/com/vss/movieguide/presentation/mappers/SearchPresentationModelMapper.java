package com.vss.movieguide.presentation.mappers;

import com.vss.movieguide.domain.models.SearchDomainModel;
import com.vss.movieguide.presentation.models.SearchPresentationModel;


public class SearchPresentationModelMapper implements PresentationModelMapper<SearchDomainModel, SearchPresentationModel> {

    // region Member Variables
    private MoviePresentationModelMapper moviePresentationModelMapper = new MoviePresentationModelMapper();
    private ArtistPresentationModelMapper artistPresentationModelMapper = new ArtistPresentationModelMapper();
    private TvShowPresentationModelMapper tvShowPresentationModelMapper = new TvShowPresentationModelMapper();
    // endregion

    @Override
    public SearchPresentationModel mapToPresentationModel(SearchDomainModel searchDomainModel) {
        SearchPresentationModel searchPresentationModel = new SearchPresentationModel();
        searchPresentationModel.setMovies(moviePresentationModelMapper.mapListToPresentationModelList(searchDomainModel.getMovies()));
        searchPresentationModel.setPersons(artistPresentationModelMapper.mapListToPresentationModelList(searchDomainModel.getPersons()));
        searchPresentationModel.setQuery(searchDomainModel.getQuery());
        searchPresentationModel.setTelevisionShows(tvShowPresentationModelMapper.mapListToPresentationModelList(searchDomainModel.getTelevisionShows()));

        return searchPresentationModel;
    }
}
