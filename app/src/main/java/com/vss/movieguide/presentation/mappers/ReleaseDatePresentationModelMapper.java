package com.vss.movieguide.presentation.mappers;

import com.vss.movieguide.domain.models.ReleaseDateDomainModel;
import com.vss.movieguide.presentation.models.ReleaseDatePresentationModel;

import java.util.ArrayList;
import java.util.List;

public class ReleaseDatePresentationModelMapper implements PresentationModelMapper<ReleaseDateDomainModel, ReleaseDatePresentationModel>, PresentationModelListMapper<ReleaseDateDomainModel, ReleaseDatePresentationModel> {

    @Override
    public ReleaseDatePresentationModel mapToPresentationModel(ReleaseDateDomainModel releaseDateDomainModel) {
        ReleaseDatePresentationModel releaseDatePresentationModel = new ReleaseDatePresentationModel();
        releaseDatePresentationModel.setCertification(releaseDateDomainModel.getCertification());
        releaseDatePresentationModel.setIso6391(releaseDateDomainModel.getIso6391());
        releaseDatePresentationModel.setNote(releaseDateDomainModel.getNote());
        releaseDatePresentationModel.setReleaseDate(releaseDateDomainModel.getReleaseDate());
        releaseDatePresentationModel.setType(releaseDateDomainModel.getType());
        return releaseDatePresentationModel;
    }

    @Override
    public List<ReleaseDatePresentationModel> mapListToPresentationModelList(List<ReleaseDateDomainModel> releaseDateDomainModels) {
        List<ReleaseDatePresentationModel> releaseDatePresentationModels = new ArrayList<>();
        if(releaseDateDomainModels != null && releaseDateDomainModels.size()>0) {
            for (ReleaseDateDomainModel releaseDateDomainModel : releaseDateDomainModels) {
                releaseDatePresentationModels.add(mapToPresentationModel(releaseDateDomainModel));
            }
        }
        return releaseDatePresentationModels;
    }
}
