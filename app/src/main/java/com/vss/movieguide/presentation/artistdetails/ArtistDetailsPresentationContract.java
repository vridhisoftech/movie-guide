package com.vss.movieguide.presentation.artistdetails;

import com.vss.movieguide.domain.models.ArtistDetailsDomainModel;
import com.vss.movieguide.presentation.base.BasePresenter;
import com.vss.movieguide.presentation.models.MoviePresentationModel;
import com.vss.movieguide.presentation.models.TvShowPresentationModel;

public interface ArtistDetailsPresentationContract {

    interface View {
        void setPersonDetailsDomainModel(ArtistDetailsDomainModel artistDetailsDomainModel);
        void showToolbarTitle();
        void hideToolbarTitle();
        void showErrorView();

        // Navigation methods
        void openMovieDetails(MoviePresentationModel movie);
        void openTelevisionShowDetails(TvShowPresentationModel televisionShow);
    }

    interface Presenter extends BasePresenter {
        void onLoadPersonDetails(int personId);
        void onMovieClick(MoviePresentationModel movie);
        void onTelevisionShowClick(TvShowPresentationModel televisionShow);
        void onScrollChange(boolean isScrolledPastThreshold);
    }
}
