package com.vss.movieguide.presentation.main;

public class MainPresenter implements MainContract.Presenter {

    // region Member Variables
    private final MainContract.View mainView;
    // endregion

    // region Constructors
    public MainPresenter(MainContract.View mainView) {
        this.mainView = mainView;
    }
    // endregion

    // region MainContract.Presenter Methods
    @Override
    public void viewSearch() {
        mainView.viewSearch();
    }
    // endregion
}
