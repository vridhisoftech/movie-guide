package com.vss.movieguide.presentation.artists;

import android.content.res.Resources;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.vss.movieguide.MovieGuide;
import com.vss.movieguide.R;
import com.vss.movieguide.di.component.ArtistsComponent;
import com.vss.movieguide.di.module.ArtistsModule;
import com.vss.movieguide.domain.models.ArtistDomainModel;
import com.vss.movieguide.domain.models.ArtistsDomainModel;
import com.vss.movieguide.presentation.base.BaseAdapter;
import com.vss.movieguide.presentation.base.BaseFragment;
import com.vss.movieguide.presentation.mappers.ArtistPresentationModelMapper;
import com.vss.movieguide.presentation.mappers.ArtistsPresentationModelMapper;
import com.vss.movieguide.presentation.models.ArtistPresentationModel;
import com.vss.movieguide.presentation.models.ArtistsPresentationModel;
import com.vss.movieguide.presentation.artistdetails.ArtistDetailsActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;

public class ArtistsFragment extends BaseFragment implements ArtistsAdapter.OnItemClickListener, ArtistsAdapter.OnReloadClickListener, ArtistsPresentationContract.View {

    // region Views
    @BindView(R.id.rv)
    RecyclerView recyclerView;
    @BindView(R.id.error_ll)
    LinearLayout errorLinearLayout;
    @BindView(R.id.pb)
    ProgressBar progressBar;
    @BindView(android.R.id.empty)
    LinearLayout emptyLinearLayout;

    private View selectedPersonView;
    // endregion

    // region Member Variables
    private ArtistsAdapter artistsAdapter;
    private Unbinder unbinder;
    private StaggeredGridLayoutManager layoutManager;
    private ArtistsPresentationModel artistsPresentationModel;
    private ArtistsComponent artistsComponent;
    private boolean isLoading = false;
    private ArtistsPresentationModelMapper artistsPresentationModelMapper = new ArtistsPresentationModelMapper();
    private ArtistPresentationModelMapper artistPresentationModelMapper = new ArtistPresentationModelMapper();
    // endregion

    // region Injected Variables
    @Inject
    ArtistsPresentationContract.Presenter personsPresenter;
    // endregion

    // region Listeners
    @OnClick(R.id.retry_btn)
    public void onReloadButtonClicked() {
        personsPresenter.onLoadPopularPersons(artistsPresentationModel == null ? 1 : artistsPresentationModel.getPageNumber());
    }

    private RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(final RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            int visibleItemCount = recyclerView.getChildCount();
            int totalItemCount = recyclerView.getAdapter().getItemCount();
            int[] positions = layoutManager.findFirstVisibleItemPositions(null);
            int firstVisibleItem = positions[1];

            if ((visibleItemCount + firstVisibleItem) >= totalItemCount
                    && totalItemCount > 0
                    && !isLoading
                    && !artistsPresentationModel.isLastPage()) {
                personsPresenter.onScrollToEndOfList();
            }
        }
    };

    // endregion

    // region Constructors
    public ArtistsFragment() {
    }
    // endregion

    // region Factory Methods
    public static ArtistsFragment newInstance() {
        return new ArtistsFragment();
    }

    public static ArtistsFragment newInstance(Bundle extras) {
        ArtistsFragment fragment = new ArtistsFragment();
        fragment.setArguments(extras);
        return fragment;
    }
    // endregion

    // region Lifecycle Methods
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        createPersonsComponent().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_artist, container, false);
        unbinder = ButterKnife.bind(this, rootView);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);

        recyclerView.setLayoutManager(layoutManager);
        artistsAdapter = new ArtistsAdapter(getContext());
        artistsAdapter.setOnItemClickListener(this);
        artistsAdapter.setOnReloadClickListener(this);
        recyclerView.setItemAnimator(new SlideInUpAnimator());
        recyclerView.setAdapter(artistsAdapter);

        // Pagination
        recyclerView.addOnScrollListener(recyclerViewOnScrollListener);

        personsPresenter.onLoadPopularPersons(artistsPresentationModel == null ? 1 : artistsPresentationModel.getPageNumber());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        removeListeners();
        unbinder.unbind();
        personsPresenter.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        releasePersonsComponent();
    }
    // endregion

    // region PersonsAdapter.OnItemClickListener Methods
    @Override
    public void onItemClick(int position, View view) {
        selectedPersonView = view;
        ArtistPresentationModel person = artistsAdapter.getItem(position);
        if(person != null){
            personsPresenter.onPersonClick(person);
        }
    }
    // endregion

    // region PersonsAdapter.OnReloadClickListener Methods
    @Override
    public void onReloadClick() {
        personsPresenter.onLoadPopularPersons(artistsPresentationModel.getPageNumber());
    }
    // endregion

    // region PersonsPresentationContract.View Methods

    @Override
    public void showEmptyView() {
        emptyLinearLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmptyView() {
        emptyLinearLayout.setVisibility(View.GONE);
    }

    @Override
    public void showErrorView() {
        errorLinearLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideErrorView() {
        errorLinearLayout.setVisibility(View.GONE);
    }

    @Override
    public void showLoadingView() {
        progressBar.setVisibility(View.VISIBLE);
        isLoading = true;
    }

    @Override
    public void hideLoadingView() {
        progressBar.setVisibility(View.GONE);
        isLoading = false;
    }

    @Override
    public void addHeaderView() {
        artistsAdapter.addHeader();
    }

    @Override
    public void addFooterView() {
        artistsAdapter.addFooter();
    }

    @Override
    public void removeFooterView() {
        artistsAdapter.removeFooter();
        isLoading = false;
    }

    @Override
    public void showErrorFooterView() {
        artistsAdapter.updateFooter(BaseAdapter.FooterType.ERROR);
    }

    @Override
    public void showLoadingFooterView() {
        artistsAdapter.updateFooter(BaseAdapter.FooterType.LOAD_MORE);
        isLoading = true;
    }

    @Override
    public void showPersons(List<ArtistDomainModel> persons) {
        artistsAdapter.addAll(artistPresentationModelMapper.mapListToPresentationModelList(persons));
    }

    @Override
    public void loadMorePersons() {
        artistsPresentationModel.incrementPageNumber();
        personsPresenter.onLoadPopularPersons(artistsPresentationModel.getPageNumber());
    }

    @Override
    public void setPersonsDomainModel(ArtistsDomainModel artistsDomainModel) {
        this.artistsPresentationModel = artistsPresentationModelMapper.mapToPresentationModel(artistsDomainModel);
    }

    @Override
    public void openPersonDetails(ArtistPresentationModel person) {
        Window window = getActivity().getWindow();
//            window.setStatusBarColor(primaryDark);

        Pair<View, String> personPair  = getPersonPair();
        ActivityOptionsCompat options = getActivityOptionsCompat(personPair);

        window.setExitTransition(null);
        ActivityCompat.startActivity(getActivity(), ArtistDetailsActivity.createIntent(getContext(), person), options.toBundle());
    }

    // endregion

    // region Helper Methods
    private void removeListeners() {
        artistsAdapter.setOnItemClickListener(null);
    }

    private ActivityOptionsCompat getActivityOptionsCompat(Pair pair){
        ActivityOptionsCompat options = null;

        Pair<View, String> bottomNavigationViewPair = getBottomNavigationViewPair();
        Pair<View, String> statusBarPair = getStatusBarPair();
        Pair<View, String> navigationBarPair  = getNavigationBarPair();
        Pair<View, String> appBarPair  = getAppBarPair();

        if(pair!=null
                && bottomNavigationViewPair != null
                && statusBarPair!= null
                && navigationBarPair!= null
                && appBarPair!= null){
            options = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(),
                    pair, bottomNavigationViewPair, statusBarPair, navigationBarPair, appBarPair);
        } else if(pair != null
                && bottomNavigationViewPair != null
                && statusBarPair != null
                && appBarPair!= null){
            options = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(),
                    pair, bottomNavigationViewPair, statusBarPair, appBarPair);
        } else if(pair != null
                && bottomNavigationViewPair != null
                && navigationBarPair != null
                && appBarPair!= null){
            options = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(),
                    pair, bottomNavigationViewPair, navigationBarPair, appBarPair);
        }

        return options;
    }

    private Pair<View, String> getPersonPair(){
        Resources resources = getResources();
        String transitionName = resources.getString(R.string.transition_person_thumbnail);
        View view = selectedPersonView.findViewById(R.id.thumbnail_iv);
        return getPair(view, transitionName);
    }

    private Pair<View, String> getBottomNavigationViewPair(){
        Resources resources = getResources();
        String transitionName = resources.getString(R.string.transition_bottom_navigation);
        View view = getActivity().findViewById(R.id.bottom_navigation);
        return getPair(view, transitionName);
    }

    private Pair<View, String> getStatusBarPair(){
        View view = getActivity().findViewById(android.R.id.statusBarBackground);
        return getPair(view);
    }

    private Pair<View, String> getNavigationBarPair(){
        View view = getActivity().findViewById(android.R.id.navigationBarBackground);
        return getPair(view);
    }

    private Pair<View, String> getAppBarPair(){
        Resources resources = getResources();
        String transitionName = resources.getString(R.string.transition_app_bar);
        View view = getActivity().findViewById(R.id.appbar);
        return getPair(view, transitionName);
    }

    private Pair<View, String> getPair(View view, String transitionName){
        Pair<View, String> pair = null;
        if(view != null) {
            pair = Pair.create(view, transitionName);
        }
        return pair;
    }

    private Pair<View, String> getPair(View view){
        Pair<View, String> pair = null;
        if(view != null) {
            pair = Pair.create(view, view.getTransitionName());
        }
        return pair;
    }

    public void scrollToTop(){
        recyclerView.scrollToPosition(0);
    }

    private ArtistsComponent createPersonsComponent(){
        artistsComponent = ((MovieGuide)getActivity().getApplication())
                .getApplicationComponent()
                .createSubcomponent(new ArtistsModule(this));
        return artistsComponent;
    }

    public void releasePersonsComponent(){
        artistsComponent = null;
    }
    // endregion
}
