package com.vss.movieguide.presentation.mappers;

import com.vss.movieguide.domain.models.TvShowCreditsDomainModel;
import com.vss.movieguide.presentation.models.TvShowCreditsPresentationModel;

public class TvShowCreditsPresentationModelMapper implements PresentationModelMapper<TvShowCreditsDomainModel, TvShowCreditsPresentationModel> {

    // region Member Variables
    private TvCreditPresentationModelMapper tvCreditPresentationModelMapper = new TvCreditPresentationModelMapper();
    // endregion

    @Override
    public TvShowCreditsPresentationModel mapToPresentationModel(TvShowCreditsDomainModel tvShowCreditsDomainModel) {
        TvShowCreditsPresentationModel tvShowCreditsPresentationModel = new TvShowCreditsPresentationModel();
        tvShowCreditsPresentationModel.setId(tvShowCreditsDomainModel.getId());
        tvShowCreditsPresentationModel.setCast(tvCreditPresentationModelMapper.mapListToPresentationModelList(tvShowCreditsDomainModel.getCast()));
        tvShowCreditsPresentationModel.setCrew(tvCreditPresentationModelMapper.mapListToPresentationModelList(tvShowCreditsDomainModel.getCrew()));
        return tvShowCreditsPresentationModel;
    }
}
