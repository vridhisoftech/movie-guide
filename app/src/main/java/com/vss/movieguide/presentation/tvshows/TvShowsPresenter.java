package com.vss.movieguide.presentation.tvshows;

import com.vss.movieguide.domain.models.TvShowDomainModel;
import com.vss.movieguide.domain.models.TvShowsDomainModel;
import com.vss.movieguide.domain.usecases.TvShowsDomainContract;
import com.vss.movieguide.presentation.models.TvShowPresentationModel;
import com.vss.movieguide.util.rxjava.SchedulerProvider;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;

public class TvShowsPresenter implements TvShowsPresentationContract.Presenter {

    // region Member Variables
    private final TvShowsPresentationContract.View televisionShowsView;
    private final TvShowsDomainContract.UseCase televisionShowsUseCase;
    private final SchedulerProvider schedulerProvider;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    // endregion

    // region Constructors
    public TvShowsPresenter(TvShowsPresentationContract.View televisionShowsView, TvShowsDomainContract.UseCase televisionShowsUseCase, SchedulerProvider schedulerProvider) {
        this.televisionShowsView = televisionShowsView;
        this.televisionShowsUseCase = televisionShowsUseCase;
        this.schedulerProvider = schedulerProvider;
    }
    // endregion

    // region TelevisionShowsPresentationContract.Presenter Methods

    @Override
    public void onDestroyView() {
        if (compositeDisposable != null)
            compositeDisposable.clear();
    }

    @Override
    public void onLoadPopularTelevisionShows(final int currentPage) {
        if(currentPage == 1){
            televisionShowsView.hideEmptyView();
            televisionShowsView.hideErrorView();
            televisionShowsView.showLoadingView();
        } else{
            televisionShowsView.showLoadingFooterView();
        }

        Disposable disposable = televisionShowsUseCase.getPopularTelevisionShows(currentPage)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribeWith(new DisposableSingleObserver<TvShowsDomainModel>() {
                    @Override
                    public void onSuccess(TvShowsDomainModel televisionShowsDomainModel) {
                        if(televisionShowsDomainModel != null){
                            List<TvShowDomainModel> tvShowDomainModels = televisionShowsDomainModel.getTelevisionShows();
                            int currentPage = televisionShowsDomainModel.getPageNumber();
                            boolean isLastPage = televisionShowsDomainModel.isLastPage();
                            boolean hasTelevisionShows = televisionShowsDomainModel.hasTelevisionShows();
                            if(currentPage == 1){
                                televisionShowsView.hideLoadingView();

                                if(hasTelevisionShows){
                                    televisionShowsView.addHeaderView();
                                    televisionShowsView.showTelevisionShows(tvShowDomainModels);

                                    if(!isLastPage)
                                        televisionShowsView.addFooterView();
                                } else {
                                    televisionShowsView.showEmptyView();
                                }
                            } else {
                                televisionShowsView.removeFooterView();

                                if(hasTelevisionShows){
                                    televisionShowsView.showTelevisionShows(tvShowDomainModels);

                                    if(!isLastPage)
                                        televisionShowsView.addFooterView();
                                }
                            }

                            televisionShowsView.setTelevisionShowsDomainModel(televisionShowsDomainModel);
                        }
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        throwable.printStackTrace();

                        if(currentPage == 1){
                            televisionShowsView.hideLoadingView();

                            televisionShowsView.showErrorView();
                        } else {
                            televisionShowsView.showErrorFooterView();
                        }
                    }
                });

        compositeDisposable.add(disposable);
    }

    @Override
    public void onTelevisionShowClick(TvShowPresentationModel televisionShow) {
        televisionShowsView.openTelevisionShowDetails(televisionShow);
    }

    @Override
    public void onScrollToEndOfList() {
        televisionShowsView.loadMoreTelevisionShows();
    }

    // endregion
}
