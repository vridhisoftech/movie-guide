package com.vss.movieguide.presentation.tvshows;

import android.content.res.Resources;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.vss.movieguide.MovieGuide;
import com.vss.movieguide.R;
import com.vss.movieguide.di.component.TvShowsComponent;
import com.vss.movieguide.di.module.TvShowsModule;
import com.vss.movieguide.domain.models.TvShowDomainModel;
import com.vss.movieguide.domain.models.TvShowsDomainModel;
import com.vss.movieguide.presentation.base.BaseAdapter;
import com.vss.movieguide.presentation.base.BaseFragment;
import com.vss.movieguide.presentation.mappers.TvShowPresentationModelMapper;
import com.vss.movieguide.presentation.mappers.TvShowsPresentationModelMapper;
import com.vss.movieguide.presentation.models.TvShowPresentationModel;
import com.vss.movieguide.presentation.models.TvShowsPresentationModel;
import com.vss.movieguide.presentation.tvishowdetails.TvShowDetailsActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;

public class TvShowsFragment extends BaseFragment implements TvShowsAdapter.OnItemClickListener, TvShowsAdapter.OnReloadClickListener, TvShowsPresentationContract.View {

    // region Views
    @BindView(R.id.rv)
    RecyclerView recyclerView;
    @BindView(R.id.error_ll)
    LinearLayout errorLinearLayout;
    @BindView(R.id.pb)
    ProgressBar progressBar;
    @BindView(android.R.id.empty)
    LinearLayout emptyLinearLayout;

    private View selectedTelevisionShowView;
    // endregion

    // region Member Variables
    private TvShowsAdapter tvShowsAdapter;
    private Unbinder unbinder;
    private StaggeredGridLayoutManager layoutManager;
    private boolean isLoading = false;
    private TvShowsPresentationModel tvShowsPresentationModel;
    private TvShowsComponent tvShowsComponent;
    private TvShowsPresentationModelMapper tvShowsPresentationModelMapper = new TvShowsPresentationModelMapper();
    private TvShowPresentationModelMapper tvShowPresentationModelMapper = new TvShowPresentationModelMapper();

    // endregion

    // region Injected Variables
    @Inject
    TvShowsPresentationContract.Presenter televisionShowsPresenter;
    // endregion

    // region Listeners
    @OnClick(R.id.retry_btn)
    public void onReloadButtonClicked() {
        televisionShowsPresenter.onLoadPopularTelevisionShows(tvShowsPresentationModel == null ? 1 : tvShowsPresentationModel.getPageNumber());
    }

    private RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(final RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            int visibleItemCount = recyclerView.getChildCount();
            int totalItemCount = recyclerView.getAdapter().getItemCount();
            int[] positions = layoutManager.findFirstVisibleItemPositions(null);
            int firstVisibleItem = positions[1];

            if ((visibleItemCount + firstVisibleItem) >= totalItemCount
                    && totalItemCount > 0
                    && !isLoading
                    && !tvShowsPresentationModel.isLastPage()) {
                televisionShowsPresenter.onScrollToEndOfList();
            }
        }
    };

    // endregion

    // region Constructors
    public TvShowsFragment() {
    }
    // endregion

    // region Factory Methods
    public static TvShowsFragment newInstance() {
        return new TvShowsFragment();
    }

    public static TvShowsFragment newInstance(Bundle extras) {
        TvShowsFragment fragment = new TvShowsFragment();
        fragment.setArguments(extras);
        return fragment;
    }
    // endregion

    // region Lifecycle Methods
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        createTelevisionShowsComponent().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_tv_shows, container, false);
        unbinder = ButterKnife.bind(this, rootView);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);

        recyclerView.setLayoutManager(layoutManager);
        tvShowsAdapter = new TvShowsAdapter(getContext());
        tvShowsAdapter.setOnItemClickListener(this);
        tvShowsAdapter.setOnReloadClickListener(this);
        recyclerView.setItemAnimator(new SlideInUpAnimator());
        recyclerView.setAdapter(tvShowsAdapter);

        // Pagination
        recyclerView.addOnScrollListener(recyclerViewOnScrollListener);

        televisionShowsPresenter.onLoadPopularTelevisionShows(tvShowsPresentationModel == null ? 1 : tvShowsPresentationModel.getPageNumber());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        removeListeners();
        unbinder.unbind();
        televisionShowsPresenter.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        releaseTelevisionShowsComponent();
    }
    // endregion

    // region TelevisionShowsAdapter.OnItemClickListener Methods
    @Override
    public void onItemClick(int position, View view) {
        selectedTelevisionShowView = view;

        TvShowPresentationModel televisionShow = tvShowsAdapter.getItem(position);
        if(televisionShow != null){
            televisionShowsPresenter.onTelevisionShowClick(televisionShow);
        }
    }
    // endregion

    // region TelevisionShowsAdapter.OnReloadClickListener Methods
    @Override
    public void onReloadClick() {
        televisionShowsPresenter.onLoadPopularTelevisionShows(tvShowsPresentationModel.getPageNumber());
    }
    // endregion

    // region TelevisionShowsPresentationContract.View Methods

    @Override
    public void showEmptyView() {
        emptyLinearLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmptyView() {
        emptyLinearLayout.setVisibility(View.GONE);
    }

    @Override
    public void showErrorView() {
        errorLinearLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideErrorView() {
        errorLinearLayout.setVisibility(View.GONE);
    }

    @Override
    public void showLoadingView() {
        progressBar.setVisibility(View.VISIBLE);
        isLoading = true;
    }

    @Override
    public void hideLoadingView() {
        progressBar.setVisibility(View.GONE);
        isLoading = false;
    }

    @Override
    public void addHeaderView() {
        tvShowsAdapter.addHeader();
    }

    @Override
    public void addFooterView() {
        tvShowsAdapter.addFooter();
    }

    @Override
    public void removeFooterView() {
        tvShowsAdapter.removeFooter();
        isLoading = false;
    }

    @Override
    public void showErrorFooterView() {
        tvShowsAdapter.updateFooter(BaseAdapter.FooterType.ERROR);
    }

    @Override
    public void showLoadingFooterView() {
        tvShowsAdapter.updateFooter(BaseAdapter.FooterType.LOAD_MORE);
        isLoading = true;
    }

    @Override
    public void showTelevisionShows(List<TvShowDomainModel> televisionShows) {
        tvShowsAdapter.addAll(tvShowPresentationModelMapper.mapListToPresentationModelList(televisionShows));
    }

    @Override
    public void loadMoreTelevisionShows() {
        tvShowsPresentationModel.incrementPageNumber();
        televisionShowsPresenter.onLoadPopularTelevisionShows(tvShowsPresentationModel.getPageNumber());
    }

    @Override
    public void setTelevisionShowsDomainModel(TvShowsDomainModel tvShowsDomainModel) {
        this.tvShowsPresentationModel = tvShowsPresentationModelMapper.mapToPresentationModel(tvShowsDomainModel);
    }

    @Override
    public void openTelevisionShowDetails(TvShowPresentationModel televisionShow) {
        Window window = getActivity().getWindow();
//            window.setStatusBarColor(primaryDark);

        Pair<View, String> televisionShowPair  = getTelevisionShowPair();
        ActivityOptionsCompat options = getActivityOptionsCompat(televisionShowPair);

        window.setExitTransition(null);
        ActivityCompat.startActivity(getActivity(), TvShowDetailsActivity.createIntent(getContext(), televisionShow), options.toBundle());
    }
    // endregion

    // region Helper Methods
    private void removeListeners() {
        tvShowsAdapter.setOnItemClickListener(null);
    }

    private ActivityOptionsCompat getActivityOptionsCompat(Pair pair){
        ActivityOptionsCompat options = null;

        Pair<View, String> bottomNavigationViewPair = getBottomNavigationViewPair();
        Pair<View, String> statusBarPair = getStatusBarPair();
        Pair<View, String> navigationBarPair  = getNavigationBarPair();
        Pair<View, String> appBarPair  = getAppBarPair();

        if(pair!=null
                && bottomNavigationViewPair != null
                && statusBarPair!= null
                && navigationBarPair!= null
                && appBarPair!= null){
            options = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(),
                    pair, bottomNavigationViewPair, statusBarPair, navigationBarPair, appBarPair);
        } else if(pair != null
                && bottomNavigationViewPair != null
                && statusBarPair != null
                && appBarPair!= null){
            options = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(),
                    pair, bottomNavigationViewPair, statusBarPair, appBarPair);
        } else if(pair != null
                && bottomNavigationViewPair != null
                && navigationBarPair != null
                && appBarPair!= null){
            options = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(),
                    pair, bottomNavigationViewPair, navigationBarPair, appBarPair);
        }

        return options;
    }

    private Pair<View, String> getTelevisionShowPair(){
        Resources resources = getResources();
        String transitionName = resources.getString(R.string.transition_television_show_thumbnail);
        View view = selectedTelevisionShowView.findViewById(R.id.thumbnail_iv);
        return getPair(view, transitionName);
    }

    private Pair<View, String> getBottomNavigationViewPair(){
        Resources resources = getResources();
        String transitionName = resources.getString(R.string.transition_bottom_navigation);
        View view = getActivity().findViewById(R.id.bottom_navigation);
        return getPair(view, transitionName);
    }

    private Pair<View, String> getStatusBarPair(){
        View view = getActivity().findViewById(android.R.id.statusBarBackground);
        return getPair(view);
    }

    private Pair<View, String> getNavigationBarPair(){
        View view = getActivity().findViewById(android.R.id.navigationBarBackground);
        return getPair(view);
    }

    private Pair<View, String> getAppBarPair(){
        Resources resources = getResources();
        String transitionName = resources.getString(R.string.transition_app_bar);
        View view = getActivity().findViewById(R.id.appbar);
        return getPair(view, transitionName);
    }

    private Pair<View, String> getPair(View view, String transitionName){
        Pair<View, String> pair = null;
        if(view != null) {
            pair = Pair.create(view, transitionName);
        }
        return pair;
    }

    private Pair<View, String> getPair(View view){
        Pair<View, String> pair = null;
        if(view != null) {
            pair = Pair.create(view, view.getTransitionName());
        }
        return pair;
    }

    public void scrollToTop(){
        recyclerView.scrollToPosition(0);
    }

    private TvShowsComponent createTelevisionShowsComponent(){
        tvShowsComponent = ((MovieGuide)getActivity().getApplication())
                .getApplicationComponent()
                .createSubcomponent(new TvShowsModule(this));
        return tvShowsComponent;
    }

    public void releaseTelevisionShowsComponent(){
        tvShowsComponent = null;
    }
    // endregion
}
