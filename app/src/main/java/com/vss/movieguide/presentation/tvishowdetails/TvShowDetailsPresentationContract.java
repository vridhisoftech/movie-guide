package com.vss.movieguide.presentation.tvishowdetails;

import com.vss.movieguide.domain.models.TvShowDetailsDomainModel;
import com.vss.movieguide.presentation.base.BasePresenter;
import com.vss.movieguide.presentation.models.ArtistPresentationModel;
import com.vss.movieguide.presentation.models.TvShowPresentationModel;

/**
  2/9/17.
 */

public interface TvShowDetailsPresentationContract {

    interface View {
        void setTelevisionShowDetailsDomainModel(TvShowDetailsDomainModel tvShowDetailsDomainModel);
        void showToolbarTitle();
        void hideToolbarTitle();
        void showErrorView();

        // Navigation methods
        void openPersonDetails(ArtistPresentationModel person);
        void openTelevisionShowDetails(TvShowPresentationModel televisionShow);
    }

    interface Presenter extends BasePresenter {
        void onLoadTelevisionShowDetails(int televisionShowId);
        void onPersonClick(ArtistPresentationModel person);
        void onTelevisionShowClick(TvShowPresentationModel televisionShow);
        void onScrollChange(boolean isScrolledPastThreshold);
    }
}
