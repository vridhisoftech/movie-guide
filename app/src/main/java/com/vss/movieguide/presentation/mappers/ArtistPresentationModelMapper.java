package com.vss.movieguide.presentation.mappers;

import com.vss.movieguide.domain.models.ArtistDomainModel;
import com.vss.movieguide.presentation.models.ArtistPresentationModel;

import java.util.ArrayList;
import java.util.List;

public class ArtistPresentationModelMapper implements PresentationModelMapper<ArtistDomainModel, ArtistPresentationModel>, PresentationModelListMapper<ArtistDomainModel, ArtistPresentationModel> {

    // region Member Variables
    private ProfileImagesPresentationModelMapper profileImagesPresentationModelMapper = new ProfileImagesPresentationModelMapper();
    // endregion

    @Override
    public ArtistPresentationModel mapToPresentationModel(ArtistDomainModel artistDomainModel) {
        ArtistPresentationModel artistPresentationModel = new ArtistPresentationModel();
        artistPresentationModel.setBiography(artistDomainModel.getBiography());
        artistPresentationModel.setBirthday(artistDomainModel.getBirthday());
        artistPresentationModel.setDeathday(artistDomainModel.getDeathday());
        artistPresentationModel.setId(artistDomainModel.getId());
        artistPresentationModel.setImages(profileImagesPresentationModelMapper.mapToPresentationModel(artistDomainModel.getImages()));
        artistPresentationModel.setImdbId(artistDomainModel.getImdbId());
        artistPresentationModel.setName(artistDomainModel.getName());
        artistPresentationModel.setPlaceOfBirth(artistDomainModel.getPlaceOfBirth());
        artistPresentationModel.setProfilePath(artistDomainModel.getProfilePath());

        return artistPresentationModel;
    }

    @Override
    public List<ArtistPresentationModel> mapListToPresentationModelList(List<ArtistDomainModel> artistDomainModels) {
        List<ArtistPresentationModel> artistPresentationModels = new ArrayList<>();
        if(artistDomainModels != null && artistDomainModels.size()>0) {
            for (ArtistDomainModel artistDomainModel : artistDomainModels) {
                artistPresentationModels.add(mapToPresentationModel(artistDomainModel));
            }
        }
        return artistPresentationModels;
    }
}
