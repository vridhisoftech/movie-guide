package com.vss.movieguide.presentation.mappers;

import com.vss.movieguide.domain.models.ArtistCreditDomainModel;
import com.vss.movieguide.presentation.models.ArtistCreditPresentationModel;

import java.util.ArrayList;
import java.util.List;

public class ArtistCreditPresentationModelMapper implements PresentationModelMapper<ArtistCreditDomainModel, ArtistCreditPresentationModel>, PresentationModelListMapper<ArtistCreditDomainModel, ArtistCreditPresentationModel> {

    @Override
    public ArtistCreditPresentationModel mapToPresentationModel(ArtistCreditDomainModel artistCreditDomainModel) {
        ArtistCreditPresentationModel artistCreditPresentationModel = new ArtistCreditPresentationModel();
        artistCreditPresentationModel.setCharacter(artistCreditDomainModel.getCharacter());
        artistCreditPresentationModel.setDepartment(artistCreditDomainModel.getDepartment());
        artistCreditPresentationModel.setFirstAirDate(artistCreditDomainModel.getFirstAirDate());
        artistCreditPresentationModel.setJob(artistCreditDomainModel.getJob());
        artistCreditPresentationModel.setMediaType(artistCreditDomainModel.getMediaType());
        artistCreditPresentationModel.setName(artistCreditDomainModel.getName());
        artistCreditPresentationModel.setPosterPath(artistCreditDomainModel.getPosterPath());
        artistCreditPresentationModel.setReleaseDate(artistCreditDomainModel.getReleaseDate());
        artistCreditPresentationModel.setTitle(artistCreditDomainModel.getTitle());
        artistCreditPresentationModel.setCreditId(artistCreditDomainModel.getCreditId());
        artistCreditPresentationModel.setId(artistCreditDomainModel.getId());
        return artistCreditPresentationModel;
    }

    @Override
    public List<ArtistCreditPresentationModel> mapListToPresentationModelList(List<ArtistCreditDomainModel> artistCreditDomainModels) {
        List<ArtistCreditPresentationModel> artistCreditPresentationModels = new ArrayList<>();
        if(artistCreditDomainModels != null && artistCreditDomainModels.size()>0) {
            for (ArtistCreditDomainModel artistCreditDomainModel : artistCreditDomainModels) {
                artistCreditPresentationModels.add(mapToPresentationModel(artistCreditDomainModel));
            }
        }
        return artistCreditPresentationModels;
    }
}
