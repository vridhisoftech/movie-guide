package com.vss.movieguide.presentation.mappers;

import com.vss.movieguide.domain.models.ProfileImageDomainModel;
import com.vss.movieguide.presentation.models.ProfileImagePresentationModel;

import java.util.ArrayList;
import java.util.List;

public class ProfileImagePresentationModelMapper implements PresentationModelMapper<ProfileImageDomainModel, ProfileImagePresentationModel>, PresentationModelListMapper<ProfileImageDomainModel, ProfileImagePresentationModel> {

    @Override
    public ProfileImagePresentationModel mapToPresentationModel(ProfileImageDomainModel profileImageDomainModel) {
        ProfileImagePresentationModel profileImagePresentationModel = new ProfileImagePresentationModel();
        profileImagePresentationModel.setAspectRatio(profileImageDomainModel.getAspectRatio());
        profileImagePresentationModel.setFilePath(profileImageDomainModel.getFilePath());
        profileImagePresentationModel.setHeight(profileImageDomainModel.getHeight());
        profileImagePresentationModel.setVoteAverage(profileImageDomainModel.getVoteAverage());
        profileImagePresentationModel.setVoteCount(profileImageDomainModel.getVoteCount());
        profileImagePresentationModel.setWidth(profileImageDomainModel.getWidth());
        return profileImagePresentationModel;
    }

    @Override
    public List<ProfileImagePresentationModel> mapListToPresentationModelList(List<ProfileImageDomainModel> profileImageDomainModels) {
        List<ProfileImagePresentationModel> profileImagePresentationModels = new ArrayList<>();
        if(profileImageDomainModels != null && profileImageDomainModels.size()>0) {
            for (ProfileImageDomainModel profileImageDomainModel : profileImageDomainModels) {
                profileImagePresentationModels.add(mapToPresentationModel(profileImageDomainModel));
            }
        }
        return profileImagePresentationModels;
    }
}
