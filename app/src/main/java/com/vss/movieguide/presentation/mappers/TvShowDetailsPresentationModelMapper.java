package com.vss.movieguide.presentation.mappers;

import com.vss.movieguide.domain.models.TvShowDetailsDomainModel;
import com.vss.movieguide.presentation.models.TvShowDetailsPresentationModel;

public class TvShowDetailsPresentationModelMapper implements PresentationModelMapper<TvShowDetailsDomainModel, TvShowDetailsPresentationModel> {

    // region Member Variables
    private TvCreditPresentationModelMapper tvCreditPresentationModelMapper = new TvCreditPresentationModelMapper();
    private TvShowPresentationModelMapper tvShowPresentationModelMapper = new TvShowPresentationModelMapper();
    // endregion

    @Override
    public TvShowDetailsPresentationModel mapToPresentationModel(TvShowDetailsDomainModel tvShowDetailsDomainModel) {
        TvShowDetailsPresentationModel tvShowDetailsPresentationModel = new TvShowDetailsPresentationModel();
        tvShowDetailsPresentationModel.setCast(tvCreditPresentationModelMapper.mapListToPresentationModelList(tvShowDetailsDomainModel.getCast()));
        tvShowDetailsPresentationModel.setCrew(tvCreditPresentationModelMapper.mapListToPresentationModelList(tvShowDetailsDomainModel.getCrew()));
        tvShowDetailsPresentationModel.setRating(tvShowDetailsDomainModel.getRating());
        tvShowDetailsPresentationModel.setSimilarTelevisionShows(tvShowPresentationModelMapper.mapListToPresentationModelList(tvShowDetailsDomainModel.getSimilarTelevisionShows()));
        tvShowDetailsPresentationModel.setTelevisionShow(tvShowPresentationModelMapper.mapToPresentationModel(tvShowDetailsDomainModel.getTelevisionShow()));
        return tvShowDetailsPresentationModel;
    }
}
