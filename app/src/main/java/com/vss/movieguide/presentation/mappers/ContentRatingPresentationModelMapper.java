package com.vss.movieguide.presentation.mappers;

import com.vss.movieguide.domain.models.ContentRatingDomainModel;
import com.vss.movieguide.presentation.models.ContentRatingPresentationModel;

import java.util.ArrayList;
import java.util.List;

public class ContentRatingPresentationModelMapper implements PresentationModelMapper<ContentRatingDomainModel, ContentRatingPresentationModel>, PresentationModelListMapper<ContentRatingDomainModel, ContentRatingPresentationModel> {

    @Override
    public ContentRatingPresentationModel mapToPresentationModel(ContentRatingDomainModel contentRatingDomainModel) {
        ContentRatingPresentationModel contentRatingPresentationModel = new ContentRatingPresentationModel();
        contentRatingPresentationModel.setIso31661(contentRatingDomainModel.getIso31661());
        contentRatingPresentationModel.setRating(contentRatingDomainModel.getRating());
        return contentRatingPresentationModel;
    }

    @Override
    public List<ContentRatingPresentationModel> mapListToPresentationModelList(List<ContentRatingDomainModel> contentRatingDomainModels) {
        List<ContentRatingPresentationModel> contentRatingPresentationModels = new ArrayList<>();
        if(contentRatingDomainModels != null && contentRatingDomainModels.size()>0) {
            for (ContentRatingDomainModel contentRatingDomainModel : contentRatingDomainModels) {
                contentRatingPresentationModels.add(mapToPresentationModel(contentRatingDomainModel));
            }
        }
        return contentRatingPresentationModels;
    }
}

