package com.vss.movieguide.presentation.mappers;

import com.vss.movieguide.domain.models.ProfileImagesDomainModel;
import com.vss.movieguide.presentation.models.ProfileImagesPresentationModel;

public class ProfileImagesPresentationModelMapper implements PresentationModelMapper<ProfileImagesDomainModel, ProfileImagesPresentationModel> {

    // region Member Variables
    private ProfileImagePresentationModelMapper profileImagePresentationModelMapper = new ProfileImagePresentationModelMapper();
    // endregion

    @Override
    public ProfileImagesPresentationModel mapToPresentationModel(ProfileImagesDomainModel profileImagesDomainModel) {
        ProfileImagesPresentationModel profileImagesPresentationModel = new ProfileImagesPresentationModel();
        if(profileImagesDomainModel != null){
            profileImagesPresentationModel.setProfiles(profileImagePresentationModelMapper.mapListToPresentationModelList(profileImagesDomainModel.getProfiles()));
        }
        return profileImagesPresentationModel;
    }
}
