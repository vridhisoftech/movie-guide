package com.vss.movieguide.presentation.mappers;

import com.vss.movieguide.domain.models.MovieDetailsDomainModel;
import com.vss.movieguide.presentation.models.MovieDetailsPresentationModel;

public class MovieDetailsPresentationModelMapper implements PresentationModelMapper<MovieDetailsDomainModel, MovieDetailsPresentationModel> {

    // region Member Variables
    private MovieCreditPresentationModelMapper movieCreditPresentationModelMapper = new MovieCreditPresentationModelMapper();
    private MoviePresentationModelMapper moviePresentationModelMapper = new MoviePresentationModelMapper();
    // endregion

    @Override
    public MovieDetailsPresentationModel mapToPresentationModel(MovieDetailsDomainModel movieDetailsDomainModel) {
        MovieDetailsPresentationModel movieDetailsPresentationModel = new MovieDetailsPresentationModel();
        movieDetailsPresentationModel.setCast(movieCreditPresentationModelMapper.mapListToPresentationModelList(movieDetailsDomainModel.getCast()));
        movieDetailsPresentationModel.setCrew(movieCreditPresentationModelMapper.mapListToPresentationModelList(movieDetailsDomainModel.getCrew()));
        movieDetailsPresentationModel.setMovie(moviePresentationModelMapper.mapToPresentationModel(movieDetailsDomainModel.getMovie()));
        movieDetailsPresentationModel.setRating(movieDetailsDomainModel.getRating());
        movieDetailsPresentationModel.setSimilarMovies(moviePresentationModelMapper.mapListToPresentationModelList(movieDetailsDomainModel.getSimilarMovies()));
        return movieDetailsPresentationModel;
    }
}
