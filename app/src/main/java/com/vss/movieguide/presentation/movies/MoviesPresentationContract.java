package com.vss.movieguide.presentation.movies;

import com.vss.movieguide.domain.models.MovieDomainModel;
import com.vss.movieguide.domain.models.MoviesDomainModel;
import com.vss.movieguide.presentation.base.BasePresenter;
import com.vss.movieguide.presentation.models.MoviePresentationModel;

import java.util.List;


public interface MoviesPresentationContract {

    interface View {
        void showEmptyView();
        void hideEmptyView();
        void showErrorView();
        void hideErrorView();
        void showLoadingView();
        void hideLoadingView();
        void addHeaderView();
        void addFooterView();
        void removeFooterView();
        void showErrorFooterView();
        void showLoadingFooterView();
        void showMovies(List<MovieDomainModel> movies);
        void loadMoreMovies();
        void setMoviesDomainModel(MoviesDomainModel moviesDomainModel);

        // Navigation methods
        void openMovieDetails(MoviePresentationModel movie);
    }

    interface Presenter extends BasePresenter {
        void onLoadPopularMovies(int currentPage);
        void onMovieClick(MoviePresentationModel movie);
        void onScrollToEndOfList();
    }
}
