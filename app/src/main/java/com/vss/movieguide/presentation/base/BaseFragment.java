package com.vss.movieguide.presentation.base;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.View;

import com.vss.movieguide.MovieGuide;
import com.squareup.leakcanary.RefWatcher;

public abstract class BaseFragment extends Fragment {

    // region Lifecycle Methods
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Timber.d("onCreate()");
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        Timber.d("onViewCreated()");
    }

    @Override
    public void onStop() {
        super.onStop();
//        Timber.d("onStop()");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        Timber.d("onDestroyView()");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        RefWatcher refWatcher = MovieGuide.getRefWatcher(getActivity());
        refWatcher.watch(this);
    }
    // region
}
