package com.vss.movieguide.presentation.mappers;

import com.vss.movieguide.domain.models.TvShowContentRatingsDomainModel;
import com.vss.movieguide.presentation.models.TvShowContentRatingsPresentationModel;

public class TvShowContentRatingsPresentationModelMapper implements PresentationModelMapper<TvShowContentRatingsDomainModel, TvShowContentRatingsPresentationModel> {

    // region Member Variables
    private ContentRatingPresentationModelMapper contentRatingPresentationModelMapper = new ContentRatingPresentationModelMapper();
    // endregion

    @Override
    public TvShowContentRatingsPresentationModel mapToPresentationModel(TvShowContentRatingsDomainModel tvShowContentRatingsDomainModel) {
        TvShowContentRatingsPresentationModel tvShowContentRatingsPresentationModel = new TvShowContentRatingsPresentationModel();
        tvShowContentRatingsPresentationModel.setContentRatings(contentRatingPresentationModelMapper.mapListToPresentationModelList(tvShowContentRatingsDomainModel.getContentRatings()));
        tvShowContentRatingsPresentationModel.setId(tvShowContentRatingsDomainModel.getId());
        return tvShowContentRatingsPresentationModel;
    }
}

