package com.vss.movieguide.presentation.artists;

import com.vss.movieguide.domain.models.ArtistDomainModel;
import com.vss.movieguide.domain.models.ArtistsDomainModel;
import com.vss.movieguide.presentation.base.BasePresenter;
import com.vss.movieguide.presentation.models.ArtistPresentationModel;

import java.util.List;

public interface ArtistsPresentationContract {

    interface View {
        void showEmptyView();
        void hideEmptyView();
        void showErrorView();
        void hideErrorView();
        void showLoadingView();
        void hideLoadingView();
        void addHeaderView();
        void addFooterView();
        void removeFooterView();
        void showErrorFooterView();
        void showLoadingFooterView();
        void showPersons(List<ArtistDomainModel> persons);
        void loadMorePersons();
        void setPersonsDomainModel(ArtistsDomainModel artistsDomainModel);

        // Navigation methods
        void openPersonDetails(ArtistPresentationModel person);
    }

    interface Presenter extends BasePresenter {
        void onLoadPopularPersons(int currentPage);
        void onPersonClick(ArtistPresentationModel person);
        void onScrollToEndOfList();
    }
}
