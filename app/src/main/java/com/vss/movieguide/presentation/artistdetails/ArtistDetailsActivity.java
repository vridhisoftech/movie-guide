package com.vss.movieguide.presentation.artistdetails;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import android.view.MenuItem;

import com.vss.movieguide.R;
import com.vss.movieguide.presentation.models.ArtistPresentationModel;

import butterknife.ButterKnife;

public class ArtistDetailsActivity extends AppCompatActivity {

    // region Constants
    protected static final String KEY_PERSON = "PERSON";
    // endregion

    // region Lifecycle Methods
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist_details);
        ButterKnife.bind(this);

        Fragment fragment = getSupportFragmentManager().findFragmentById(android.R.id.content);
        if (fragment == null) {
            fragment = ArtistDetailsFragment.newInstance(getIntent().getExtras());
            getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .replace(android.R.id.content, fragment, fragment.getClass().getSimpleName())
                    .commit();
        } else {
            getSupportFragmentManager()
                    .beginTransaction()
                    .attach(fragment)
                    .commit();
        }
    }
    // endregion

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                supportFinishAfterTransition();
//                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // Use factory methods for creating Intents
    public static Intent createIntent(Context context, ArtistPresentationModel person){
        Intent intent = new Intent(context, ArtistDetailsActivity.class);
        intent.putExtra(KEY_PERSON, person);
        return intent;
    }
}
