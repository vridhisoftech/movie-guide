package com.vss.movieguide.presentation.mappers;

import com.vss.movieguide.domain.models.ArtistCreditsDomainModel;
import com.vss.movieguide.presentation.models.ArtistCreditsPresentationModel;

public class ArtistCreditsPresentationModelMapper implements PresentationModelMapper<ArtistCreditsDomainModel, ArtistCreditsPresentationModel> {

    // region Member Variables
    private ArtistCreditPresentationModelMapper artistCreditPresentationModelMapper = new ArtistCreditPresentationModelMapper();
    // endregion

    @Override
    public ArtistCreditsPresentationModel mapToPresentationModel(ArtistCreditsDomainModel artistCreditsDomainModel) {
        ArtistCreditsPresentationModel personCreditsPresentationModel = new ArtistCreditsPresentationModel();
        personCreditsPresentationModel.setCast(artistCreditPresentationModelMapper.mapListToPresentationModelList(artistCreditsDomainModel.getCast()));
        personCreditsPresentationModel.setCrew(artistCreditPresentationModelMapper.mapListToPresentationModelList(artistCreditsDomainModel.getCrew()));
        personCreditsPresentationModel.setId(artistCreditsDomainModel.getId());
        return personCreditsPresentationModel;
    }
}
