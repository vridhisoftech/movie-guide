package com.vss.movieguide.presentation.main;

public interface MainContract {

    interface View {
        void viewSearch();
    }

    interface Presenter {
        void viewSearch();
    }
}
