package com.vss.movieguide.presentation.tvishowdetails;

import com.vss.movieguide.domain.models.TvShowDetailsDomainModel;
import com.vss.movieguide.domain.usecases.TvShowDetailsDomainContract;
import com.vss.movieguide.presentation.models.ArtistPresentationModel;
import com.vss.movieguide.presentation.models.TvShowPresentationModel;
import com.vss.movieguide.util.rxjava.SchedulerProvider;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;

/**
  2/9/17.
 */

public class TvShowDetailsPresenter implements TvShowDetailsPresentationContract.Presenter {

    // region Member Variables
    private final TvShowDetailsPresentationContract.View televisionShowDetailsView;
    private final TvShowDetailsDomainContract.UseCase televisionShowDetailsUseCase;
    private final SchedulerProvider schedulerProvider;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    // endregion

    // region Constructors

    public TvShowDetailsPresenter(TvShowDetailsPresentationContract.View televisionShowDetailsView, TvShowDetailsDomainContract.UseCase televisionShowDetailsUseCase, SchedulerProvider schedulerProvider) {
        this.televisionShowDetailsView = televisionShowDetailsView;
        this.televisionShowDetailsUseCase = televisionShowDetailsUseCase;
        this.schedulerProvider = schedulerProvider;
    }

    // endregion

    // region TelevisionShowDetailsPresentationContract.Presenter Methods

    @Override
    public void onDestroyView() {
        if (compositeDisposable != null)
            compositeDisposable.clear();
    }

    @Override
    public void onLoadTelevisionShowDetails(int televisionShowId) {
        Disposable disposable = televisionShowDetailsUseCase.getTelevisionShowDetails(televisionShowId)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribeWith(new DisposableSingleObserver<TvShowDetailsDomainModel>() {
                    @Override
                    public void onSuccess(TvShowDetailsDomainModel televisionShowDetailsDomainModel) {
                        if(televisionShowDetailsDomainModel != null){
                            televisionShowDetailsView.setTelevisionShowDetailsDomainModel(televisionShowDetailsDomainModel);
                        }
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        throwable.printStackTrace();

                        televisionShowDetailsView.showErrorView();
                    }
                });

        compositeDisposable.add(disposable);
    }

    @Override
    public void onPersonClick(ArtistPresentationModel person) {
        televisionShowDetailsView.openPersonDetails(person);
    }

    @Override
    public void onTelevisionShowClick(TvShowPresentationModel televisionShow) {
        televisionShowDetailsView.openTelevisionShowDetails(televisionShow);
    }

    @Override
    public void onScrollChange(boolean isScrolledPastThreshold) {
        if(isScrolledPastThreshold)
            televisionShowDetailsView.showToolbarTitle();
        else
            televisionShowDetailsView.hideToolbarTitle();
    }

    // endregion
}
