package com.vss.movieguide.presentation.mappers;

import com.vss.movieguide.domain.models.TvShowCreditDomainModel;
import com.vss.movieguide.presentation.models.TvShowCreditPresentationModel;

import java.util.ArrayList;
import java.util.List;

public class TvCreditPresentationModelMapper implements PresentationModelMapper<TvShowCreditDomainModel, TvShowCreditPresentationModel>, PresentationModelListMapper<TvShowCreditDomainModel, TvShowCreditPresentationModel> {

    @Override
    public TvShowCreditPresentationModel mapToPresentationModel(TvShowCreditDomainModel tvShowCreditDomainModel) {
        TvShowCreditPresentationModel tvShowCreditPresentationModel = new TvShowCreditPresentationModel();
        tvShowCreditPresentationModel.setCharacter(tvShowCreditDomainModel.getCharacter());
        tvShowCreditPresentationModel.setDepartment(tvShowCreditDomainModel.getDepartment());
        tvShowCreditPresentationModel.setJob(tvShowCreditDomainModel.getJob());
        tvShowCreditPresentationModel.setName(tvShowCreditDomainModel.getName());
        tvShowCreditPresentationModel.setProfilePath(tvShowCreditDomainModel.getProfilePath());
        tvShowCreditPresentationModel.setCreditId(tvShowCreditDomainModel.getCreditId());
        tvShowCreditPresentationModel.setId(tvShowCreditDomainModel.getId());
        return tvShowCreditPresentationModel;
    }

    @Override
    public List<TvShowCreditPresentationModel> mapListToPresentationModelList(List<TvShowCreditDomainModel> tvShowCreditDomainModels) {
        List<TvShowCreditPresentationModel> tvShowCreditPresentationModels = new ArrayList<>();
        if(tvShowCreditDomainModels != null && tvShowCreditDomainModels.size()>0) {
            for (TvShowCreditDomainModel tvShowCreditDomainModel : tvShowCreditDomainModels) {
                tvShowCreditPresentationModels.add(mapToPresentationModel(tvShowCreditDomainModel));
            }
        }
        return tvShowCreditPresentationModels;
    }
}
