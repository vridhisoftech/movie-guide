package com.vss.movieguide.presentation.main;

import android.content.res.Resources;
import android.os.Bundle;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.fragment.app.Fragment;
import androidx.core.util.Pair;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.view.View;
import android.view.Window;

import com.vss.movieguide.R;
import com.vss.movieguide.presentation.artists.ArtistsFragment;
import com.vss.movieguide.presentation.artists.SettingsFragment;
import com.vss.movieguide.presentation.movies.MoviesFragment;
import com.vss.movieguide.presentation.search.SearchActivity;
import com.vss.movieguide.presentation.tvshows.TvShowsFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeActivity extends AppCompatActivity implements MainContract.View {

    // region Views
    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigationView;
    @BindView(R.id.search_cv)
    CardView searchCardView;
    // endregion

    private MainContract.Presenter mainPresenter;
    // endregion

    // region Listeners
    @OnClick(R.id.search_cv)
    public void onSearchCardViewClicked(View view) {
        mainPresenter.viewSearch();
    }
    // endregion

    // region Lifecycle Methods
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.Theme_MovieGuide_MainActivity);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        mainPresenter = new MainPresenter(this);

        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content_fl);
        if (fragment == null) {
            fragment = MoviesFragment.newInstance(getIntent().getExtras());
            getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .replace(R.id.content_fl, fragment, fragment.getClass().getSimpleName())
                    .commit();
        } else {
            getSupportFragmentManager()
                    .beginTransaction()
                    .attach(fragment)
                    .commit();
        }

        bottomNavigationView.setOnNavigationItemSelectedListener(
                item -> {
                    if(!item.isChecked()){
                        item.setChecked(true);
                        Fragment fragment1;
                        switch (item.getItemId()) {
                            case R.id.action_movies:
                                fragment1 = MoviesFragment.newInstance();
                                break;
                            case R.id.action_tv_shows:
                                fragment1 = TvShowsFragment.newInstance();
                                break;
                            case R.id.action_artist:
                                fragment1 = ArtistsFragment.newInstance();
                                break;
                                case R.id.action_settings:
                                fragment1 = SettingsFragment.newInstance();
                                break;
                            default:
                                fragment1 = MoviesFragment.newInstance();
                                break;
                        }

                        getSupportFragmentManager()
                                .beginTransaction()
                                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                                .replace(R.id.content_fl, fragment1, fragment1.getClass().getSimpleName())
                                .commit();
                    } else {
                        Fragment fragment1 = getSupportFragmentManager().findFragmentById(R.id.content_fl);
                        if(fragment1 instanceof MoviesFragment){
                            ((MoviesFragment) fragment1).scrollToTop();
                        } else if(fragment1 instanceof TvShowsFragment){
                            ((TvShowsFragment) fragment1).scrollToTop();
                        } else if(fragment1 instanceof ArtistsFragment){
                            ((ArtistsFragment) fragment1).scrollToTop();
                        }
                    }
                    return false;
                });
    }
    // endregion

    // region MainContract.View Methods

    @Override
    public void viewSearch() {
        Window window = getWindow();
//        window.setStatusBarColor(primaryDark);

        Resources resources = searchCardView.getResources();
        Pair<View, String> searchPair  = getSearchViewPair(searchCardView, resources.getString(R.string.transition_search));

        ActivityOptionsCompat options = getActivityOptionsCompat(searchPair);

        window.setExitTransition(null);
        ActivityCompat.startActivity(this, SearchActivity.createIntent(this), options.toBundle());
    }

    // endregion

    private ActivityOptionsCompat getActivityOptionsCompat(Pair pair){
        ActivityOptionsCompat options = null;

        Pair<View, String> navigationBarPair  = getNavigationBarPair();
        Pair<View, String> statusBarPair = getStatusBarPair();

        if(pair!=null && statusBarPair!= null && navigationBarPair!= null){
            options = ActivityOptionsCompat.makeSceneTransitionAnimation(this,
                    pair, statusBarPair, navigationBarPair);
        } else if(pair != null && statusBarPair != null){
            options = ActivityOptionsCompat.makeSceneTransitionAnimation(this,
                    pair, statusBarPair);
        } else if(pair != null && navigationBarPair != null){
            options = ActivityOptionsCompat.makeSceneTransitionAnimation(this,
                    pair, navigationBarPair);
        }

        return options;
    }

    private Pair<View, String> getStatusBarPair(){
        Pair<View, String> pair = null;
        View statusBar = findViewById(android.R.id.statusBarBackground);
        if(statusBar != null)
            pair = Pair.create(statusBar, statusBar.getTransitionName());
        return pair;
    }

    private Pair<View, String> getNavigationBarPair(){
        Pair<View, String> pair = null;
        View navigationBar = findViewById(android.R.id.navigationBarBackground);
        if(navigationBar != null)
            pair = Pair.create(navigationBar, navigationBar.getTransitionName());
        return pair;
    }

    private Pair<View, String> getSearchViewPair(View view, String transition){
        Pair<View, String> searchPair = null;
        View searchView = view.findViewById(R.id.search_cv);
        if(searchView != null){
            searchPair = Pair.create(searchView, transition);
        }

        return searchPair;
    }
    // endregion
}
