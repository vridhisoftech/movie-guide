package com.vss.movieguide.presentation.mappers;

import com.vss.movieguide.domain.models.TvShowsDomainModel;
import com.vss.movieguide.presentation.models.TvShowsPresentationModel;

import java.util.Calendar;

public class TvShowsPresentationModelMapper implements PresentationModelMapper<TvShowsDomainModel, TvShowsPresentationModel> {

    // region Constants
    private static final int PAGE_SIZE = 20;
    private static final int SEVEN_DAYS = 7;
    // endregion

    // region Member Variables
    private TvShowPresentationModelMapper tvShowPresentationModelMapper = new TvShowPresentationModelMapper();
    // endregion

    @Override
    public TvShowsPresentationModel mapToPresentationModel(TvShowsDomainModel tvShowsDomainModel) {
        TvShowsPresentationModel tvShowsPresentationModel = new TvShowsPresentationModel();
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, SEVEN_DAYS);
        tvShowsPresentationModel.setExpiredAt(calendar.getTime());
        tvShowsPresentationModel.setLastPage(tvShowsDomainModel.getTelevisionShows().size() < PAGE_SIZE);
        tvShowsPresentationModel.setTelevisionShows(tvShowPresentationModelMapper.mapListToPresentationModelList(tvShowsDomainModel.getTelevisionShows()));
        tvShowsPresentationModel.setPageNumber(tvShowsDomainModel.getPageNumber());
        return tvShowsPresentationModel;
    }
}
