package com.vss.movieguide.presentation.models;

import java.util.List;

public class TvShowCreditsPresentationModel {

    // region Fields
    public int id;
    public List<TvShowCreditPresentationModel> cast = null;
    public List<TvShowCreditPresentationModel> crew = null;
    // endregion

    // region Getters

    public int getId() {
        return id;
    }

    public List<TvShowCreditPresentationModel> getCast() {
        return cast;
    }

    public List<TvShowCreditPresentationModel> getCrew() {
        return crew;
    }

    // endregion

    // region Setters

    public void setId(int id) {
        this.id = id;
    }

    public void setCast(List<TvShowCreditPresentationModel> cast) {
        this.cast = cast;
    }

    public void setCrew(List<TvShowCreditPresentationModel> crew) {
        this.crew = crew;
    }

    // endregion

    @Override
    public String toString() {
        return "TelevisionShowCreditsPresentationModel{" +
                "id=" + id +
                ", cast=" + cast +
                ", crew=" + crew +
                '}';
    }
}
