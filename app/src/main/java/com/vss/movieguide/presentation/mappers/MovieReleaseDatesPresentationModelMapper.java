package com.vss.movieguide.presentation.mappers;

import com.vss.movieguide.domain.models.MovieReleaseDatesDomainModel;
import com.vss.movieguide.presentation.models.MovieReleaseDatesPresentationModel;

import java.util.Calendar;

public class MovieReleaseDatesPresentationModelMapper implements PresentationModelMapper<MovieReleaseDatesDomainModel, MovieReleaseDatesPresentationModel> {

    // region Constants
    private static final int THIRTY_DAYS = 30;
    // endregion

    // region Member Variables
    private MovieReleaseDatePresentationModelMapper movieReleaseDatePresentationModelMapper = new MovieReleaseDatePresentationModelMapper();
    // endregion

    @Override
    public MovieReleaseDatesPresentationModel mapToPresentationModel(MovieReleaseDatesDomainModel movieReleaseDatesDomainModel) {
        MovieReleaseDatesPresentationModel movieReleaseDatesPresentationModel = new MovieReleaseDatesPresentationModel();
        movieReleaseDatesPresentationModel.setId(movieReleaseDatesDomainModel.getId());
        movieReleaseDatesPresentationModel.setMovieReleaseDates(movieReleaseDatePresentationModelMapper.mapListToPresentationModelList(movieReleaseDatesDomainModel.getMovieReleaseDates()));
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, THIRTY_DAYS);
        movieReleaseDatesPresentationModel.setExpiredAt(calendar.getTime());
        return movieReleaseDatesPresentationModel;
    }
}
