package com.vss.movieguide.presentation.mappers;

public interface PresentationModelMapper<DomainModel, PresentationModel> {
    PresentationModel mapToPresentationModel(DomainModel domainModel);
}