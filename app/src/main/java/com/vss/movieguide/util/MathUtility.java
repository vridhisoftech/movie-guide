package com.vss.movieguide.util;

public class MathUtility {

    private MathUtility() { }

    public static float constrain(float min, float max, float v) {
        return Math.max(min, Math.min(max, v));
    }
}
