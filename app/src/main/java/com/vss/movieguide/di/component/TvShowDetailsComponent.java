package com.vss.movieguide.di.component;

import com.vss.movieguide.di.module.TvShowDetailsModule;
import com.vss.movieguide.di.scope.PresentationScope;
import com.vss.movieguide.presentation.tvishowdetails.TvShowDetailsFragment;

import dagger.Subcomponent;

@PresentationScope
@Subcomponent(modules = {TvShowDetailsModule.class})
public interface TvShowDetailsComponent {
    // Setup injection targets
    void inject(TvShowDetailsFragment target);
}
