package com.vss.movieguide.di.component;

import com.vss.movieguide.di.module.MovieDetailsModule;
import com.vss.movieguide.di.scope.PresentationScope;
import com.vss.movieguide.presentation.moviedetails.MovieDetailsFragment;

import dagger.Subcomponent;

@PresentationScope
@Subcomponent(modules = {MovieDetailsModule.class})
public interface MovieDetailsComponent {
    // Setup injection targets
    void inject(MovieDetailsFragment target);
}
