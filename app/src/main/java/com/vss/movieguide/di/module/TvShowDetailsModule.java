package com.vss.movieguide.di.module;

import com.vss.movieguide.data.network.MovieGuideService;
import com.vss.movieguide.data.repositories.tv.TvShowDataSourceContract;
import com.vss.movieguide.data.repositories.tv.TvShowLocalDataSource;
import com.vss.movieguide.data.repositories.tv.TvShowRemoteDataSource;
import com.vss.movieguide.data.repositories.tv.TvShowRepository;
import com.vss.movieguide.domain.usecases.TvShowDetailsDomainContract;
import com.vss.movieguide.domain.usecases.TvShowDetailsUseCase;
import com.vss.movieguide.presentation.tvishowdetails.TvShowDetailsPresentationContract;
import com.vss.movieguide.presentation.tvishowdetails.TvShowDetailsPresenter;
import com.vss.movieguide.util.rxjava.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class TvShowDetailsModule {

    private TvShowDetailsPresentationContract.View televisionShowDetailsView;

    public TvShowDetailsModule(TvShowDetailsPresentationContract.View televisionShowDetailsView) {
        this.televisionShowDetailsView = televisionShowDetailsView;
    }

    @Provides
    public TvShowDataSourceContract.LocalDateSource provideTelevisionShowLocalDataSource() {
        return new TvShowLocalDataSource();
    }

    @Provides
    public TvShowDataSourceContract.RemoteDateSource provideTelevisionShowRemoteDataSource(MovieGuideService movieGuideService) {
        return new TvShowRemoteDataSource(movieGuideService);
    }

    @Provides
    public TvShowDataSourceContract.Repository provideTelevisionShowRepository(TvShowDataSourceContract.LocalDateSource televisionShowLocalDataSource, TvShowDataSourceContract.RemoteDateSource televisionShowRemoteDataSource) {
        return new TvShowRepository(televisionShowLocalDataSource, televisionShowRemoteDataSource);
    }

    @Provides
    public TvShowDetailsDomainContract.UseCase provideTelevisionShowDetailsUseCase(TvShowDataSourceContract.Repository televisionShowRepository) {
        return new TvShowDetailsUseCase(televisionShowRepository);
    }

    @Provides
    public TvShowDetailsPresentationContract.Presenter provideTelevisionShowDetailsPresenter(TvShowDetailsDomainContract.UseCase televisionShowDetailsUseCase, SchedulerProvider schedulerProvider) {
        return new TvShowDetailsPresenter(televisionShowDetailsView, televisionShowDetailsUseCase, schedulerProvider);
    }
}
