package com.vss.movieguide.di.component;

import com.vss.movieguide.di.module.SearchModule;
import com.vss.movieguide.di.scope.PresentationScope;
import com.vss.movieguide.presentation.search.SearchFragment;

import dagger.Subcomponent;

@PresentationScope
@Subcomponent(modules = {SearchModule.class})
public interface SearchComponent {
    // Setup injection targets
    void inject(SearchFragment target);
}
