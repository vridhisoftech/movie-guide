package com.vss.movieguide.di.component;

import com.vss.movieguide.MovieGuide;
import com.vss.movieguide.di.module.AndroidModule;
import com.vss.movieguide.di.module.ApplicationModule;
import com.vss.movieguide.di.module.ArtistDetailsModule;
import com.vss.movieguide.di.module.ArtistsModule;
import com.vss.movieguide.di.module.MovieDetailsModule;
import com.vss.movieguide.di.module.MoviesModule;
import com.vss.movieguide.di.module.NetworkModule;
import com.vss.movieguide.di.module.RxModule;
import com.vss.movieguide.di.module.SearchModule;
import com.vss.movieguide.di.module.TvShowDetailsModule;
import com.vss.movieguide.di.module.TvShowsModule;
import com.vss.movieguide.di.scope.ApplicationScope;

import dagger.Component;

@ApplicationScope
@Component(modules = {ApplicationModule.class, AndroidModule.class, NetworkModule.class, RxModule.class} )
public interface ApplicationComponent {
    // Setup injection targets
    void inject(MovieGuide target);

    MoviesComponent createSubcomponent(MoviesModule moviesModule);
    MovieDetailsComponent createSubcomponent(MovieDetailsModule movieDetailsModule);
    TvShowsComponent createSubcomponent(TvShowsModule tvShowsModule);
    TvShowDetailsComponent createSubcomponent(TvShowDetailsModule tvShowDetailsModule);
    ArtistsComponent createSubcomponent(ArtistsModule artistsModule);
    ArtistDetailsComponent createSubcomponent(ArtistDetailsModule artistDetailsModule);
    SearchComponent createSubcomponent(SearchModule searchModule);
}
