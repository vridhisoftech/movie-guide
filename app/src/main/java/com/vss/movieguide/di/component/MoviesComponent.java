package com.vss.movieguide.di.component;

import com.vss.movieguide.di.module.MoviesModule;
import com.vss.movieguide.di.scope.PresentationScope;
import com.vss.movieguide.presentation.movies.MoviesFragment;

import dagger.Subcomponent;

@PresentationScope
@Subcomponent(modules = {MoviesModule.class})
public interface MoviesComponent {
    // Setup injection targets
    void inject(MoviesFragment target);
}
