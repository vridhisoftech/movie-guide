package com.vss.movieguide.di.module;

import com.vss.movieguide.data.network.MovieGuideService;
import com.vss.movieguide.data.repositories.tv.TvShowDataSourceContract;
import com.vss.movieguide.data.repositories.tv.TvShowLocalDataSource;
import com.vss.movieguide.data.repositories.tv.TvShowRemoteDataSource;
import com.vss.movieguide.data.repositories.tv.TvShowRepository;
import com.vss.movieguide.domain.usecases.TvShowsDomainContract;
import com.vss.movieguide.domain.usecases.TvShowsUseCase;
import com.vss.movieguide.presentation.tvshows.TvShowsPresentationContract;
import com.vss.movieguide.presentation.tvshows.TvShowsPresenter;
import com.vss.movieguide.util.rxjava.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class TvShowsModule {

    private TvShowsPresentationContract.View televisionShowsView;

    public TvShowsModule(TvShowsPresentationContract.View televisionShowsView) {
        this.televisionShowsView = televisionShowsView;
    }

    @Provides
    public TvShowDataSourceContract.LocalDateSource provideTelevisionShowLocalDataSource() {
        return new TvShowLocalDataSource();
    }

    @Provides
    public TvShowDataSourceContract.RemoteDateSource provideTelevisionShowRemoteDataSource(MovieGuideService movieGuideService) {
        return new TvShowRemoteDataSource(movieGuideService);
    }

    @Provides
    public TvShowDataSourceContract.Repository provideTelevisionShowRepository(TvShowDataSourceContract.LocalDateSource televisionShowLocalDataSource, TvShowDataSourceContract.RemoteDateSource televisionShowRemoteDataSource) {
        return new TvShowRepository(televisionShowLocalDataSource, televisionShowRemoteDataSource);
    }

    @Provides
    public TvShowsDomainContract.UseCase provideTelevisionShowsUseCase(TvShowDataSourceContract.Repository televisionShowRepository) {
        return new TvShowsUseCase(televisionShowRepository);
    }

    @Provides
    public TvShowsPresentationContract.Presenter provideTelevisionShowsPresenter(TvShowsDomainContract.UseCase televisionShowsUseCase, SchedulerProvider schedulerProvider) {
        return new TvShowsPresenter(televisionShowsView, televisionShowsUseCase, schedulerProvider);
    }
}
