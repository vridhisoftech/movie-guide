package com.vss.movieguide.di.component;

import com.vss.movieguide.di.module.ArtistsModule;
import com.vss.movieguide.di.scope.PresentationScope;
import com.vss.movieguide.presentation.artists.ArtistsFragment;

import dagger.Subcomponent;

@PresentationScope
@Subcomponent(modules = {ArtistsModule.class})
public interface ArtistsComponent {
    // Setup injection targets
    void inject(ArtistsFragment target);
}
