package com.vss.movieguide.di.component;

import com.vss.movieguide.di.module.TvShowsModule;
import com.vss.movieguide.di.scope.PresentationScope;
import com.vss.movieguide.presentation.tvshows.TvShowsFragment;

import dagger.Subcomponent;

@PresentationScope
@Subcomponent(modules = {TvShowsModule.class})
public interface TvShowsComponent {
    // Setup injection targets
    void inject(TvShowsFragment target);
}
