package com.vss.movieguide.di.module;

import com.vss.movieguide.data.network.MovieGuideService;
import com.vss.movieguide.data.repositories.movie.MovieDataSourceContract;
import com.vss.movieguide.data.repositories.movie.MovieLocalDataSource;
import com.vss.movieguide.data.repositories.movie.MovieRemoteDataSource;
import com.vss.movieguide.data.repositories.movie.MovieRepository;
import com.vss.movieguide.domain.usecases.MoviesDomainContract;
import com.vss.movieguide.domain.usecases.MoviesUseCase;
import com.vss.movieguide.presentation.movies.MoviesPresentationContract;
import com.vss.movieguide.presentation.movies.MoviesPresenter;
import com.vss.movieguide.util.rxjava.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class MoviesModule {

    private MoviesPresentationContract.View moviesView;

    public MoviesModule(MoviesPresentationContract.View moviesView) {
        this.moviesView = moviesView;
    }

    @Provides
    public MovieDataSourceContract.LocalDateSource provideMovieLocalDataSource() {
        return new MovieLocalDataSource();
    }

    @Provides
    public MovieDataSourceContract.RemoteDateSource provideMovieRemoteDataSource(MovieGuideService movieGuideService) {
        return new MovieRemoteDataSource(movieGuideService);
    }

    @Provides
    public MovieDataSourceContract.Repository provideMovieRepository(MovieDataSourceContract.LocalDateSource movieLocalDataSource, MovieDataSourceContract.RemoteDateSource movieRemoteDataSource) {
        return new MovieRepository(movieLocalDataSource, movieRemoteDataSource);
    }

    @Provides
    public MoviesDomainContract.UseCase provideMoviesUseCase(MovieDataSourceContract.Repository movieRepository) {
        return new MoviesUseCase(movieRepository);
    }

    @Provides
    public MoviesPresentationContract.Presenter provideMoviesPresenter(MoviesDomainContract.UseCase moviesUseCase, SchedulerProvider schedulerProvider) {
        return new MoviesPresenter(moviesView, moviesUseCase, schedulerProvider);
    }
}
