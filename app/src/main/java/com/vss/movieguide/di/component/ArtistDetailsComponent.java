package com.vss.movieguide.di.component;

import com.vss.movieguide.di.module.ArtistDetailsModule;
import com.vss.movieguide.di.scope.PresentationScope;
import com.vss.movieguide.presentation.artistdetails.ArtistDetailsFragment;

import dagger.Subcomponent;

@PresentationScope
@Subcomponent(modules = {ArtistDetailsModule.class})
public interface ArtistDetailsComponent {
    // Setup injection targets
    void inject(ArtistDetailsFragment target);
}
