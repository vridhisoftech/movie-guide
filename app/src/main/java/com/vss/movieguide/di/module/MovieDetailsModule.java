package com.vss.movieguide.di.module;

import com.vss.movieguide.data.network.MovieGuideService;
import com.vss.movieguide.data.repositories.movie.MovieDataSourceContract;
import com.vss.movieguide.data.repositories.movie.MovieLocalDataSource;
import com.vss.movieguide.data.repositories.movie.MovieRemoteDataSource;
import com.vss.movieguide.data.repositories.movie.MovieRepository;
import com.vss.movieguide.domain.usecases.MovieDetailsDomainContract;
import com.vss.movieguide.domain.usecases.MovieDetailsUseCase;
import com.vss.movieguide.presentation.moviedetails.MovieDetailsPresentationContract;
import com.vss.movieguide.presentation.moviedetails.MovieDetailsPresenter;
import com.vss.movieguide.util.rxjava.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class MovieDetailsModule {

    private MovieDetailsPresentationContract.View movieDetailsView;

    public MovieDetailsModule(MovieDetailsPresentationContract.View movieDetailsView) {
        this.movieDetailsView = movieDetailsView;
    }

    @Provides
    public MovieDataSourceContract.LocalDateSource provideMovieLocalDataSource() {
        return new MovieLocalDataSource();
    }

    @Provides
    public MovieDataSourceContract.RemoteDateSource provideMovieRemoteDataSource(MovieGuideService movieGuideService) {
        return new MovieRemoteDataSource(movieGuideService);
    }

    @Provides
    public MovieDataSourceContract.Repository provideMovieRepository(MovieDataSourceContract.LocalDateSource movieLocalDataSource, MovieDataSourceContract.RemoteDateSource movieRemoteDataSource) {
        return new MovieRepository(movieLocalDataSource, movieRemoteDataSource);
    }

    @Provides
    public MovieDetailsDomainContract.UseCase provideMovieDetailsUseCase(MovieDataSourceContract.Repository movieRepository) {
        return new MovieDetailsUseCase(movieRepository);
    }

    @Provides
    public MovieDetailsPresentationContract.Presenter provideMovieDetailsPresenter(MovieDetailsDomainContract.UseCase movieDetailsUseCase, SchedulerProvider schedulerProvider) {
        return new MovieDetailsPresenter(movieDetailsView, movieDetailsUseCase, schedulerProvider);
    }
}
