package com.vss.movieguide.di.module;

import com.vss.movieguide.data.network.MovieGuideService;
import com.vss.movieguide.data.repositories.artist.ArtistDataSourceContract;
import com.vss.movieguide.data.repositories.artist.ArtistLocalDataSource;
import com.vss.movieguide.data.repositories.artist.ArtistRemoteDataSource;
import com.vss.movieguide.data.repositories.artist.ArtistRepository;
import com.vss.movieguide.domain.usecases.ArtistsDomainContract;
import com.vss.movieguide.domain.usecases.ArtistsUseCase;
import com.vss.movieguide.presentation.artists.ArtistsPresentationContract;
import com.vss.movieguide.presentation.artists.ArtistsPresenter;
import com.vss.movieguide.util.rxjava.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class ArtistsModule {

    private ArtistsPresentationContract.View personsView;

    public ArtistsModule(ArtistsPresentationContract.View personsView) {
        this.personsView = personsView;
    }

    @Provides
    public ArtistDataSourceContract.LocalDateSource providePersonLocalDataSource() {
        return new ArtistLocalDataSource();
    }

    @Provides
    public ArtistDataSourceContract.RemoteDateSource providePersonRemoteDataSource(MovieGuideService movieGuideService) {
        return new ArtistRemoteDataSource(movieGuideService);
    }

    @Provides
    public ArtistDataSourceContract.Repository providePersonRepository(ArtistDataSourceContract.LocalDateSource personLocalDataSource, ArtistDataSourceContract.RemoteDateSource personRemoteDataSource) {
        return new ArtistRepository(personLocalDataSource, personRemoteDataSource);
    }

    @Provides
    public ArtistsDomainContract.UseCase providePersonsUseCase(ArtistDataSourceContract.Repository personRepository) {
        return new ArtistsUseCase(personRepository);
    }

    @Provides
    public ArtistsPresentationContract.Presenter providePersonsPresenter(ArtistsDomainContract.UseCase personsUseCase, SchedulerProvider schedulerProvider) {
        return new ArtistsPresenter(personsView, personsUseCase, schedulerProvider);
    }
}
