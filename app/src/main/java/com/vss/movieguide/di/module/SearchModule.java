package com.vss.movieguide.di.module;

import com.vss.movieguide.data.network.MovieGuideService;
import com.vss.movieguide.data.repositories.search.SearchDataSourceContract;
import com.vss.movieguide.data.repositories.search.SearchLocalDataSource;
import com.vss.movieguide.data.repositories.search.SearchRemoteDataSource;
import com.vss.movieguide.data.repositories.search.SearchRepository;
import com.vss.movieguide.domain.usecases.SearchDomainContract;
import com.vss.movieguide.domain.usecases.SearchUseCase;
import com.vss.movieguide.presentation.search.SearchPresentationContract;
import com.vss.movieguide.presentation.search.SearchPresenter;
import com.vss.movieguide.util.rxjava.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class SearchModule {

    private SearchPresentationContract.View searchView;

    public SearchModule(SearchPresentationContract.View searchView) {
        this.searchView = searchView;
    }

    @Provides
    public SearchDataSourceContract.LocalDateSource provideSearchLocalDataSource() {
        return new SearchLocalDataSource();
    }

    @Provides
    public SearchDataSourceContract.RemoteDateSource provideSearchRemoteDataSource(MovieGuideService movieGuideService) {
        return new SearchRemoteDataSource(movieGuideService);
    }

    @Provides
    public SearchDataSourceContract.Repository provideSearchRepository(SearchDataSourceContract.LocalDateSource searchLocalDataSource, SearchDataSourceContract.RemoteDateSource searchRemoteDataSource) {
        return new SearchRepository(searchLocalDataSource, searchRemoteDataSource);
    }

    @Provides
    public SearchDomainContract.UseCase provideSearchUseCase(SearchDataSourceContract.Repository searchRepository) {
        return new SearchUseCase(searchRepository);
    }

    @Provides
    public SearchPresentationContract.Presenter provideSearchPresenter(SearchDomainContract.UseCase searchUseCase, SchedulerProvider schedulerProvider) {
        return new SearchPresenter(searchView, searchUseCase, schedulerProvider);
    }
}
