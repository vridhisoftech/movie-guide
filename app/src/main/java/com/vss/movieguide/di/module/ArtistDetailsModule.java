package com.vss.movieguide.di.module;

import com.vss.movieguide.data.network.MovieGuideService;
import com.vss.movieguide.data.repositories.artist.ArtistDataSourceContract;
import com.vss.movieguide.data.repositories.artist.ArtistLocalDataSource;
import com.vss.movieguide.data.repositories.artist.ArtistRemoteDataSource;
import com.vss.movieguide.data.repositories.artist.ArtistRepository;
import com.vss.movieguide.domain.usecases.ArtistDetailsDomainContract;
import com.vss.movieguide.domain.usecases.ArtistDetailsUseCase;
import com.vss.movieguide.presentation.artistdetails.ArtistDetailsPresentationContract;
import com.vss.movieguide.presentation.artistdetails.ArtistDetailsPresenter;
import com.vss.movieguide.util.rxjava.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class ArtistDetailsModule {

    private ArtistDetailsPresentationContract.View personDetailsView;

    public ArtistDetailsModule(ArtistDetailsPresentationContract.View personDetailsView) {
        this.personDetailsView = personDetailsView;
    }

    @Provides
    public ArtistDataSourceContract.LocalDateSource providePersonLocalDataSource() {
        return new ArtistLocalDataSource();
    }

    @Provides
    public ArtistDataSourceContract.RemoteDateSource providePersonRemoteDataSource(MovieGuideService movieGuideService) {
        return new ArtistRemoteDataSource(movieGuideService);
    }

    @Provides
    public ArtistDataSourceContract.Repository providePersonRepository(ArtistDataSourceContract.LocalDateSource personLocalDataSource, ArtistDataSourceContract.RemoteDateSource personRemoteDataSource) {
        return new ArtistRepository(personLocalDataSource, personRemoteDataSource);
    }

    @Provides
    public ArtistDetailsDomainContract.UseCase providePersonDetailsUseCase(ArtistDataSourceContract.Repository personRepository) {
        return new ArtistDetailsUseCase(personRepository);
    }

    @Provides
    public ArtistDetailsPresentationContract.Presenter providePersonDetailsPresenter(ArtistDetailsDomainContract.UseCase personDetailsUseCase, SchedulerProvider schedulerProvider) {
        return new ArtistDetailsPresenter(personDetailsView, personDetailsUseCase, schedulerProvider);
    }
}
