package com.vss.movieguide.di.module;

import com.vss.movieguide.util.rxjava.ProductionSchedulerProvider;
import com.vss.movieguide.util.rxjava.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

/**
  1/7/18.
 */

@Module
public class RxModule {

    @Provides
    public SchedulerProvider provideSchedulerProvider() {
        return new ProductionSchedulerProvider();
    }
}
