package com.vss.movieguide.di.module;

import android.app.Application;

import dagger.Module;
import dagger.Provides;

/**
  2/9/17.
 */

@Module
public class ApplicationModule {

    private Application application;

    public ApplicationModule(Application application) {
        this.application = application;
    }

    @Provides
    public Application provideApplication() {
        return application;
    }
}
