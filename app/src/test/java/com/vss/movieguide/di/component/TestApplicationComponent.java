package com.vss.movieguide.di.component;

import com.vss.movieguide.MovieRemoteDataSourceTest;
import com.vss.movieguide.di.module.TestNetworkModule;
import com.vss.movieguide.di.module.AndroidModule;
import com.vss.movieguide.di.module.ApplicationModule;
import com.vss.movieguide.di.module.RxModule;
import com.vss.movieguide.di.scope.ApplicationScope;

import dagger.Component;

/**
  1/15/18.
 */

@ApplicationScope
@Component(modules = {ApplicationModule.class, AndroidModule.class, TestNetworkModule.class, RxModule.class})
public interface TestApplicationComponent extends ApplicationComponent {
    void inject(MovieRemoteDataSourceTest target);
}
