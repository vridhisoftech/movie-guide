package com.vss.movieguide.di.module;

import android.app.Application;
import android.content.Context;

import com.vss.movieguide.data.network.AuthorizedNetworkInterceptor;
import com.vss.movieguide.data.network.MovieGuideService;

import dagger.Module;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;

import static org.mockito.Mockito.mock;

@Module
public class TestNetworkModule extends NetworkModule {

    @Override
    public Cache provideCache(Application application) {
        return mock(Cache.class);
    }

    @Override
    public HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        return mock(HttpLoggingInterceptor.class);
    }

    @Override
    public OkHttpClient provideOkHttpClient(Cache cache, AuthorizedNetworkInterceptor authorizedNetworkInterceptor, HttpLoggingInterceptor httpLoggingInterceptor) {
        return mock(OkHttpClient.class);
    }


    @Override
    public AuthorizedNetworkInterceptor provideAuthorizedNetworkInterceptor(Context context) {
        return mock(AuthorizedNetworkInterceptor.class);
    }

    @Override
    public Retrofit provideRetrofit(OkHttpClient okHttpClient) {
        return mock(Retrofit.class);
    }

    @Override
    public MovieGuideService provideMovieGuideService(Retrofit retrofit) {
        return mock(MovieGuideService.class);
    }
}
