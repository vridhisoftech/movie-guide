package com.vss.movieguide;

import com.vss.movieguide.domain.models.TvShowDomainModel;
import com.vss.movieguide.domain.models.TvShowsDomainModel;
import com.vss.movieguide.domain.usecases.TvShowsDomainContract;
import com.vss.movieguide.presentation.models.TvShowPresentationModel;
import com.vss.movieguide.presentation.tvshows.TvShowsPresentationContract;
import com.vss.movieguide.presentation.tvshows.TvShowsPresenter;
import com.vss.movieguide.util.rxjava.SchedulerProvider;
import com.vss.movieguide.util.rxjava.TestSchedulerProvider;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.reactivex.Single;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

public class TelevisionShowsPresenterTest {

    // region Test Doubles

    // Mocks
    @Mock
    private TvShowsPresentationContract.View mockTelevisionShowsView;
    @Mock
    private TvShowsDomainContract.UseCase mockTelevisionShowsUseCase;

    // Stubs
    private TvShowsDomainModel tvShowsDomainModelStub;
    // endregion

    // region Member Variables
    private TvShowsPresenter tvShowsPresenter;
    private SchedulerProvider schedulerProvider = new TestSchedulerProvider();
    // endregion

    @Before
    public void setUp() {
        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this);

        // Get a reference to the class under test
        tvShowsPresenter = new TvShowsPresenter(mockTelevisionShowsView, mockTelevisionShowsUseCase, schedulerProvider);
    }

    // region Test Methods
//    @Test(expected = IOException.class)
    @Test
    public void onLoadPopularTelevisionShows_shouldShowError_whenFirstPageRequestFailed() {
        // 1. (Given) Set up conditions required for the test
        tvShowsDomainModelStub = getTelevisionShowsDomainModelStub(0, 1, true);

        when(mockTelevisionShowsUseCase.getPopularTelevisionShows(anyInt())).thenReturn(Single.error(new IOException()));

        // 2. (When) Then perform one or more actions
        tvShowsPresenter.onLoadPopularTelevisionShows(tvShowsDomainModelStub.getPageNumber());

        // 3. (Then) Afterwards, verify that the state you are expecting is actually achieved
        verify(mockTelevisionShowsView).hideEmptyView();
        verify(mockTelevisionShowsView).hideErrorView();
        verify(mockTelevisionShowsView).showLoadingView();

        verify(mockTelevisionShowsView).hideLoadingView();
        verify(mockTelevisionShowsView).showErrorView();
    }

    @Test
    public void onLoadPopularTelevisionShows_shouldShowError_whenNextPageRequestFailed() {
        // 1. (Given) Set up conditions required for the test
        tvShowsDomainModelStub = getTelevisionShowsDomainModelStub(0, 2, true);

        when(mockTelevisionShowsUseCase.getPopularTelevisionShows(anyInt())).thenReturn(Single.error(new IOException()));

        // 2. (When) Then perform one or more actions
        tvShowsPresenter.onLoadPopularTelevisionShows(tvShowsDomainModelStub.getPageNumber());

        // 3. (Then) Afterwards, verify that the state you are expecting is actually achieved
        verify(mockTelevisionShowsView).showLoadingFooterView();

        verify(mockTelevisionShowsView).showErrorFooterView();
    }

    @Test
    public void onLoadPopularTelevisionShowss_shouldShowEmpty_whenFirstPageHasNoTelevisionShows() {
        // 1. (Given) Set up conditions required for the test
        tvShowsDomainModelStub = getTelevisionShowsDomainModelStub(0, 1, true);

        when(mockTelevisionShowsUseCase.getPopularTelevisionShows(anyInt())).thenReturn(Single.just(tvShowsDomainModelStub));

        // 2. (When) Then perform one or more actions
        tvShowsPresenter.onLoadPopularTelevisionShows(tvShowsDomainModelStub.getPageNumber());

        // 3. (Then) Afterwards, verify that the state you are expecting is actually achieved
        verify(mockTelevisionShowsView).hideEmptyView();
        verify(mockTelevisionShowsView).hideErrorView();
        verify(mockTelevisionShowsView).showLoadingView();

        verify(mockTelevisionShowsView).hideLoadingView();
        verify(mockTelevisionShowsView).showEmptyView();
        verify(mockTelevisionShowsView).setTelevisionShowsDomainModel(tvShowsDomainModelStub);
    }

    @Test
    public void onLoadPopularTelevisionShows_shouldNotAddTelevisionShows_whenNextPageHasNoTelevisionShows() {
        // 1. (Given) Set up conditions required for the test
        tvShowsDomainModelStub = getTelevisionShowsDomainModelStub(0, 2, true);

        when(mockTelevisionShowsUseCase.getPopularTelevisionShows(anyInt())).thenReturn(Single.just(tvShowsDomainModelStub));

        // 2. (When) Then perform one or more actions
        tvShowsPresenter.onLoadPopularTelevisionShows(tvShowsDomainModelStub.getPageNumber());

        // 3. (Then) Afterwards, verify that the state you are expecting is actually achieved
        verify(mockTelevisionShowsView).showLoadingFooterView();

        verify(mockTelevisionShowsView).removeFooterView();
        verify(mockTelevisionShowsView).setTelevisionShowsDomainModel(tvShowsDomainModelStub);
    }

    @Test
    public void onLoadPopularTelevisionShows_shouldAddTelevisionShows_whenFirstPageHasTelevisionShowsAndIsLastPage() {
        // 1. (Given) Set up conditions required for the test
        tvShowsDomainModelStub = getTelevisionShowsDomainModelStub(5, 1, true);

        when(mockTelevisionShowsUseCase.getPopularTelevisionShows(anyInt())).thenReturn(Single.just(tvShowsDomainModelStub));

        // 2. (When) Then perform one or more actions
        tvShowsPresenter.onLoadPopularTelevisionShows(tvShowsDomainModelStub.getPageNumber());

        // 3. (Then) Afterwards, verify that the state you are expecting is actually achieved
        verify(mockTelevisionShowsView).hideEmptyView();
        verify(mockTelevisionShowsView).hideErrorView();
        verify(mockTelevisionShowsView).showLoadingView();

        verify(mockTelevisionShowsView).hideLoadingView();
        verify(mockTelevisionShowsView).addHeaderView();
        verify(mockTelevisionShowsView).showTelevisionShows(tvShowsDomainModelStub.getTelevisionShows());
        verify(mockTelevisionShowsView).setTelevisionShowsDomainModel(tvShowsDomainModelStub);
    }

    @Test
    public void onLoadPopularTelevisionShows_shouldAddTelevisionShows_whenFirstPageHasTelevisionShowsAndIsNotLastPage() {
        // 1. (Given) Set up conditions required for the test
        tvShowsDomainModelStub = getTelevisionShowsDomainModelStub(5, 1, false);

        when(mockTelevisionShowsUseCase.getPopularTelevisionShows(anyInt())).thenReturn(Single.just(tvShowsDomainModelStub));

        // 2. (When) Then perform one or more actions
        tvShowsPresenter.onLoadPopularTelevisionShows(tvShowsDomainModelStub.getPageNumber());

        // 3. (Then) Afterwards, verify that the state you are expecting is actually achieved
        verify(mockTelevisionShowsView).hideEmptyView();
        verify(mockTelevisionShowsView).hideErrorView();
        verify(mockTelevisionShowsView).showLoadingView();

        verify(mockTelevisionShowsView).hideLoadingView();
        verify(mockTelevisionShowsView).addHeaderView();
        verify(mockTelevisionShowsView).showTelevisionShows(tvShowsDomainModelStub.getTelevisionShows());
        verify(mockTelevisionShowsView).addFooterView();
        verify(mockTelevisionShowsView).setTelevisionShowsDomainModel(tvShowsDomainModelStub);
    }

    @Test
    public void onLoadPopularTelevisionShows_shouldAddTelevisionShows_whenNextPageHasTelevisionShowsAndIsLastPage() {
        // 1. (Given) Set up conditions required for the test
        tvShowsDomainModelStub = getTelevisionShowsDomainModelStub(5, 2, true);

        when(mockTelevisionShowsUseCase.getPopularTelevisionShows(anyInt())).thenReturn(Single.just(tvShowsDomainModelStub));

        // 2. (When) Then perform one or more actions
        tvShowsPresenter.onLoadPopularTelevisionShows(tvShowsDomainModelStub.getPageNumber());

        // 3. (Then) Afterwards, verify that the state you are expecting is actually achieved
        verify(mockTelevisionShowsView).showLoadingFooterView();

        verify(mockTelevisionShowsView).removeFooterView();
        verify(mockTelevisionShowsView).showTelevisionShows(tvShowsDomainModelStub.getTelevisionShows());
        verify(mockTelevisionShowsView).setTelevisionShowsDomainModel(tvShowsDomainModelStub);
    }

    @Test
    public void onLoadPopularTelevisionShows_shouldAddTelevisionShows_whenNextPageHasTelevisionShowsAndIsNotLastPage() {
        // 1. (Given) Set up conditions required for the test
        tvShowsDomainModelStub = getTelevisionShowsDomainModelStub(5, 2, false);

        when(mockTelevisionShowsUseCase.getPopularTelevisionShows(anyInt())).thenReturn(Single.just(tvShowsDomainModelStub));

        // 2. (When) Then perform one or more actions
        tvShowsPresenter.onLoadPopularTelevisionShows(tvShowsDomainModelStub.getPageNumber());

        // 3. (Then) Afterwards, verify that the state you are expecting is actually achieved
        verify(mockTelevisionShowsView).showLoadingFooterView();

        verify(mockTelevisionShowsView).removeFooterView();
        verify(mockTelevisionShowsView).showTelevisionShows(tvShowsDomainModelStub.getTelevisionShows());
        verify(mockTelevisionShowsView).addFooterView();
        verify(mockTelevisionShowsView).setTelevisionShowsDomainModel(tvShowsDomainModelStub);
    }

    @Test
    public void onTelevisionShowClick_shouldOpenTelevisionShowDetails() {
        // 1. (Given) Set up conditions required for the test
        TvShowPresentationModel televisionShow = new TvShowPresentationModel();

        // 2. (When) Then perform one or more actions
        tvShowsPresenter.onTelevisionShowClick(televisionShow);

        // 3. (Then) Afterwards, verify that the state you are expecting is actually achieved
        verify(mockTelevisionShowsView).openTelevisionShowDetails(televisionShow);

        verifyZeroInteractions(mockTelevisionShowsUseCase);
    }

    @Test
    public void onScrollToEndOfList_shouldLoadMoreItems() {
        // 1. (Given) Set up conditions required for the test

        // 2. (When) Then perform one or more actions
        tvShowsPresenter.onScrollToEndOfList();

        // 3. (Then) Afterwards, verify that the state you are expecting is actually achieved
        verify(mockTelevisionShowsView).loadMoreTelevisionShows();

        verifyZeroInteractions(mockTelevisionShowsUseCase);
    }

    @Test
    public void onDestroyView_shouldNotInteractWithViewOrUsecase() {
        // 1. (Given) Set up conditions required for the test

        // 2. (When) Then perform one or more actions
        tvShowsPresenter.onDestroyView();
        // 3. (Then) Afterwards, verify that the state you are expecting is actually achieved
        verifyZeroInteractions(mockTelevisionShowsView);
        verifyZeroInteractions(mockTelevisionShowsUseCase);
    }

    // endregion

    // region Helper Methods
    private List<TvShowDomainModel> getListOfTelevisionShows(int numOfTelevisionShows){
        List<TvShowDomainModel> televisionShows = new ArrayList<>();
        for(int i=0; i<numOfTelevisionShows; i++){
            TvShowDomainModel televisionShow = new TvShowDomainModel();
            televisionShows.add(televisionShow);
        }
        return televisionShows;
    }

    private TvShowsDomainModel getTelevisionShowsDomainModelStub(int numOfTelevisionShows, int pageNumber, boolean lastPage){
        TvShowsDomainModel tvShowsDomainModel = new TvShowsDomainModel();
        tvShowsDomainModel.setTelevisionShows(getListOfTelevisionShows(numOfTelevisionShows));
        tvShowsDomainModel.setPageNumber(pageNumber);
        tvShowsDomainModel.setLastPage(lastPage);
        tvShowsDomainModel.setExpiredAt(Calendar.getInstance().getTime());
        return tvShowsDomainModel;
    }
    // endregion
}
