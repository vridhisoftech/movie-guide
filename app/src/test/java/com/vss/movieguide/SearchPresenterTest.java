package com.vss.movieguide;

import com.vss.movieguide.domain.models.MovieDomainModel;
import com.vss.movieguide.domain.models.ArtistDomainModel;
import com.vss.movieguide.domain.models.SearchDomainModel;
import com.vss.movieguide.domain.models.TvShowDomainModel;
import com.vss.movieguide.domain.usecases.SearchDomainContract;
import com.vss.movieguide.presentation.models.MoviePresentationModel;
import com.vss.movieguide.presentation.models.ArtistPresentationModel;
import com.vss.movieguide.presentation.models.TvShowPresentationModel;
import com.vss.movieguide.presentation.search.SearchPresentationContract;
import com.vss.movieguide.presentation.search.SearchPresenter;
import com.vss.movieguide.util.rxjava.SchedulerProvider;
import com.vss.movieguide.util.rxjava.TestSchedulerProvider;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
  2/9/17.
 */

public class SearchPresenterTest {

    // region Test Doubles

    // Mocks
    @Mock
    private SearchPresentationContract.View mockSearchView;
    @Mock
    private SearchDomainContract.UseCase mockSearchUseCase;

    // Stubs
//    private ArgumentCaptor<DisposableSingleObserver> disposableSingleObserverArgumentCaptor;
    private SearchDomainModel searchDomainModelStub;
    // endregion

    // region Member Variables
    private SearchPresenter searchPresenter;
    private SchedulerProvider schedulerProvider = new TestSchedulerProvider();
    // endregion

    @Before
    public void setUp() {
        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this);

        // Get a reference to the class under test
        searchPresenter = new SearchPresenter(mockSearchView, mockSearchUseCase, schedulerProvider);
    }

    // region Test Methods

    //@Test(expected = IOException.class)
    @Test
    public void onLoadSearch_shouldShowError_whenRequestFailed() {
        // 1. (Given) Set up conditions required for the test
        searchDomainModelStub = getSearchDomainModelStub();

        CharSequence[] queries = {"J", "Je", "Jen", "Jenn", "Jenni", "Jennif", "Jennife", "Jennifer"};
        Observable<CharSequence> searchQueriesObservable = Observable.fromArray(queries);

        when(mockSearchUseCase.getSearchResponse(anyString())).thenReturn(Single.error(new IOException()));


        // 2. (When) Then perform one or more actions
        searchPresenter.onLoadSearch(searchQueriesObservable);

        // 3. (Then) Afterwards, verify that the state you are expecting is actually achieved
        verify(mockSearchView, times(queries.length+1)).hideLoadingView();

        verify(mockSearchView).showErrorView();
    }

    // Test fails because of scheudluers
    @Test
    public void onLoadSearch_shouldShowTelevisionShowDetails_whenRequestSucceeded() {
        // 1. (Given) Set up conditions required for the test
        searchDomainModelStub = getSearchDomainModelStub();

        CharSequence[] queries = {"J", "Je", "Jen", "Jenn", "Jenni", "Jennif", "Jennife", "Jennifer"};
        Observable<CharSequence> searchQueriesObservable = Observable.fromArray(queries);

        when(mockSearchUseCase.getSearchResponse(anyString())).thenReturn(Single.just(searchDomainModelStub));

        // 2. (When) Then perform one or more actions
        searchPresenter.onLoadSearch(searchQueriesObservable);

        // 3. (Then) Afterwards, verify that the state you are expecting is actually achieved
        verify(mockSearchView, times(queries.length+1)).hideLoadingView();

        verify(mockSearchView).clearMoviesAdapter();
        verify(mockSearchView).clearTelevisionShowsAdapter();
        verify(mockSearchView).clearPersonsAdapter();
    }

    @Test
    public void onMovieClick_shouldOpenMovieDetails() {
        // 1. (Given) Set up conditions required for the test
        MoviePresentationModel movie = new MoviePresentationModel();

        // 2. (When) Then perform one or more actions
        searchPresenter.onMovieClick(movie);

        // 3. (Then) Afterwards, verify that the state you are expecting is actually achieved
        verify(mockSearchView).openMovieDetails(movie);

        verifyZeroInteractions(mockSearchUseCase);
    }

    @Test
    public void onTelevisionShowClick_shouldOpenTelevisionShowDetails() {
        // 1. (Given) Set up conditions required for the test
        TvShowPresentationModel televisionShow = new TvShowPresentationModel();

        // 2. (When) Then perform one or more actions
        searchPresenter.onTelevisionShowClick(televisionShow);

        // 3. (Then) Afterwards, verify that the state you are expecting is actually achieved
        verify(mockSearchView).openTelevisionShowDetails(televisionShow);

        verifyZeroInteractions(mockSearchUseCase);
    }

    @Test
    public void onPersonClick_shouldOpenPersonDetails() {
        // 1. (Given) Set up conditions required for the test
        ArtistPresentationModel person = new ArtistPresentationModel();

        // 2. (When) Then perform one or more actions
        searchPresenter.onPersonClick(person);

        // 3. (Then) Afterwards, verify that the state you are expecting is actually achieved
        verify(mockSearchView).openPersonDetails(person);

        verifyZeroInteractions(mockSearchUseCase);
    }

    @Test
    public void onDestroyView_shouldNotHaveInteractions() {
        // 1. (Given) Set up conditions required for the test

        // 2. (When) Then perform one or more actions
        searchPresenter.onDestroyView();
        // 3. (Then) Afterwards, verify that the state you are expecting is actually achieved
        verifyZeroInteractions(mockSearchView);
        verifyZeroInteractions(mockSearchUseCase);
    }

    // endregion

    // region Helper Methods
    private List<MovieDomainModel> getListOfMovies(int numOfMovies){
        List<MovieDomainModel> movies = new ArrayList<>();
        for(int i=0; i<numOfMovies; i++){
            MovieDomainModel movie = new MovieDomainModel();
            movies.add(movie);
        }
        return movies;
    }

    private List<TvShowDomainModel> getListOfTelevisionShows(int numOfTelevisionShows){
        List<TvShowDomainModel> televisionShows = new ArrayList<>();
        for(int i=0; i<numOfTelevisionShows; i++){
            TvShowDomainModel televisionShow = new TvShowDomainModel();
            televisionShows.add(televisionShow);
        }
        return televisionShows;
    }

    private List<ArtistDomainModel> getListOfPersons(int numOfPersons){
        List<ArtistDomainModel> persons = new ArrayList<>();
        for(int i=0; i<numOfPersons; i++){
            ArtistDomainModel person = new ArtistDomainModel();
            persons.add(person);
        }
        return persons;
    }

    private SearchDomainModel getSearchDomainModelStub(){
        SearchDomainModel searchDomainModel = new SearchDomainModel();
        String query = "";
        List<MovieDomainModel> movies = getListOfMovies(0);
        List<TvShowDomainModel> televisionShows = getListOfTelevisionShows(0);
        List<ArtistDomainModel> persons = getListOfPersons(0);
        searchDomainModel.setQuery(query);
        searchDomainModel.setMovies(movies);
        searchDomainModel.setTelevisionShows(televisionShows);
        searchDomainModel.setPersons(persons);
        return searchDomainModel;
    }
    // endregion

}
