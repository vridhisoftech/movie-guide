package com.vss.movieguide;

import com.vss.movieguide.domain.models.TvShowCreditDomainModel;
import com.vss.movieguide.domain.models.TvShowDetailsDomainModel;
import com.vss.movieguide.domain.models.TvShowDomainModel;
import com.vss.movieguide.domain.usecases.TvShowDetailsDomainContract;
import com.vss.movieguide.presentation.models.ArtistPresentationModel;
import com.vss.movieguide.presentation.models.TvShowPresentationModel;
import com.vss.movieguide.presentation.tvishowdetails.TvShowDetailsPresentationContract;
import com.vss.movieguide.presentation.tvishowdetails.TvShowDetailsPresenter;
import com.vss.movieguide.util.rxjava.SchedulerProvider;
import com.vss.movieguide.util.rxjava.TestSchedulerProvider;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;


public class TvShowDetailsPresenterTest {

    // region Test Doubles

    // Mocks
    @Mock
    private TvShowDetailsPresentationContract.View mockTelevisionShowDetailsView;
    @Mock
    private TvShowDetailsDomainContract.UseCase mockTelevisionShowDetailsUseCase;

    // Stubs
    private TvShowDetailsDomainModel tvShowDetailsDomainModelStub;
    // endregion

    // region Member Variables
    private TvShowDetailsPresenter tvShowDetailsPresenter;
    private SchedulerProvider schedulerProvider = new TestSchedulerProvider();
    // endregion

    @Before
    public void setUp() {
        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this);

        // Get a reference to the class under test
        tvShowDetailsPresenter = new TvShowDetailsPresenter(mockTelevisionShowDetailsView, mockTelevisionShowDetailsUseCase, schedulerProvider);
    }

    // region Test Methods
//    @Test(expected = IOException.class)
    @Test
    public void onLoadTelevisionShowDetails_shouldShowError_whenRequestFailed() {
        // 1. (Given) Set up conditions required for the test
        tvShowDetailsDomainModelStub = getTvShowDetailsDomainModelStub();

        when(mockTelevisionShowDetailsUseCase.getTelevisionShowDetails(anyInt())).thenReturn(Single.error(new IOException()));

        // 2. (When) Then perform one or more actions
        tvShowDetailsPresenter.onLoadTelevisionShowDetails(tvShowDetailsDomainModelStub.getTelevisionShow().getId());

        verify(mockTelevisionShowDetailsView).showErrorView();
    }

    @Test
    public void onLoadTelevisionShowDetails_shouldShowTelevisionShowDetails_whenRequestSucceeded() {
        // 1. (Given) Set up conditions required for the test
        tvShowDetailsDomainModelStub = getTvShowDetailsDomainModelStub();

        when(mockTelevisionShowDetailsUseCase.getTelevisionShowDetails(anyInt())).thenReturn(Single.just(tvShowDetailsDomainModelStub));


        // 2. (When) Then perform one or more actions
        tvShowDetailsPresenter.onLoadTelevisionShowDetails(tvShowDetailsDomainModelStub.getTelevisionShow().getId());

        // 3. (Then) Afterwards, verify that the state you are expecting is actually achieved
        verify(mockTelevisionShowDetailsView).setTelevisionShowDetailsDomainModel(tvShowDetailsDomainModelStub);
    }

    @Test
    public void onPersonClick_shouldOpenPersonDetails() {
        // 1. (Given) Set up conditions required for the test
        ArtistPresentationModel person = new ArtistPresentationModel();

        // 2. (When) Then perform one or more actions
        tvShowDetailsPresenter.onPersonClick(person);

        // 3. (Then) Afterwards, verify that the state you are expecting is actually achieved
        verify(mockTelevisionShowDetailsView).openPersonDetails(person);

        verifyZeroInteractions(mockTelevisionShowDetailsUseCase);
    }

    @Test
    public void onTelevisionShowClick_shouldOpenTelevisionShowDetails() {
        // 1. (Given) Set up conditions required for the test
        TvShowPresentationModel televisionShow = new TvShowPresentationModel();

        // 2. (When) Then perform one or more actions
        tvShowDetailsPresenter.onTelevisionShowClick(televisionShow);

        // 3. (Then) Afterwards, verify that the state you are expecting is actually achieved
        verify(mockTelevisionShowDetailsView).openTelevisionShowDetails(televisionShow);

        verifyZeroInteractions(mockTelevisionShowDetailsUseCase);
    }

    @Test
    public void onScrollChange_shouldShowToolbarTitle_whenScrolledPastThreshold() {
        // 1. (Given) Set up conditions required for the test
        boolean isScrolledPastThreshold = true;

        // 2. (When) Then perform one or more actions
        tvShowDetailsPresenter.onScrollChange(isScrolledPastThreshold);

        // 3. (Then) Afterwards, verify that the state you are expecting is actually achieved
        verify(mockTelevisionShowDetailsView).showToolbarTitle();

        verifyZeroInteractions(mockTelevisionShowDetailsUseCase);
    }

    @Test
    public void onScrollChange_shouldHideToolbarTitle_whenNotScrolledPastThreshold() {
        // 1. (Given) Set up conditions required for the test
        boolean isScrolledPastThreshold = false;

        // 2. (When) Then perform one or more actions
        tvShowDetailsPresenter.onScrollChange(isScrolledPastThreshold);

        // 3. (Then) Afterwards, verify that the state you are expecting is actually achieved
        verify(mockTelevisionShowDetailsView).hideToolbarTitle();

        verifyZeroInteractions(mockTelevisionShowDetailsUseCase);
    }

    @Test
    public void onDestroyView_shouldNotInteractWithViewOrUsecase() {
        // 1. (Given) Set up conditions required for the test

        // 2. (When) Then perform one or more actions
        tvShowDetailsPresenter.onDestroyView();
        // 3. (Then) Afterwards, verify that the state you are expecting is actually achieved
        verifyZeroInteractions(mockTelevisionShowDetailsView);
        verifyZeroInteractions(mockTelevisionShowDetailsUseCase);
    }

    // endregion

    // region Helper Methods
    private TvShowDetailsDomainModel getTvShowDetailsDomainModelStub(){
        TvShowDetailsDomainModel tvShowDetailsDomainModel = new TvShowDetailsDomainModel();
        TvShowDomainModel televisionShow = new TvShowDomainModel();
        televisionShow.setId(1);
        List<TvShowCreditDomainModel> cast = new ArrayList<>();
        List<TvShowCreditDomainModel> crew = new ArrayList<>();
        List<TvShowDomainModel> similarTelevisionShows = new ArrayList<>();
        String rating = "";
        tvShowDetailsDomainModel.setTelevisionShow(televisionShow);
        tvShowDetailsDomainModel.setCast(cast);
        tvShowDetailsDomainModel.setCrew(crew);
        tvShowDetailsDomainModel.setSimilarTelevisionShows(similarTelevisionShows);
        tvShowDetailsDomainModel.setRating(rating);
        return tvShowDetailsDomainModel;
    }
    // endregion
}
