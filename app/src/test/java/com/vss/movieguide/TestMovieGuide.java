package com.vss.movieguide;

import com.vss.movieguide.di.component.TestApplicationComponent;
import com.vss.movieguide.di.module.TestNetworkModule;
import com.vss.movieguide.di.module.AndroidModule;

/**
  1/15/18.
 */

public class TestMovieGuide extends MovieGuide {

    // region Static Variables
    private static TestMovieGuide currentApplication = null;
    // endregion

    private TestApplicationComponent testApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        currentApplication = this;

        testApplicationComponent = createApplicationComponent();
    }

    public TestApplicationComponent createApplicationComponent() {
        return DaggerTestApplicationComponent.builder()
                .androidModule(new AndroidModule(this))
                .networkModule(new TestNetworkModule())
                .build();
    }

//    @Override
//    public ApplicationComponent getApplicationComponent() {
//        return testApplicationComponent;
//    }

    public TestApplicationComponent getApplicationComponent() {
        return testApplicationComponent;
    }

    public static TestMovieGuide getInstance() {
        return currentApplication;
    }

}
