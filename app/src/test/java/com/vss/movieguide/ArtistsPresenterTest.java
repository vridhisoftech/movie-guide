package com.vss.movieguide;

import com.vss.movieguide.domain.models.ArtistDomainModel;
import com.vss.movieguide.domain.models.ArtistsDomainModel;
import com.vss.movieguide.domain.usecases.ArtistsDomainContract;
import com.vss.movieguide.presentation.artists.ArtistsPresentationContract;
import com.vss.movieguide.presentation.models.ArtistPresentationModel;
import com.vss.movieguide.presentation.artists.ArtistsPresenter;
import com.vss.movieguide.util.rxjava.SchedulerProvider;
import com.vss.movieguide.util.rxjava.TestSchedulerProvider;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.reactivex.Single;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;


public class ArtistsPresenterTest {

    // region Test Doubles

    // Mocks
    @Mock
    private ArtistsPresentationContract.View mockPersonsView;
    @Mock
    private ArtistsDomainContract.UseCase mockPersonsUseCase;

    // Stubs
    private ArtistsDomainModel artistsDomainModelStub;
    // endregion

    // region Member Variables
    private ArtistsPresenter artistsPresenter;
    private SchedulerProvider schedulerProvider = new TestSchedulerProvider();
    // endregion

    @Before
    public void setUp() {
        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this);

        // Get a reference to the class under test
        artistsPresenter = new ArtistsPresenter(mockPersonsView, mockPersonsUseCase, schedulerProvider);
    }

    // region Test Methods
//    @Test(expected = IOException.class)
    @Test
    public void onLoadPopularPersons_shouldShowError_whenFirstPageRequestFailed() {
        // 1. (Given) Set up conditions required for the test
        artistsDomainModelStub = getPersonsDomainModelStub(0, 1, true);

        when(mockPersonsUseCase.getPopularPersons(anyInt())).thenReturn(Single.error(new IOException()));

        // 2. (When) Then perform one or more actions
        artistsPresenter.onLoadPopularPersons(artistsDomainModelStub.getPageNumber());

        // 3. (Then) Afterwards, verify that the state you are expecting is actually achieved
        verify(mockPersonsView).hideEmptyView();
        verify(mockPersonsView).hideErrorView();
        verify(mockPersonsView).showLoadingView();

        verify(mockPersonsView).hideLoadingView();
        verify(mockPersonsView).showErrorView();
    }

    @Test
    public void onLoadPopularPersons_shouldShowError_whenNextPageRequestFailed() {
        // 1. (Given) Set up conditions required for the test
        artistsDomainModelStub = getPersonsDomainModelStub(0, 2, true);

        when(mockPersonsUseCase.getPopularPersons(anyInt())).thenReturn(Single.error(new IOException()));

        // 2. (When) Then perform one or more actions
        artistsPresenter.onLoadPopularPersons(artistsDomainModelStub.getPageNumber());

        // 3. (Then) Afterwards, verify that the state you are expecting is actually achieved
        verify(mockPersonsView).showLoadingFooterView();

        verify(mockPersonsView).showErrorFooterView();
    }

    @Test
    public void onLoadPopularPersons_shouldShowEmpty_whenFirstPageHasNoPersons() {
        // 1. (Given) Set up conditions required for the test
        artistsDomainModelStub = getPersonsDomainModelStub(0, 1, true);

        when(mockPersonsUseCase.getPopularPersons(anyInt())).thenReturn(Single.just(artistsDomainModelStub));

        // 2. (When) Then perform one or more actions
        artistsPresenter.onLoadPopularPersons(artistsDomainModelStub.getPageNumber());

        // 3. (Then) Afterwards, verify that the state you are expecting is actually achieved
        verify(mockPersonsView).hideEmptyView();
        verify(mockPersonsView).hideErrorView();
        verify(mockPersonsView).showLoadingView();

        verify(mockPersonsView).hideLoadingView();
        verify(mockPersonsView).showEmptyView();
        verify(mockPersonsView).setPersonsDomainModel(artistsDomainModelStub);
    }

    @Test
    public void onLoadPopularPersons_shouldNotAddPersons_whenNextPageHasNoPersons() {
        // 1. (Given) Set up conditions required for the test
        artistsDomainModelStub = getPersonsDomainModelStub(0, 2, true);

        when(mockPersonsUseCase.getPopularPersons(anyInt())).thenReturn(Single.just(artistsDomainModelStub));

        // 2. (When) Then perform one or more actions
        artistsPresenter.onLoadPopularPersons(artistsDomainModelStub.getPageNumber());

        // 3. (Then) Afterwards, verify that the state you are expecting is actually achieved
        verify(mockPersonsView).showLoadingFooterView();

        verify(mockPersonsView).removeFooterView();
        verify(mockPersonsView).setPersonsDomainModel(artistsDomainModelStub);
    }

    @Test
    public void onLoadPopularPersons_shouldAddPersons_whenFirstPageHasPersonsAndIsLastPage() {
        // 1. (Given) Set up conditions required for the test
        artistsDomainModelStub = getPersonsDomainModelStub(5, 1, true);

        when(mockPersonsUseCase.getPopularPersons(anyInt())).thenReturn(Single.just(artistsDomainModelStub));

        // 2. (When) Then perform one or more actions
        artistsPresenter.onLoadPopularPersons(artistsDomainModelStub.getPageNumber());

        // 3. (Then) Afterwards, verify that the state you are expecting is actually achieved
        verify(mockPersonsView).hideEmptyView();
        verify(mockPersonsView).hideErrorView();
        verify(mockPersonsView).showLoadingView();

        verify(mockPersonsView).hideLoadingView();
        verify(mockPersonsView).addHeaderView();
        verify(mockPersonsView).showPersons(artistsDomainModelStub.getPersons());
        verify(mockPersonsView).setPersonsDomainModel(artistsDomainModelStub);
    }

    @Test
    public void onLoadPopularPersons_shouldAddPersons_whenFirstPageHasPersonsAndIsNotLastPage() {
        // 1. (Given) Set up conditions required for the test
        artistsDomainModelStub = getPersonsDomainModelStub(5, 1, false);

        when(mockPersonsUseCase.getPopularPersons(anyInt())).thenReturn(Single.just(artistsDomainModelStub));

        // 2. (When) Then perform one or more actions
        artistsPresenter.onLoadPopularPersons(artistsDomainModelStub.getPageNumber());

        // 3. (Then) Afterwards, verify that the state you are expecting is actually achieved
        verify(mockPersonsView).hideEmptyView();
        verify(mockPersonsView).hideErrorView();
        verify(mockPersonsView).showLoadingView();

        verify(mockPersonsView).hideLoadingView();
        verify(mockPersonsView).addHeaderView();
        verify(mockPersonsView).showPersons(artistsDomainModelStub.getPersons());
        verify(mockPersonsView).addFooterView();
        verify(mockPersonsView).setPersonsDomainModel(artistsDomainModelStub);
    }

    @Test
    public void onLoadPopularPersons_shouldAddPersons_whenNextPageHasPersonsAndIsLastPage() {
        // 1. (Given) Set up conditions required for the test
        artistsDomainModelStub = getPersonsDomainModelStub(5, 2, true);

        when(mockPersonsUseCase.getPopularPersons(anyInt())).thenReturn(Single.just(artistsDomainModelStub));

        // 2. (When) Then perform one or more actions
        artistsPresenter.onLoadPopularPersons(artistsDomainModelStub.getPageNumber());

        // 3. (Then) Afterwards, verify that the state you are expecting is actually achieved
        verify(mockPersonsView).showLoadingFooterView();

        verify(mockPersonsView).removeFooterView();
        verify(mockPersonsView).showPersons(artistsDomainModelStub.getPersons());
        verify(mockPersonsView).setPersonsDomainModel(artistsDomainModelStub);
    }

    @Test
    public void onLoadPopularPersons_shouldAddPersons_whenNextPageHasPersonsAndIsNotLastPage() {
        // 1. (Given) Set up conditions required for the test
        artistsDomainModelStub = getPersonsDomainModelStub(5, 2, false);

        when(mockPersonsUseCase.getPopularPersons(anyInt())).thenReturn(Single.just(artistsDomainModelStub));

        // 2. (When) Then perform one or more actions
        artistsPresenter.onLoadPopularPersons(artistsDomainModelStub.getPageNumber());

        // 3. (Then) Afterwards, verify that the state you are expecting is actually achieved
        verify(mockPersonsView).showLoadingFooterView();

        verify(mockPersonsView).removeFooterView();
        verify(mockPersonsView).showPersons(artistsDomainModelStub.getPersons());
        verify(mockPersonsView).addFooterView();
        verify(mockPersonsView).setPersonsDomainModel(artistsDomainModelStub);
//        verify(mockPersonsView, times(1)).setModel(any(PersonsWrapper.class)); // Alternative verify check
    }

    @Test
    public void onPersonClick_shouldOpenPersonDetails() {
        // 1. (Given) Set up conditions required for the test
        ArtistPresentationModel person = new ArtistPresentationModel();

        // 2. (When) Then perform one or more actions
        artistsPresenter.onPersonClick(person);

        // 3. (Then) Afterwards, verify that the state you are expecting is actually achieved
        verify(mockPersonsView).openPersonDetails(person);

        verifyZeroInteractions(mockPersonsUseCase);
    }

    @Test
    public void onScrollToEndOfList_shouldLoadMoreItems() {
        // 1. (Given) Set up conditions required for the test

        // 2. (When) Then perform one or more actions
        artistsPresenter.onScrollToEndOfList();

        // 3. (Then) Afterwards, verify that the state you are expecting is actually achieved
        verify(mockPersonsView).loadMorePersons();

        verifyZeroInteractions(mockPersonsUseCase);
    }

    @Test
    public void onDestroyView_shouldNotInteractWithViewOrUsecase() {
        // 1. (Given) Set up conditions required for the test

        // 2. (When) Then perform one or more actions
        artistsPresenter.onDestroyView();
        // 3. (Then) Afterwards, verify that the state you are expecting is actually achieved
        verifyZeroInteractions(mockPersonsView);
        verifyZeroInteractions(mockPersonsUseCase);
    }

    // endregion

    // region Helper Methods
    private List<ArtistDomainModel> getListOfPersons(int numOfPersons){
        List<ArtistDomainModel> persons = new ArrayList<>();
        for(int i=0; i<numOfPersons; i++){
            ArtistDomainModel person = new ArtistDomainModel();
            persons.add(person);
        }
        return persons;
    }

    private ArtistsDomainModel getPersonsDomainModelStub(int numOfPersons, int pageNumber, boolean lastPage){
        ArtistsDomainModel artistsDomainModel = new ArtistsDomainModel();
        artistsDomainModel.setPersons(getListOfPersons(numOfPersons));
        artistsDomainModel.setPageNumber(pageNumber);
        artistsDomainModel.setLastPage(lastPage);
        artistsDomainModel.setExpiredAt(Calendar.getInstance().getTime());
        return artistsDomainModel;
    }
    // endregion
}
