package com.vss.movieguide;

import com.vss.movieguide.domain.models.ArtistCreditDomainModel;
import com.vss.movieguide.domain.models.ArtistDetailsDomainModel;
import com.vss.movieguide.domain.models.ArtistDomainModel;
import com.vss.movieguide.domain.usecases.ArtistDetailsDomainContract;
import com.vss.movieguide.presentation.models.MoviePresentationModel;
import com.vss.movieguide.presentation.models.TvShowPresentationModel;
import com.vss.movieguide.presentation.artistdetails.ArtistDetailsPresentationContract;
import com.vss.movieguide.presentation.artistdetails.ArtistDetailsPresenter;
import com.vss.movieguide.util.rxjava.SchedulerProvider;
import com.vss.movieguide.util.rxjava.TestSchedulerProvider;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

public class ArtistDetailsPresenterTest {

    // region Test Doubles

    // Mocks
    @Mock
    private ArtistDetailsPresentationContract.View mockPersonDetailsView;
    @Mock
    private ArtistDetailsDomainContract.UseCase mockPersonDetailsUseCase;

    // Stubs
    private ArtistDetailsDomainModel artistDetailsDomainModelStub;
    // endregion

    // region Member Variables
    private ArtistDetailsPresenter artistDetailsPresenter;
    private SchedulerProvider schedulerProvider = new TestSchedulerProvider();
    // endregion

    @Before
    public void setUp() {
        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this);

        // Get a reference to the class under test
        artistDetailsPresenter = new ArtistDetailsPresenter(mockPersonDetailsView, mockPersonDetailsUseCase, schedulerProvider);
    }

    // region Test Methods
//    @Test(expected = IOException.class)
    @Test
    public void onLoadTelevisionShowDetails_shouldShowError_whenRequestFailed() {
        // 1. (Given) Set up conditions required for the test
        artistDetailsDomainModelStub = getArtistDetailsDomainModelStub();

        when(mockPersonDetailsUseCase.getPersonDetails(anyInt())).thenReturn(Single.error(new IOException()));

        // 2. (When) Then perform one or more actions
        artistDetailsPresenter.onLoadPersonDetails(artistDetailsDomainModelStub.getPerson().getId());

        // 3. (Then) Afterwards, verify that the state you are expecting is actually achieved
        verify(mockPersonDetailsView).showErrorView();
    }

    @Test
    public void onLoadTelevisionShowDetails_shouldShowTelevisionShowDetails_whenRequestSucceeded() {
        // 1. (Given) Set up conditions required for the test
        artistDetailsDomainModelStub = getArtistDetailsDomainModelStub();

        when(mockPersonDetailsUseCase.getPersonDetails(anyInt())).thenReturn(Single.just(artistDetailsDomainModelStub));

        // 2. (When) Then perform one or more actions
        artistDetailsPresenter.onLoadPersonDetails(artistDetailsDomainModelStub.getPerson().getId());

        // 3. (Then) Afterwards, verify that the state you are expecting is actually achieved
        verify(mockPersonDetailsView).setPersonDetailsDomainModel(artistDetailsDomainModelStub);
    }

    @Test
    public void onMovieClick_shouldOpenMovieDetails() {
        // 1. (Given) Set up conditions required for the test
        MoviePresentationModel movie = new MoviePresentationModel();

        // 2. (When) Then perform one or more actions
        artistDetailsPresenter.onMovieClick(movie);

        // 3. (Then) Afterwards, verify that the state you are expecting is actually achieved
        verify(mockPersonDetailsView).openMovieDetails(movie);

        verifyZeroInteractions(mockPersonDetailsUseCase);
    }

    @Test
    public void onTelevisionShowClick_shouldOpenTelevisionShowDetails() {
        // 1. (Given) Set up conditions required for the test
        TvShowPresentationModel televisionShow = new TvShowPresentationModel();

        // 2. (When) Then perform one or more actions
        artistDetailsPresenter.onTelevisionShowClick(televisionShow);

        // 3. (Then) Afterwards, verify that the state you are expecting is actually achieved
        verify(mockPersonDetailsView).openTelevisionShowDetails(televisionShow);

        verifyZeroInteractions(mockPersonDetailsUseCase);
    }

    @Test
    public void onScrollChange_shouldShowToolbarTitle_whenScrolledPastThreshold() {
        // 1. (Given) Set up conditions required for the test
        boolean isScrolledPastThreshold = true;

        // 2. (When) Then perform one or more actions
        artistDetailsPresenter.onScrollChange(isScrolledPastThreshold);

        // 3. (Then) Afterwards, verify that the state you are expecting is actually achieved
        verify(mockPersonDetailsView).showToolbarTitle();

        verifyZeroInteractions(mockPersonDetailsUseCase);
    }

    @Test
    public void onScrollChange_shouldHideToolbarTitle_whenNotScrolledPastThreshold() {
        // 1. (Given) Set up conditions required for the test
        boolean isScrolledPastThreshold = false;

        // 2. (When) Then perform one or more actions
        artistDetailsPresenter.onScrollChange(isScrolledPastThreshold);

        // 3. (Then) Afterwards, verify that the state you are expecting is actually achieved
        verify(mockPersonDetailsView).hideToolbarTitle();

        verifyZeroInteractions(mockPersonDetailsUseCase);
    }

    @Test
    public void onDestroyView_shouldNotInteractWithViewOrUsecase() {
        // 1. (Given) Set up conditions required for the test

        // 2. (When) Then perform one or more actions
        artistDetailsPresenter.onDestroyView();
        // 3. (Then) Afterwards, verify that the state you are expecting is actually achieved
        verifyZeroInteractions(mockPersonDetailsView);
        verifyZeroInteractions(mockPersonDetailsUseCase);
    }

    // endregion

    // region Helper Methods
    private ArtistDetailsDomainModel getArtistDetailsDomainModelStub(){
        ArtistDetailsDomainModel artistDetailsDomainModel = new ArtistDetailsDomainModel();
        ArtistDomainModel person = new ArtistDomainModel();
        person.setId(1);
        List<ArtistCreditDomainModel> cast = new ArrayList<>();
        List<ArtistCreditDomainModel> crew = new ArrayList<>();
        artistDetailsDomainModel.setPerson(person);
        artistDetailsDomainModel.setCast(cast);
        artistDetailsDomainModel.setCrew(crew);
        return artistDetailsDomainModel;
    }
    // endregion
}
