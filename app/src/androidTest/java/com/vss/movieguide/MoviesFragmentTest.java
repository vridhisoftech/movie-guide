package com.vss.movieguide;

import androidx.test.espresso.IdlingRegistry;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.vss.movieguide.presentation.main.HomeActivity;
import com.vss.movieguide.util.EspressoIdlingResource;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

/**
  2/23/17.
 */

@RunWith(AndroidJUnit4.class)
@LargeTest
public class MoviesFragmentTest {

    // region Member Variables
    @Rule
    public ActivityTestRule<HomeActivity> activityRule = new ActivityTestRule(HomeActivity.class);
    // endregion

    @Before
    public void setUp() {
        // Register BackgroundWork IdlingResource
        IdlingRegistry.getInstance().register(EspressoIdlingResource.getIdlingResource());
    }

    // region Test Methods
    @Test
    public void onMovieClick_shouldOpenToMovieDetails() {
        // 1. (Given) Set up conditions required for the test

        // 2. (When) Then perform one or more actions
//        onView(withId(R.id.rv)).perform(RecyclerViewActions.scrollToPosition(1));
        onView(withId(R.id.rv)).perform(actionOnItemAtPosition(8, click()));

        // 3. (Then) Afterwards, verify that the state you are expecting is actually achieved
        onView(withId(R.id.backdrop_iv)).check(matches(isDisplayed()));
    }
    // endregion

    @After
    public void tearDown() {
        // Unregister BackgroundWork IdlingResource
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.getIdlingResource());
    }
}
